# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
# %pylab inline
from cpymad.madx import Madx

# %%
import matplotlib.patches as patches

def plotLatticeSeries(ax,series, height=1., v_offset=0., color='r',alpha=0.5):
    aux=series
    ax.add_patch(
    patches.Rectangle(
        (aux.s-aux.l, v_offset-height/2.),   # (x,y)
        aux.l,          # width
        height,          # height
        color=color, alpha=alpha
    )
    )
    return;


# %% [markdown]
# # Parameters

# %%
c = 2000          # m, circumference
ncells = 40       # number of cells
lcell = c/ncells # m, length of cell 
lq = 3.0          # m, length of main quads (both focusing and defocusing)
ld = 5.0          # m, length of main dipoles 
f = 1.5*lcell      # m, focusing length
k1 = 1/f          # 1/m, focusing strength for main quads
k2 = -1/f          # 1/m, focusing strength for main quads

# Triplet
lq3 = 3.0          # m, length of triplets
k1q3 = 0.030303030  # 1/m, focusing strength of tripplets
k2q3 = -0.030303030 # 1/m, focusing strength of tripplets

# Matching quads left of the IPs
lqm = 3.0          # m, matching quads
k1m = 0         # 1/m, focusing strength of matching quads
k2m = 0         # 1/m, focusing strength of matching quads
k3m = 0         # 1/m, focusing strength of matching quads
k4m = 0         # 1/m, focusing strength of matching quads
k5m = 0         # 1/m, focusing strength of matching quads

# Matching quads right of the IPs
lqm = 3.0          # m, matching quads
k6m = 0         # 1/m, focusing strength of matching quads
k7m = 0         # 1/m, focusing strength of matching quads
k8m = 0         # 1/m, focusing strength of matching quads
k9m = 0         # 1/m, focusing strength of matching quads
k10m = 0         # 1/m, focusing strength of matching quads

# Sextupoles
ks1 = 0
ks2 = 0


# Flags
flat_optics = True
if flat_optics:
    betaxstar = 0.7
    betaystar = 0.7
else:
    betaxstar = 0.4
    betaystar = 1.0
    
thick_dipoles = True
if thick_dipoles:
    dipoleclass = 'DIPOL    : SBEND       , L:=ld, ANGLE:= angle;'
else:
    dipoleclass = 'DIPOL    : multipole, knl:= {angle};'

# %% [markdown]
# # Lattice

# %%
par = f'''
        c = {c};
        ncells = {ncells};
        lcell = {lcell};
        lq = {lq};
        ld = {ld};
        f = {f};
        k1 = {k1};
        k2 = {k2};
        lq3 = {lq3};
        k1q3 = {k1q3};
        k2q3 = {k2q3};
        lqm = {lqm};
        k1m = {k1m};
        k2m = {k2m};
        k3m = {k3m};
        k4m = {k4m};
        k5m = {k5m};
        k6m = {k6m};
        k7m = {k7m};
        k8m = {k8m};
        k9m = {k9m};
        k10m = {k10m};
        ks1 = {ks1};
        ks2 = {ks2};
'''

ele = f'''
        {dipoleclass}
        
        ! Main focusing quads
        QF       : QUADRUPOLE  , K1:=k1, L:=lq;          ! main focusing quadrupoles
        QD       : QUADRUPOLE  , K1:=k2, L:=lq;          ! main defocusing quadrupoles
        !QF       : MULTIPOLE  , knl:={0,k1};
        !QD       : MULTIPOLE  , knl:={0,k2};
        
        ! Tripplets
        QFT      : QUADRUPOLE  , K1:=k1q3, L:=lq3;       ! main triplet focusing quadrupoles
        QDT      : QUADRUPOLE  , K1:=k2q3, L:=lq3;       ! main triplet defocusing quadrupoles
        
        ! Matching quads left of the IPs
        QM1      : QUADRUPOLE  , K1:=k1m, L:=lqm;        
        QM2      : QUADRUPOLE  , K1:=k2m, L:=lqm;        
        QM3      : QUADRUPOLE  , K1:=k3m, L:=lqm;        
        QM4      : QUADRUPOLE  , K1:=k4m, L:=lqm;        
        QM5      : QUADRUPOLE  , K1:=k5m, L:=lqm;        
        QM6      : QUADRUPOLE  , K1:=k6m, L:=lqm;        
        QM7      : QUADRUPOLE  , K1:=k7m, L:=lqm;        
        QM8      : QUADRUPOLE  , K1:=k8m, L:=lqm;        
        QM9      : QUADRUPOLE  , K1:=k9m, L:=lqm;        
        QM10     : QUADRUPOLE  , K1:=k10m, L:=lqm;        
        mymarker: marker;
'''
ele += '''
        ! Sextupoles
        !SEXT1    : SEXTUPOLE   , K2:=KS1, L:=0.1;
        !SEXT2    : SEXTUPOLE   , K2:=KS2, L:=0.1;
        SEXT1    : MULTIPOLE   , Knl:={0,0,KS1};
        SEXT2    : MULTIPOLE   , Knl:={0,0,KS2};
'''

# Main ring
seq = '''
        ring: SEQUENCE, refer=centre, L=c;
'''
for i in range(ncells):
    #if i in [0,2,ncells/2-3,ncells/2-1, ncells/2, ncells/2+2, ncells-3, ncells-1]:
    if i==100:
        seq += f'''
        qf{i}: qf, at = {i}*lcell+lq/2;
        xno{i}1: sext1, at = lcell*{i}+ lcell*0.1;
        qd{i}: qd, at = {i}*lcell+lcell*0.5+lq/2;
        xno{i}2: sext2, at = lcell*{i}+ lcell*0.6;
        \n'''
    else:
        seq += f'''
        qf{i}: qf, at = lcell*{i}+lq;
        xno{i}1: sext1, at = lcell*{i}+ lcell*0.1;
        sb{i}1: DIPOL, at = lcell*{i} + lcell*0.2+ld/2;
        sb{i}2: DIPOL, at = lcell*{i} + lcell*0.35+ld/2;
        qd{i}: qd, at = lcell*{i}+lcell*0.5+lq/2;
        xno{i}2: sext2, at = lcell*{i}+ lcell*0.6;
        sb{i}3: DIPOL, at = lcell*{i} + lcell*0.65+ld/2;
        sb{i}4: DIPOL, at = lcell*{i} + lcell*0.8+ld/2;
        \n'''
seq += '''
        endsequence;
'''

beam = '''
        beam, particle=proton, energy=20.0;
'''

# %%
madinput = f'''
        {par}
        {ele}
        {seq}
        {beam}
        angle = twopi/(4*ncells);
        use, sequence=ring;
'''

# %%
#mad = Madx(stdout=False)
mad = Madx()

# %%
mad.input(madinput)

# %% [markdown]
# # A simple match

# %%
phi_cell= 60
tuneH = phi_cell*np.pi/180*ncells/2/np.pi
tuneV = phi_cell*np.pi/180*ncells/2/np.pi

# %%
mad.input(f'''
        MATCH, Sequence=ring;
            VARY, NAME = k1, STEP = 1e-4;
            VARY, NAME = k2, STEP = 1e-4;
            GLOBAL, Q1 = {tuneH};
            GLOBAL, Q2 = {tuneV};
            JACOBIAN,CALLS=1000,TOLERANCE=1.0E-18,STRATEGY=3;
        ENDMATCH;
''')

# %%
mad.input('twiss;')
twtable = mad.table.twiss
myTwiss = twtable.dframe()

# %%
fig = plt.figure(figsize=(13,8))
fontsize=15

ax1=plt.subplot2grid((3,3), (0,0), colspan=3, rowspan=1)
plt.plot(myTwiss['s'],0*myTwiss['s'],'k')
DF=myTwiss[(myTwiss['keyword']=='quadrupole')]
#DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='r')
color = 'red'
ax1.set_ylabel('1/f=K1L [m$^{-1}$]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax1.tick_params(axis='y', labelcolor=color)
ax1.tick_params(axis='x', bottom=False,labelbottom=False)
ax1.tick_params(labelsize=fontsize)
#plt.grid()
plt.ylim(-.08,.08)
#plt.title('Our machine, 2016 Optics, Beam 1, IP5 squeeze, $\\beta_{IP}=$'+format(myTwiss[myTwiss['name'].str.contains('ip5')]['betx'].values[0],'2.2f')+' m, Q=('+format(madx.table.summ.Q1[0],'2.3f')+', '+ format(madx.table.summ.Q2[0],'2.3f')+'), $\\xi$=('+format(madx.table.summ.DQ1[0],'2.3f')+', '+ format(madx.table.summ.DQ2[0],'2.3f')+')')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'blue'
ax2.set_ylabel('$\\theta$=K0L [mrad]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax2.tick_params(axis='y', labelcolor=color)
ax2.tick_params(labelsize=fontsize)
DF=myTwiss[(myTwiss['keyword']=='multipole')]
if thick_dipoles:
    DF=myTwiss[(myTwiss['keyword']=='sbend')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(ax2,aux, height=aux.angle, v_offset=aux.angle/2, color='b')
    plotLatticeSeries(ax2,aux, height=aux.k0l, v_offset=aux.k0l/2, color='b')
plt.ylim(-.1,.1)

plt.subplot2grid((3,3), (1,0), colspan=3, rowspan=2,sharex=ax1)

plot(myTwiss.s,myTwiss.betx,'-b',label='$\\beta_x$')
plot(myTwiss.s,myTwiss.bety,'-r',label='$\\beta_y$')
plot(myTwiss.s,myTwiss.dx,'-g',label='$D_x$')
xlabel('s [m]',fontsize=fontsize)
ylabel('$\\beta$ [m]',fontsize=fontsize)
xticks(fontsize=fontsize)
yticks(fontsize=fontsize)
grid()
legend(fontsize=fontsize-3,loc='upper right')


# %%
# check phase advance
print('phase advance per cell in x and y')
print(mad.table.summ.q1*2*np.pi/ncells*180/np.pi)
print(mad.table.summ.q2*2*np.pi/ncells*180/np.pi)

# %% [markdown]
# # Add dispersion suppresor

# %%
# Remove dipoles (m=1, n=1 scheme for 60 degrees phase advance)
seqedit = '''
        Seqedit, sequence=ring;
        flatten;
'''
for i in range(ncells):
    if i in [ncells-6, ncells-4,ncells-3,ncells-2,ncells-1,0,1,2,3, 5, 
             ncells/2-6, ncells/2-4,ncells/2-3,ncells/2-2,ncells/2-1,ncells/2,ncells/2+1,ncells/2+2,ncells/2+3, ncells/2+5]:
        for j in range(4):
            seqedit+= f'''remove, element=sb{i}{j+1};\n'''
            
seqedit += '''flatten; 
              endedit;'''

# %%
mad.input('use, sequence=ring;')

mad.input(f'''
{seqedit}
angle = twopi/(4*ncells - 4*(4+1)*2*2);
''')
mad.input('use, sequence=ring;')

# %%
# Rematching is not needed if bends are thin
if thick_dipoles:
    mad.input(f'''
            MATCH, Sequence=ring;
                VARY, NAME = k1, STEP = 1e-4;
                VARY, NAME = k2, STEP = 1e-4;
                GLOBAL, Q1 = {tuneH};
                GLOBAL, Q2 = {tuneV};
                JACOBIAN,CALLS=1000,TOLERANCE=1.0E-18,STRATEGY=3;
            ENDMATCH;
    ''')

# %%
mad.input('use, sequence=ring;')
mad.input('twiss;')
twtable = mad.table.twiss
myTwiss = twtable.dframe()

# %%
fig = plt.figure(figsize=(18,8))
fontsize=15

ax1=plt.subplot2grid((3,3), (0,0), colspan=3, rowspan=1)
plt.plot(myTwiss['s'],0*myTwiss['s'],'k')
for i in range(41):
    ax1.axvline((i)*50,c='black',lw=0.5,ls='--')
DF=myTwiss[(myTwiss['keyword']=='quadrupole')]
#DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='r')
color = 'red'
ax1.set_ylabel('1/f=K1L [m$^{-1}$]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax1.tick_params(axis='y', labelcolor=color)
ax1.tick_params(axis='x', bottom=False,labelbottom=False)
ax1.tick_params(labelsize=fontsize)
ax1.plot(0,0,'*',c='purple',ms=15)
ax1.plot(c/2,0,'*',c='purple',ms=15)
plt.ylim(-.08,.08)
#plt.title('Our machine, 2016 Optics, Beam 1, IP5 squeeze, $\\beta_{IP}=$'+format(myTwiss[myTwiss['name'].str.contains('ip5')]['betx'].values[0],'2.2f')+' m, Q=('+format(madx.table.summ.Q1[0],'2.3f')+', '+ format(madx.table.summ.Q2[0],'2.3f')+'), $\\xi$=('+format(madx.table.summ.DQ1[0],'2.3f')+', '+ format(madx.table.summ.DQ2[0],'2.3f')+')')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'blue'
ax2.set_ylabel('$\\theta$=K0L [mrad]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax2.tick_params(axis='y', labelcolor=color)
ax2.tick_params(labelsize=fontsize)
DF=myTwiss[(myTwiss['keyword']=='multipole')]
if thick_dipoles:
    DF=myTwiss[(myTwiss['keyword']=='sbend')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(ax2,aux, height=aux.angle, v_offset=aux.angle/2, color='b')
    plotLatticeSeries(ax2,aux, height=aux.k0l, v_offset=aux.k0l/2, color='b')
plt.ylim(-.2,.2)

plt.subplot2grid((3,3), (1,0), colspan=3, rowspan=2,sharex=ax1)

plot(myTwiss.s,myTwiss.betx,'-b',label='$\\beta_x$')
plot(myTwiss.s,myTwiss.bety,'-r',label='$\\beta_y$')
#plot(myTwiss.s,myTwiss.alfx*10,'--b')
#plot(myTwiss.s,myTwiss.alfy*10,'--r')
plot(myTwiss.s,myTwiss.dx,'-g',label='$D_x$')
xlabel('s [m]',fontsize=fontsize)
ylabel('$\\beta$ [m]',fontsize=fontsize)
xticks(fontsize=fontsize)
yticks(fontsize=fontsize)
grid()
legend(fontsize=fontsize-3,loc='upper right')


# %%
fig.gca().set_xlim(16*lcell,24*lcell)
fig.gca().set_ylim(-3,3)
display(fig)

# %%
# Optics at the entrance of the cell (left of the IP)
print(myTwiss.betx[0])
print(myTwiss.alfx[0])
print(myTwiss.bety[0])
print(myTwiss.alfy[0])
betxtargetleft = myTwiss.betx[0]
alfxtargetleft = myTwiss.alfx[0]
betytargetleft = myTwiss.bety[0]
alfytargetleft = myTwiss.alfy[0]

# %%
# Optics at the end of the cell (right of the IP)
print(myTwiss.betx[-1])
print(myTwiss.alfx[-1])
print(myTwiss.bety[-1])
print(myTwiss.alfy[-1])
betxtargetright = myTwiss.betx[-1]
alfxtargetright = myTwiss.alfx[-1]
betytargetright = myTwiss.bety[-1]
alfytargetright = myTwiss.alfy[-1]

# %%
# Get strengths of main quads
mad.input('value, k1;')
mad.input('value, k2;')
k1 = mad.globals.k1
k2 = mad.globals.k2

# %%
# check phase advance
print('phase advance per cell in x and y')
print(mad.table.summ.q1*2*np.pi/ncells*180/np.pi)
print(mad.table.summ.q2*2*np.pi/ncells*180/np.pi)

# %% [markdown]
# # IP left

# %%
# straight section left of the IP
seq2 = '''
        ss: SEQUENCE, refer=centre, L=4*lcell;
        
        ! matching quads
        n = 0;
        QM11: qm1, at = lcell*n+lcell*0.25+lqm/2;
        QM12: qm2, at = lcell*n+lcell*0.75+lqm/2;
        
        ! matching quads
        n = 1; 
        QM13: qm3, at = lcell*n+lcell*0.25+lqm/2;
        
        n = 2; !empty
        QM14: qm4, at = lcell*n+lcell*0+lqm/2;
        QM15: qm5, at = lcell*n+lcell*0.75+lqm/2;
        
        ! Triplet
        n = 3;  
        QFT11: qft, at = lcell*n+16.66666666666;
        QDT11: qdt, at = lcell*n+25;
        QDT12: qdt, at = lcell*n+33.33333333333;
        QFT12: qft, at = lcell*n+41.66666666666;
        
        endsequence;
'''

beam2 = '''
        beam, particle=proton, energy=20.0;
'''

# %%
madinput = f'''
        {seq2}
        {beam2}
        use, sequence=ss;
        
'''
mad.input(madinput)

# %%
# Some initial parameters
mad.input(f'''
k1m = -0.02012455257;
k2m = 0.02286888419;
k3m = -0.01958231945;
k4m = 0.01119300171;
k5m = -0.02108338403;
''')


# %%
# match beginning of the lattice
mad.input('SAVEBETA, LABEL=bini, PLACE=#s;')
mad.input(f'twiss, betx={betxtargetleft},bety={betytargetleft},alfx={alfxtargetleft},alfy={alfytargetleft};')

# %%
# match end of the lattice (IP)
mad.input(f'''
MATCH, SEQUENCE=ss, beta0=bini;
VARY, NAME = k1m, STEP = 1e-4;
VARY, NAME = k2m, STEP = 1e-4;
VARY, NAME = k3m, STEP = 1e-4;
VARY, NAME = k4m, STEP = 1e-4;
VARY, NAME = k5m, STEP = 1e-4;
!constraint,sequence=ss,range=#s/#e,betx<4000;
constraint,sequence=ss,range=#e,betx={betaxstar},bety={betaystar},alfx=0,alfy=0;
jacobian,calls=5000,tolerance=1.e-10;
endmatch;
''')

# %%
mad.input('twiss, beta0=bini;')
twtable=mad.table.twiss
myTwiss = twtable.dframe()

# %%
fig = plt.figure(figsize=(18,8))
fontsize=15

ax1=plt.subplot2grid((3,3), (0,0), colspan=3, rowspan=1)
plt.plot(myTwiss['s'],0*myTwiss['s'],'k')
DF=myTwiss[(myTwiss['keyword']=='quadrupole')]
#DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='r')
color = 'red'
ax1.set_ylabel('1/f=K1L [m$^{-1}$]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax1.tick_params(axis='y', labelcolor=color)
ax1.tick_params(axis='x', bottom=False,labelbottom=False)
ax1.tick_params(labelsize=fontsize)
plt.ylim(-.1,.1)
plt.grid(True)
#plt.title('Our machine, 2016 Optics, Beam 1, IP5 squeeze, $\\beta_{IP}=$'+format(myTwiss[myTwiss['name'].str.contains('ip5')]['betx'].values[0],'2.2f')+' m, Q=('+format(madx.table.summ.Q1[0],'2.3f')+', '+ format(madx.table.summ.Q2[0],'2.3f')+'), $\\xi$=('+format(madx.table.summ.DQ1[0],'2.3f')+', '+ format(madx.table.summ.DQ2[0],'2.3f')+')')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'blue'
ax2.set_ylabel('$\\theta$=K0L [mrad]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax2.tick_params(axis='y', labelcolor=color)
ax2.tick_params(labelsize=fontsize)
#DF=myTwiss[(myTwiss['keyword']=='sbend')]
DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(ax2,aux, height=aux.angle, v_offset=aux.angle/2, color='b')
    plotLatticeSeries(ax2,aux, height=aux.k0l, v_offset=aux.k0l/2, color='b')
plt.ylim(-.2,.2)

plt.subplot2grid((3,3), (1,0), colspan=3, rowspan=2,sharex=ax1)

plot(myTwiss.s,myTwiss.betx,'-b',label='$\\beta_x$')
plot(myTwiss.s,myTwiss.bety,'-r',label='$\\beta_y$')
plot(myTwiss.s,myTwiss.dx,'-g',label='$D_x$')
xlabel('s [m]',fontsize=fontsize)
ylabel('$\\beta$ [m]',fontsize=fontsize)
xticks(fontsize=fontsize)
yticks(fontsize=fontsize)
grid()
legend(fontsize=fontsize-3,loc='upper right')


# %%
# Get global strengths of matching quads
mad.input('value,k1m;')
mad.input('value,k2m;')
mad.input('value,k3m;')
mad.input('value,k4m;')
mad.input('value,k5m;')
k1m = mad.globals.k1m
k2m = mad.globals.k2m
k3m = mad.globals.k3m
k4m = mad.globals.k4m
k5m = mad.globals.k5m

# %%
# Confirm that the optics at the entrance are correct
print(myTwiss.betx[0]-betxtargetleft)
print(myTwiss.alfx[0]-alfxtargetleft)
print(myTwiss.bety[0]-betytargetleft)
print(myTwiss.alfy[0]-alfytargetleft)

# %% [markdown]
# # IP right

# %%
# straight section left of the IP
seq2 = '''
        ss2: SEQUENCE, refer=centre, L=4*lcell;
        
        ! Triplet
        n = 0;
        ! Antisymmetric
        !QDT22: qdt, at = lcell*n+50-41.66666666666;
        !QFT22: qft, at = lcell*n+50-33.33333333333;
        !QFT21: qft, at = lcell*n+50-25;
        !QDT21: qdt, at = lcell*n+50-16.66666666666;
        ! Symmetric
        QFT22: qft, at = lcell*n+50-41.66666666666;
        QDT22: qdt, at = lcell*n+50-33.33333333333;
        QDT21: qdt, at = lcell*n+50-25;
        QFT21: qft, at = lcell*n+50-16.66666666666;
        
        
        ! matching quads
        n = 1;
        QM16: qm6, at = lcell*n+lcell*(1-0.75)-lqm/2;
        QM17: qm7, at = lcell*n+lcell*(1-0)-lqm/2;
        
        n = 2; !empty
        QM18: qm8, at = lcell*n+lcell*(1-0.25)-lqm/2;
    
        ! Triplet
        n = 3;  
        QM19: qm9, at = lcell*n+lcell*(1-0.75)-lqm/2;
        QM110: qm10, at = lcell*n+lcell*(1-0.25)-lqm/2;
        
        endsequence;
'''

beam2 = '''
        beam, particle=proton, energy=20.0;
'''

# %%
madinput = f'''
        {seq2}
        {beam2}
        use, sequence=ss2;
        
'''
mad.input(madinput)

# %%
# some initial strengths
mad.input('''
k10m = -0.020000721;
k9m = 0.02273259172;
k8m = -0.02270564855;
k7m = 0.01058591628;
k6m = -0.02038114704;
''')

# %%
# match beginning of the lattice (IP)
mad.input('SAVEBETA, LABEL=bini, PLACE=#s;')
mad.input(f'twiss, betx={betaxstar},bety={betaystar},alfx=0.0,alfy=0.0;')

# %%
# match end of the lattice
mad.input('use, sequence=ss2;')
mad.input(f'''
MATCH, SEQUENCE=ss2, beta0=bini;
VARY, NAME = k6m, STEP = 1e-4;
VARY, NAME = k7m, STEP = 1e-4;
VARY, NAME = k8m, STEP = 1e-4;
VARY, NAME = k9m, STEP = 1e-4;
VARY, NAME = k10m, STEP = 1e-4;
!constraint,sequence=ss2,range=#s/#e,betx<10000;
constraint,sequence=ss2,range=#e,betx={betxtargetright},bety={betytargetright},alfx={alfxtargetright},alfy={alfytargetright};
jacobian,calls=10000,tolerance=1.e-10;
endmatch;
''')

# %%
mad.input('twiss, beta0=bini;')
twtable=mad.table.twiss
myTwiss = twtable.dframe()

# %%
fig = plt.figure(figsize=(18,8))
fontsize=15

ax1=plt.subplot2grid((3,3), (0,0), colspan=3, rowspan=1)
plt.plot(myTwiss['s'],0*myTwiss['s'],'k')
DF=myTwiss[(myTwiss['keyword']=='quadrupole')]
#DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='r')
color = 'red'
ax1.set_ylabel('1/f=K1L [m$^{-1}$]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax1.tick_params(axis='y', labelcolor=color)
ax1.tick_params(axis='x', bottom=False,labelbottom=False)
ax1.tick_params(labelsize=fontsize)
plt.ylim(-.1,.1)
#plt.title('Our machine, 2016 Optics, Beam 1, IP5 squeeze, $\\beta_{IP}=$'+format(myTwiss[myTwiss['name'].str.contains('ip5')]['betx'].values[0],'2.2f')+' m, Q=('+format(madx.table.summ.Q1[0],'2.3f')+', '+ format(madx.table.summ.Q2[0],'2.3f')+'), $\\xi$=('+format(madx.table.summ.DQ1[0],'2.3f')+', '+ format(madx.table.summ.DQ2[0],'2.3f')+')')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'blue'
ax2.set_ylabel('$\\theta$=K0L [mrad]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax2.tick_params(axis='y', labelcolor=color)
ax2.tick_params(labelsize=fontsize)
#DF=myTwiss[(myTwiss['keyword']=='sbend')]
DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(ax2,aux, height=aux.angle, v_offset=aux.angle/2, color='b')
    plotLatticeSeries(ax2,aux, height=aux.k0l, v_offset=aux.k0l/2, color='b')
plt.ylim(-.2,.2)

plt.subplot2grid((3,3), (1,0), colspan=3, rowspan=2,sharex=ax1)

plot(myTwiss.s,myTwiss.betx,'-b',label='$\\beta_x$')
plot(myTwiss.s,myTwiss.bety,'-r',label='$\\beta_y$')
plot(myTwiss.s,myTwiss.dx,'-g',label='$D_x$')
xlabel('s [m]',fontsize=fontsize)
ylabel('$\\beta$ [m]',fontsize=fontsize)
xticks(fontsize=fontsize)
yticks(fontsize=fontsize)
grid()
legend(fontsize=fontsize-3,loc='upper right')


# %%
# Get global strengths of matching quads
mad.input('value,k6m;')
mad.input('value,k7m;')
mad.input('value,k8m;')
mad.input('value,k9m;')
mad.input('value,k10m;')
k6m = mad.globals.k6m
k7m = mad.globals.k7m
k8m = mad.globals.k8m
k9m = mad.globals.k9m
k10m = mad.globals.k10m

# %%
# Confirm that the optics at the exit are correct
print(myTwiss.betx[-1]-betxtargetright)
print(myTwiss.alfx[-1]-alfxtargetright)
print(myTwiss.bety[-1]-betytargetright)
print(myTwiss.alfy[-1]-alfytargetright)

# %% [markdown]
# # Stiched straight section

# %%
# straight section left of the IP
seq3 = '''
        ss3: SEQUENCE, refer=centre, L=8*lcell;
        
        ! matching quads
        n = 0;
        QM11: qm1, at = lcell*n+lcell*0.25+lqm/2;
        QM12: qm2, at = lcell*n+lcell*0.75+lqm/2;
        
        ! matching quads
        n = 1; 
        QM13: qm3, at = lcell*n+lcell*0.25+lqm/2;
        
        n = 2; !empty
        QM14: qm4, at = lcell*n+lcell*0+lqm/2;
        QM15: qm5, at = lcell*n+lcell*0.75+lqm/2;
        
        ! Triplet
        n = 3;  
        QFT11: qft, at = lcell*n+16.66666666666;
        QDT11: qdt, at = lcell*n+25;
        QDT12: qdt, at = lcell*n+33.33333333333;
        QFT12: qft, at = lcell*n+41.66666666666;
        
        myMarker: marker, at = 4*lcell;
        
        
        ! Triplet
        n = 4;
        ! Antisymmetric
        !QDT22: qdt, at = lcell*n+lcell-41.66666666666;
        !QFT22: qft, at = lcell*n+lcell-33.33333333333;
        !QFT21: qft, at = lcell*n+lcell-25;
        !QDT21: qdt, at = lcell*n+lcell-16.66666666666;
        ! Symmetric
        QFT22: qft, at = lcell*n+lcell-41.66666666666;
        QDT22: qdt, at = lcell*n+lcell-33.33333333333;
        QDT21: qdt, at = lcell*n+lcell-25;
        QFT21: qft, at = lcell*n+lcell-16.66666666666;
        
        
        ! matching quads
        n = 5;
        QM16: qm6, at = lcell*n+lcell*(1-0.75)-lqm/2;
        QM17: qm7, at = lcell*n+lcell*(1-0)-lqm/2;
        
        n = 6; !empty
        QM18: qm8, at = lcell*n+lcell*(1-0.25)-lqm/2;
    
        ! Triplet
        n = 7;  
        QM19: qm9, at = lcell*n+lcell*(1-0.75)-lqm/2;
        QM110: qm10, at = lcell*n+lcell*(1-0.25)-lqm/2;
        
        endsequence;
'''

beam2 = '''
        beam, particle=proton, energy=20.0;
'''

# %%
madinput = f'''
        {seq3}
        {beam2}
        use, sequence=ss3;
        
'''
mad.input(madinput)

# %%
mad.input(f'''
!k10m = -0.02012455257;
!k9m = 0.02286888419;
!k8m = -0.01958231945;
!k7m = 0.01119300171;
!k6m = -0.02108338403;

k10m = {k10m};
k9m = {k9m};
k8m = {k8m};
k7m = {k7m};
k6m = {k6m};


k1m = {k1m};
k2m = {k2m};
k3m = {k3m};
k4m = {k4m};
k5m = {k5m};
''')

# %%
mad.input('SAVEBETA, LABEL=bini, PLACE=#s;')
mad.input(f'twiss, betx={betxtargetleft},bety={betytargetleft},alfx={alfxtargetleft},alfy={alfytargetleft};')

# %%
mad.input('twiss, beta0=bini;')
twtable=mad.table.twiss
myTwiss = twtable.dframe()

# %%
fig = plt.figure(figsize=(18,8))
fontsize=15

ax1=plt.subplot2grid((3,3), (0,0), colspan=3, rowspan=1)
plt.plot(myTwiss['s'],0*myTwiss['s'],'k')
DF=myTwiss[(myTwiss['keyword']=='quadrupole')]
#DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='r')
color = 'red'
ax1.set_ylabel('1/f=K1L [m$^{-1}$]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax1.tick_params(axis='y', labelcolor=color)
ax1.tick_params(axis='x', bottom=False,labelbottom=False)
ax1.tick_params(labelsize=fontsize)
plt.ylim(-.1,.1)
plt.grid(True)
#plt.title('Our machine, 2016 Optics, Beam 1, IP5 squeeze, $\\beta_{IP}=$'+format(myTwiss[myTwiss['name'].str.contains('ip5')]['betx'].values[0],'2.2f')+' m, Q=('+format(madx.table.summ.Q1[0],'2.3f')+', '+ format(madx.table.summ.Q2[0],'2.3f')+'), $\\xi$=('+format(madx.table.summ.DQ1[0],'2.3f')+', '+ format(madx.table.summ.DQ2[0],'2.3f')+')')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'blue'
ax2.set_ylabel('$\\theta$=K0L [mrad]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax2.tick_params(axis='y', labelcolor=color)
ax2.tick_params(labelsize=fontsize)
#DF=myTwiss[(myTwiss['keyword']=='sbend')]
DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(ax2,aux, height=aux.angle, v_offset=aux.angle/2, color='b')
    plotLatticeSeries(ax2,aux, height=aux.k0l, v_offset=aux.k0l/2, color='b')
plt.ylim(-.2,.2)

plt.subplot2grid((3,3), (1,0), colspan=3, rowspan=2,sharex=ax1)

plot(myTwiss.s,myTwiss.betx,'-b',label='$\\beta_x$')
plot(myTwiss.s,myTwiss.bety,'-r',label='$\\beta_y$')
plot(myTwiss.s,myTwiss.alfx,'--b',label='$\\alpha_x$')
plot(myTwiss.s,myTwiss.alfy,'--r',label='$\\alpha_y$')
plot(myTwiss.s,myTwiss.dx,'-g',label='$D_x$')
xlabel('s [m]',fontsize=fontsize)
ylabel('$\\beta$ [m]',fontsize=fontsize)
xticks(fontsize=fontsize)
yticks(fontsize=fontsize)
grid()
#plt.ylim(-2,2)
#plt.ylim(30,80)
#plt.xlim(-4,4)
#plt.xlim(8*lcell-4,8*lcell+4)
legend(fontsize=fontsize-3,loc='upper right')


# %%
# Optics at the entrance of the cell (left of the IP)
print(myTwiss.betx[0])
print(myTwiss.alfx[0])
print(myTwiss.bety[0])
print(myTwiss.alfy[0])

# %%
# Optics at the entrance of the cell (left of the IP)
print(myTwiss.betx[-1])
print(myTwiss.alfx[-1])
print(myTwiss.bety[-1])
print(myTwiss.alfy[-1])

# %% [markdown]
# # Collider

# %%
# Main ring
seq = '''
        collider: SEQUENCE, refer=centre, L=c;
'''
for i in range(ncells):
    #if i in [ncells-6, ncells-4,ncells-3,ncells-2,ncells-1,0,1,2,3, 5, 
    #         ncells/2-6, ncells/2-4,ncells/2-3,ncells/2-2,ncells/2-1,ncells/2,ncells/2+1,ncells/2+2,ncells/2+3, ncells/2+5]:
    if i in [0, ncells/2]: # right of the IP
        seq +=f'''
                IP{i}: mymarker, at = lcell*{i};
                QFT{i}2: qft, at = lcell*{i}+lcell-41.66666666666;
                QDT{i}2: qdt, at = lcell*{i}+lcell-33.33333333333;
                QDT{i}1: qdt, at = lcell*{i}+lcell-25;
                QFT{i}1: qft, at = lcell*{i}+lcell-16.66666666666;
        '''
    elif i in [1, ncells/2+1]: # match cell nr. 1
        seq +=f'''
                QM{i}6: qm6, at = lcell*{i}+lcell*(1-0.75)-lqm/2;
                QM{i}7: qm7, at = lcell*{i}+lcell*(1-0) - lqm/2;
        '''
    elif i in [2, ncells/2+2]: # match cell nr. 2
        seq +=f'''QM{i}8: qm8, at = lcell*{i}+lcell*(1-0.25) - lqm/2;'''
    elif i in [3, ncells/2+3]: # match cell nr. 3
        seq +=f'''     
                QM{i}9: qm9, at = lcell*{i}+lcell*(1-0.75) - lqm/2;
                QM{i}10: qm10, at = lcell*{i}+lcell*(1-0.25)-lqm/2;
        '''
    elif i in [5, ncells/2+5]: # no dipoles
        seq += f'''
        qf{i}: qf, at = {i}*lcell+lq/2;
        xno{i}1: sext1, at = lcell*{i}+ lcell*0.1;
        qd{i}: qd, at = {i}*lcell+lcell*0.5+lq/2;
        xno{i}2: sext2, at = lcell*{i}+ lcell*0.6;
        \n'''
    elif i in [ncells-6, ncells/2-6]: # no dipoles
        seq += f'''
        qf{i}: qf, at = {i}*lcell+lq/2;
        xno{i}1: sext1, at = lcell*{i}+ lcell*0.1;
        qd{i}: qd, at = {i}*lcell+lcell*0.5+lq/2;
        xno{i}2: sext2, at = lcell*{i}+ lcell*0.6;
        \n'''
    elif i in [ncells-4, ncells/2-4]: # match cell nr. 3
        seq +=f'''
                QM{i}1: qm1, at = lcell*{i}+lcell*0.25+lqm/2;
                QM{i}2: qm2, at = lcell*{i}+lcell*0.75+lqm/2;
        ''' 
    elif i in [ncells-3, ncells/2-3]: # match cell nr. 2
        seq +=f'''
                QM{i}3: qm3, at = lcell*{i}+lcell*0.25+lqm/2;
        '''
    elif i in [ncells-2, ncells/2-2]: # match cell nr. 1
        seq +=f'''
                QM{i}4: qm4, at = lcell*{i}+lcell*0+lqm/2;
                QM{i}5: qm5, at = lcell*{i}+lcell*0.75+lqm/2;
        '''
    
    elif i in [ncells-1, ncells/2-1]: # left of the IP
        seq +=f'''
                QFT{i}1: qft, at = lcell*{i}+16.66666666666;
                QDT{i}1: qdt, at = lcell*{i}+25;
                QDT{i}2: qdt, at = lcell*{i}+33.33333333333;
                QFT{i}2: qft, at = lcell*{i}+41.66666666666;
        ''' 
    else:
        seq += f'''
        qf{i}: qf, at = lcell*{i}+lq;
        xno{i}1: sext1, at = lcell*{i}+ lcell*0.1;
        sb{i}1: DIPOL, at = lcell*{i} + lcell*0.2+ld/2;
        sb{i}2: DIPOL, at = lcell*{i} + lcell*0.35+ld/2;
        qd{i}: qd, at = lcell*{i}+lcell*0.5+lq/2;
        xno{i}2: sext2, at = lcell*{i}+ lcell*0.6;
        sb{i}3: DIPOL, at = lcell*{i} + lcell*0.65+ld/2;
        sb{i}4: DIPOL, at = lcell*{i} + lcell*0.8+ld/2;
        \n'''
seq += '''
        endsequence;
'''

beam = '''
        beam, particle=proton, energy=20.0;
'''

# %%
'''
k1 = {k1};
k2 = {k2};

k1q3 = 0.030303030;
k2q3 = -0.030303030;

!k10m = -0.02012455257;
!k9m = 0.02286888419;
!k8m = -0.01958231945;
!k7m = 0.01119300171;
!k6m = -0.02108338403;
k10m = -0.020000721;
k9m = 0.02273259172;
k8m = -0.02270564855;
k7m = 0.01058591628;
k6m = -0.02038114704;

k1m = -0.02012455257;
k2m = 0.02286888419;
k3m = -0.01958231945;
k4m = 0.01119300171;
k5m = -0.02108338403;
'''

# %%
madinput = f'''
        {seq}
        {beam}
        angle = twopi/(4*ncells - 4*(4+1)*2*2);
        k1 = {k1};
        k2 = {k2};
        k1q3 = {k1q3};
        k2q3 = {k2q3};
        
        
        k1m = {k1m};
        k2m = {k2m};
        k3m = {k3m};
        k4m = {k4m};
        k5m = {k5m};
        k6m = {k6m};
        k7m = {k7m};
        k8m = {k8m};
        k9m = {k9m};
        k10m = {k10m};

        use, sequence=collider;
'''

# %%
mad.input(madinput)

# %%
#mad.input('SAVEBETA, LABEL=bini, PLACE=#s;')
#mad.input(f'twiss, betx={0.7},bety={0.7},alfx={0.0},alfy={0.0};')
mad.input('twiss;')
twtable=mad.table.twiss
myTwiss = twtable.dframe()

# %%
fig = plt.figure(figsize=(18,8))
fontsize=15

ax1=plt.subplot2grid((3,3), (0,0), colspan=3, rowspan=1)
plt.plot(myTwiss['s'],0*myTwiss['s'],'k')
DF=myTwiss[(myTwiss['keyword']=='quadrupole')]
#DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='r')
color = 'red'
ax1.set_ylabel('1/f=K1L [m$^{-1}$]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax1.tick_params(axis='y', labelcolor=color)
ax1.tick_params(axis='x', bottom=False,labelbottom=False)
ax1.tick_params(labelsize=fontsize)
plt.ylim(-.1,.1)
plt.grid(True)
#plt.title('Our machine, 2016 Optics, Beam 1, IP5 squeeze, $\\beta_{IP}=$'+format(myTwiss[myTwiss['name'].str.contains('ip5')]['betx'].values[0],'2.2f')+' m, Q=('+format(madx.table.summ.Q1[0],'2.3f')+', '+ format(madx.table.summ.Q2[0],'2.3f')+'), $\\xi$=('+format(madx.table.summ.DQ1[0],'2.3f')+', '+ format(madx.table.summ.DQ2[0],'2.3f')+')')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'blue'
ax2.set_ylabel('$\\theta$=K0L [mrad]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax2.tick_params(axis='y', labelcolor=color)
ax2.tick_params(labelsize=fontsize)
DF=myTwiss[(myTwiss['keyword']=='multipole')]
if thick_dipoles:
    DF=myTwiss[(myTwiss['keyword']=='sbend')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(ax2,aux, height=aux.angle, v_offset=aux.angle/2, color='b')
    plotLatticeSeries(ax2,aux, height=aux.k0l, v_offset=aux.k0l/2, color='b')
plt.ylim(-.2,.2)

plt.subplot2grid((3,3), (1,0), colspan=3, rowspan=2,sharex=ax1)

plot(myTwiss.s,myTwiss.betx,'-b',label='$\\beta_x$')
plot(myTwiss.s,myTwiss.bety,'-r',label='$\\beta_y$')
plot(myTwiss.s,myTwiss.dx,'-g',label='$D_x$')
xlabel('s [m]',fontsize=fontsize)
ylabel('$\\beta$ [m]',fontsize=fontsize)
xticks(fontsize=fontsize)
yticks(fontsize=fontsize)
grid()
plt.ylim(0,4000)
legend(fontsize=fontsize-3,loc='upper right')

# %%
fig.gca().set_xlim(17*lcell,24*lcell)
fig.gca().set_ylim(-2,2)
display(fig)

# %%
mad.input('survey;')
survtable = mad.table.survey
plt.plot(survtable['x'],survtable['z'],'o-k')
plt.axis('square');
plt.xlabel('X [m]')
plt.ylabel('Z [m]')
plt.grid()

# %% [markdown]
# # Chromaticity correction

# %%
mad.input('''
match_chroma: macro={
    MATCH, Sequence=collider;
        VARY, NAME = ks1, STEP = 1e-3;
        VARY, NAME = ks2, STEP = 1e-3;
        GLOBAL, dQ1 = 0.0;
        GLOBAL, dQ2 = 0.0;
        JACOBIAN,CALLS=1000,TOLERANCE=1.0E-18,STRATEGY=3;
    ENDMATCH;
}
''')
mad.input('use, sequence=collider; exec, match_chroma;')

# %%
mad.input('twiss;')
twtable=mad.table.twiss
myTwiss = twtable.dframe()

# %%
fig = plt.figure(figsize=(18,8))
fontsize=15

ax1=plt.subplot2grid((3,3), (0,0), colspan=3, rowspan=1)
plt.plot(myTwiss['s'],0*myTwiss['s'],'k')
DF=myTwiss[(myTwiss['keyword']=='quadrupole')]
#DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='r')
color = 'red'
ax1.set_ylabel('1/f=K1L [m$^{-1}$]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax1.tick_params(axis='y', labelcolor=color)
ax1.tick_params(axis='x', bottom=False,labelbottom=False)
ax1.tick_params(labelsize=fontsize)
plt.ylim(-.1,.1)
plt.grid(True)
#plt.title('Our machine, 2016 Optics, Beam 1, IP5 squeeze, $\\beta_{IP}=$'+format(myTwiss[myTwiss['name'].str.contains('ip5')]['betx'].values[0],'2.2f')+' m, Q=('+format(madx.table.summ.Q1[0],'2.3f')+', '+ format(madx.table.summ.Q2[0],'2.3f')+'), $\\xi$=('+format(madx.table.summ.DQ1[0],'2.3f')+', '+ format(madx.table.summ.DQ2[0],'2.3f')+')')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'blue'
ax2.set_ylabel('$\\theta$=K0L [mrad]', color=color,fontsize=fontsize)  # we already handled the x-label with ax1
ax2.tick_params(axis='y', labelcolor=color)
ax2.tick_params(labelsize=fontsize)
DF=myTwiss[(myTwiss['keyword']=='multipole')]
if thick_dipoles:
    DF=myTwiss[(myTwiss['keyword']=='sbend')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(ax2,aux, height=aux.angle, v_offset=aux.angle/2, color='b')
    plotLatticeSeries(ax2,aux, height=aux.k0l, v_offset=aux.k0l/2, color='b')
    plotLatticeSeries(ax2,aux, height=aux.k2l*2, v_offset=aux.k2l/2*2, color='g')
plt.ylim(-.2,.2)

plt.subplot2grid((3,3), (1,0), colspan=3, rowspan=2,sharex=ax1)

plot(myTwiss.s,myTwiss.betx,'-b',label='$\\beta_x$')
plot(myTwiss.s,myTwiss.bety,'-r',label='$\\beta_y$')
plot(myTwiss.s,myTwiss.dx,'-g',label='$D_x$')
xlabel('s [m]',fontsize=fontsize)
ylabel('$\\beta$ [m]',fontsize=fontsize)
xticks(fontsize=fontsize)
yticks(fontsize=fontsize)
grid()
plt.ylim(0,4000)
legend(fontsize=fontsize-3,loc='upper right')

# %%
