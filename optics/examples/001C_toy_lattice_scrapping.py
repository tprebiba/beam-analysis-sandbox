#%%
import optics.toy_lattice as otl
import numpy as np
import matplotlib.pyplot as plt
import time
import copy
from optics.toy_lattice import generate_full_q_gaussian
import beamtools.fitting as btf


# Lattice
optics_ring = {'alfa_x':-0.14684,'beta_x':5.8096,'mu_x':4.17, 'd_x':1.4687*0, 'dp_x':0, # no dispersion
               'alfa_y':-0.19932,'beta_y':4.3730,'mu_y':4.23, 'd_y':0.0000, 'dp_y':0}
map = otl.one_turn_map(**optics_ring)


# Beam parameters
beta_gamma = 0.519753*1.170526
emit = 0.4535e-6 # normalized
n_part = 30000
beam_params = {'eps_x': emit/beta_gamma, 'eps_y': emit/beta_gamma,
               'dpp_rms': 1.85E-3, 'n_part': n_part, 'xcenter': 0, 'ycenter': 0}
# 0: x, 1: px, 2: y, 3: py, 4: dpp
#p0 = otl.generate_beam(optics_ring, beam_params) # matched gaussian
q = 1.45
#p0,_ = otl.generate_beam_qGaussian(optics_ring, beam_params, q=q) # q-gaussian in one plane
p0,d = otl.generate_full_q_gaussian(optics_ring, beam_params, q=q, beta=1, n_part=beam_params['n_part']) # q-gaussian in both planes


# Scrapper
class myScrapper():
	def __init__(self, scrapper_position, plane):
		self.scrapper_position = scrapper_position
		if plane == 'x':
			self.index = 0
		elif plane == 'y':
			self.index = 2
		self.lost_count = 0
		self.active = True
	
	def activate(self):
		self.active = True

	def deactivate(self):
		self.active = False

	def set_position(self, scrapper_position):
		self.scrapper_position = scrapper_position

	def scrape(self, p):
		if self.active:
			mask = p[self.index] > self.scrapper_position
			if len(mask) > 0:
				p[self.index][mask] = np.nan # self.scrapper_position
				p[self.index+1][mask] = np.nan # 0
				self.lost_count += np.sum(mask)
				print('Scrapped %i particles.'%np.sum(mask))
			else:
				print('No particles scrapped.')
		return p
scrapper_position = 2e-3
Scrapper = myScrapper(scrapper_position, 'y')


# Track
p = copy.deepcopy(p0)
n_turns = 1000
y = np.zeros((n_part,n_turns))
py = np.zeros((n_part,n_turns))
x = np.zeros((n_part,n_turns))
px = np.zeros((n_part,n_turns))
print('Starting tracking....')
st = time.time()
for i in range(n_turns):
	p = map.track(p)
	x[:,i] = p[0]
	px[:,i] = p[1]
	y[:,i] = p[2]
	py[:,i] = p[3]
	p = Scrapper.scrape(p)
et = time.time()
print('Tracked %i particles for %i turns in %1.5f seconds.'%(n_part,n_turns,et-st))


# Plot vertical and horizontal phase space before and after scrapping
# If initial distribution is Gaussian: no dependence after scrapping between planes
# There is a dependence if initial distribution is q-Gaussian
for plane in ['y','x']:
	if plane == 'x':
		pos = x
		mom = px
	elif plane == 'y':
		pos = y
		mom = py

	f, axs = plt.subplots(1,1,figsize=(6,5), facecolor='white')
	fontsize=20
	ax = axs
	ax.set_xlabel(f'{plane} [mm]', fontsize=fontsize)
	ax.set_ylabel(f'$p_{plane}$ [mrad]', fontsize=fontsize)
	ax.tick_params(axis='both', which='major', labelsize=fontsize)
	ax.set_xlim(-10,10)
	ax.set_ylim(-2,2)
	ax_histx = plt.axes([ax.get_position().x0, ax.get_position().y1+0.02, ax.get_position().width, 0.15])
	ax_histx.set_xlim(ax.get_xlim())
	ax_histx.tick_params(axis='both', labelbottom=False, labelleft=False)
	ax_histy = plt.axes([ax.get_position().x1+0.02, ax.get_position().y0, 0.1, ax.get_position().height])
	ax_histy.set_ylim(ax.get_ylim())
	ax_histy.tick_params(axis='both', labelbottom=False, labelleft=False)
	for ii,c in zip([0,99],['blue','red']):
		ax.plot(pos[:,ii]*1e3,mom[:,ii]*1e3, '.', ms=3.0, color=c)
		#ax_histx.hist(y[:,ii]*1e3, bins=200, color=c, range=(-10,10))
		hist,bin = np.histogram(pos[:,ii]*1e3, bins=200, range=(-10,10))
		ax_histx.plot(bin[0:-1],hist, color=c)
		try:
			popt,pcov,popte,x_gaussianfit,y_gaussianfit = btf.makeGaussianFit_5_parameters(bin[0:-1],hist)
			q0 = 0.6
			p0 = [popt[1], q0,  1/popt[2]**2/(5-3*q0), popt[0], popt[3], popt[4]] # mu, q, beta, A, offset, slope
			poptq,pcovq,popteq, x_gaussianfit, y_gaussianfit = btf.makeQGaussianFit(bin[0:-1],hist,p0)
			ax_histx.plot(x_gaussianfit,y_gaussianfit, color=c, lw=1, label='%1.2f'%poptq[1])
			ax_histx.legend()
		except:
			pass
		#ax_histy.hist(py[:,ii]*1e3, bins=200, orientation='horizontal', color=c,range=(-2,2))
		hist,bin = np.histogram(mom[:,ii]*1e3, bins=200, range=(-2,2))
		ax_histy.plot(hist,bin[0:-1], color=c)
	if plane=='y':
		ax.axvline(scrapper_position*1e3, lw=4, color='black')
	#f.tight_layout()
	#plt.show()

# %%
