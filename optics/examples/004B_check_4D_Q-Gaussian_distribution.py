import numpy as np
import matplotlib.pyplot as plt
import beamtools.distributions as btd
import optics.toy_lattice as otl


def check_against_original(q, beta, x_array, bins=700):

    high = 20
    X = x_array
    npoints = 2001
    xs = np.linspace(-1*high,high,npoints)
    qgauss = btd.qgauss(xs, mu=0, q=q, beta=beta, A=1)
    qgauss = np.nan_to_num(qgauss, 0)

    _, _, _ = plt.hist(X, bins=bins, density=True, alpha=0.3, label='histogram(x)')
    plt.plot(xs, qgauss, label='desired q-Gaussian')
    plt.legend(loc='lower left')

    plt.grid()
    plt.xlim([-2, 2])
    plt.xlabel('$\sigma$')    

    
def check_PDF(x, px, y, py, F, g_F):    
    X = np.array(x)
    PX = np.array(px)
    Y = np.array(y)
    PY = np.array(py)

    F_hist= X**2 + Y**2 +PX**2 + PY**2
    r_hist= (X**2 + PX**2)

    _,_,_=plt.hist(r_hist, bins=300 ,density=True, label='$r^2$', alpha=0.5) # need more particles to see if its ok 
    _,_,_=plt.hist(F_hist, bins=300 ,density=True, label='$F$', alpha=0.5)

    plt.plot(F, g, label='$PDF(F)$ or $g(F)$',) 
    plt.xlim([0,15])
    plt.legend()
    plt.grid()
    plt.show()


# Lattice
optics_ring = {'alfa_x':-0.14684,'beta_x':5.8096,'mu_x':4.17, 'd_x':1.4687, 'dp_x':0,
               'alfa_y':-0.19932,'beta_y':4.3730,'mu_y':4.23, 'd_y':0.0000, 'dp_y':0}

# Beam parameters
beta_gamma = 0.519753*1.170526
emit = 0.4535e-6 # normalized
n_part = 50000
beam_params = {'eps_x': emit/beta_gamma, 'eps_y': emit/beta_gamma,
               'dpp_rms': 1.85E-3, 'n_part': n_part, 'xcenter': 0, 'ycenter': 0}
# Matched 4D Q-Gaussian
# 0: x, 1: px, 2: y, 3: py, 4: dpp
q = 1.4
beta = 1
p,d = otl.generate_full_q_gaussian(optics_ring, beam_params, q=q, beta=beta, n_part=beam_params['n_part'])

#check_against_original(q, beta, p.x)
check_against_original(q, beta, d['x'])
plt.show()