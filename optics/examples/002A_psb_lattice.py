import optics.psb_lattice as psb
import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx
import matplotlib.patches as patches


# Loading the lattice
mad = Madx()
twtable = psb.get_MADX_lattice(mad, QH_target = 4.40, QV_target=4.40)
myTwiss=mad.table.twiss.dframe()


# Plotting the optics
# Plotting format based on G. Sterbini's lectures on Optics Design (CAS 2022)
# https://github.com/cerncas/2022Sevrier/blob/main/examples/Lattices.ipynb
def plotLatticeSeries(ax,series, height=1., v_offset=0., color='r',alpha=0.5):
    aux=series
    ax.add_patch(
    patches.Rectangle(
        (aux.s-aux.l, v_offset-height/2.),   # (x,y)
        aux.l,          # width
        height,          # height
        color=color, alpha=alpha))
    return;
fig = plt.figure(figsize=(13,8))
fontsize=14
# Plotting the elements
ax1=plt.subplot2grid((3,3), (0,0), colspan=3, rowspan=1)
plt.plot(myTwiss['s'],0*myTwiss['s'],'k')
DF=myTwiss[(myTwiss['keyword']=='quadrupole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='r')
DF=myTwiss[(myTwiss['keyword']=='multipole')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(plt.gca(),aux, height=aux.k1l, v_offset=aux.k1l/2, color='r')
color = 'red'
ax1.set_ylabel('K1L [m$^{-1}$]', color=color, fontsize=fontsize)
ax1.tick_params(axis='y', labelcolor=color)
ax1.tick_params(labelsize=fontsize)
ax1.set_ylim(-.8,.8)
ax1.set_xlim(-2,twtable.summary.length+2)
ax1.grid(True)
plt.title('CERN Proton Synchrotron Booster, Q1='+format(mad.table.summ.Q1[0],'2.3f')+', Q2='+ format(mad.table.summ.Q2[0],'2.3f'),
	  fontsize=fontsize)
ax2 = ax1.twinx()
color = 'blue'
ax2.set_ylabel('K0L [rad]', color=color, fontsize=fontsize)
ax2.tick_params(axis='y', labelcolor=color, labelsize=fontsize)
ax2.set_ylim(-.8,.8)
DF=myTwiss[(myTwiss['keyword']=='sbend')]
for i in range(len(DF)):
    aux=DF.iloc[i]
    plotLatticeSeries(ax2, aux, height=aux.angle, v_offset=aux.angle/2, color='b')
# Plotting the optics
ax3 = plt.subplot2grid((3,3), (1,0), colspan=3, rowspan=2,sharex=ax1)
ax3.plot(myTwiss['s'],myTwiss['betx'],'b', label='$\\beta_x$')
ax3.plot(myTwiss['s'],myTwiss['bety'],'green', label='$\\beta_y$')
ax3.legend(loc='best',fontsize=fontsize-2)
ax3.set_ylabel('[m]', fontsize=fontsize)
ax3.set_xlabel('s [m]', fontsize=fontsize)
ax3.tick_params(labelsize=fontsize)
ax3.grid(True)
ax3.set_xlim(-2,twtable.summary.length+2)
ax4 = plt.gca().twinx()
gamma_rel = twtable.summary.gamma
beta_rel = np.sqrt(1-1/gamma_rel**2.0)
ax4.plot(myTwiss['s'],myTwiss['dx']*beta_rel,'r', label='$D_x$')
ax4.set_ylabel('$D_x$ [m]', color='r', fontsize=fontsize)
ax4.tick_params(axis='y', labelcolor='r', labelsize=fontsize)
ax4.set_ylim(0,-2)
fig.tight_layout()
plt.show()
