#%%
import optics.toy_lattice as otl
import numpy as np
import matplotlib.pyplot as plt
import time
import nafflib as NAFFlib

#%%
# Lattice
Qyset = 4.23
optics_ring = {'alfa_x':0.0,'beta_x':1.0,'mu_x':4.331, 'd_x':0.0, 'dp_x':0,
               'alfa_y':0.0,'beta_y':1.0,'mu_y':4.448, 'd_y':0.0, 'dp_y':0}
map = otl.one_turn_map(**optics_ring)

#%%
# Beam parameters
n_part = 50
# 0: x, 1: px, 2: y, 3: py, 4: dpp
p = otl.particle_line(n_part, xmax=0.040, ymax=0.040, pxmax=0.040, pymax=0.040)

#%%
# Perturbations
L = 1.0
k1 = -6.15363e-4*100*0 # quadrupolar error
k2 = 50*0.00451*100 # sextupolar error

#%%
# Track
n_turns = 2000
x = np.zeros((n_part,n_turns))
px = np.zeros((n_part,n_turns))
y = np.zeros((n_part,n_turns))
py = np.zeros((n_part,n_turns))
print('Started tracking....')
st = time.time()
# Include_space charge
n_sckicks = 89*0
if n_sckicks>0:
	optics_ring['mu_y'] = optics_ring['mu_y']/n_sckicks
	optics_ring['mu_x'] = optics_ring['mu_x']/n_sckicks
	map = otl.one_turn_map(**optics_ring)
	Nb = 80e10*0
	sigma = 0.005 # beam size in m
p0 = p
y[:,0] = p0[2]
py[:,0] = p0[3]
for i in range(1,n_turns):
	p0[2] = y[:,i-1]
	p0[3] = py[:,i-1]
	if n_sckicks>0:
		for j in range(n_sckicks):
			p1 = map.track(p0)
			p1[3] = p1[3] - otl.sckick(p1[2],sigma,Nb)/n_sckicks
			p1[1] = p1[1] - otl.sckick(p1[0],sigma,Nb)/n_sckicks
			p0,p1 = p1,p0
	else:
		p1 = map.track(p0)
		p0,p1 = p1,p0

	x[:,i] = np.copy(p0[0])
	px[:,i] = np.copy(p0[1] - k1*L*p1[0] - 0.5*k2*L*p1[0]**2.0)
	y[:,i] = np.copy(p0[2])
	py[:,i] = np.copy(p0[3]- k1*L*p1[2] - 0.5*k2*L*p1[2]**2.0)
	
et = time.time()
print('Tracked %i particles for %i turns in %1.5f seconds.'%(n_part,n_turns,et-st))


#%%
# Tunes
nn = 140
#qx = NAFFlib.multiparticle_tunes(y[:,0:nn]-1.j*py[:,0:nn],order=2,interpolation=0)
qy = NAFFlib.multiparticle_tunes(y[:,0:nn])


#%%
# Plots
fig, axs = plt.subplots(1,2,figsize=(12,5), facecolor='w')
fontsize=24
xlim = 12
ms = 0.5

# Phase space
for ax, plane in zip(axs, ['x','y']):
	#ax = axs[0]
	ax.set_xlabel(f'${plane}$ [mm]', fontsize=fontsize) 
	ax.set_ylabel(f'$p_{plane}$ [mrad]', fontsize=fontsize)
	ax.tick_params(labelsize=fontsize)
	ax.set_ylim(-xlim,xlim)
	ax.set_xlim(-xlim,xlim)
	ax.grid(True)

for i in range(n_part):
	axs[0].plot(x[i,:]*1e3, px[i,:]*1e3, '.', ms=ms, c='blue')
	axs[1].plot(y[i,:]*1e3, py[i,:]*1e3, '.', ms=ms, c='blue')

fig.tight_layout()
plt.show()

fig, ax = plt.subplots(1,1,figsize=(6,5), facecolor='w')
xlim = 30
ax.set_xlabel(f'x [mm]', fontsize=fontsize) 
ax.set_ylabel(f'y [mm]', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
ax.set_ylim(-xlim,xlim)
ax.set_xlim(-xlim,xlim)
ax.grid(True)
for i in range(n_part):
	ax.plot(x[i,:]*1e3, y[i,:]*1e3, '.', ms=ms, c='blue')
fig.tight_layout()
plt.show()
# %%
