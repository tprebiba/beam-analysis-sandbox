# A set of functions to handle the LEIR mad-x lattice

def get_twiss_table(mad, centre=False):

    mad.input('twiss, centre=%s;'%centre)
    twtable = mad.table.twiss
    return twtable


def get_MADX_lattice(mad, year='2021',
		     makethin=False, scenario='scenarios/nominal/1_flat_bottom/leir_fb_nominal.str',
		     beam = 'scenarios/nominal/1_flat_bottom/leir_fb_nominal.beam',
		     load='eos', slice=1):
	'''
	Loads the PS lattice in MAD-X matched to the desired working point
	Returns the ks and the twiss table
	'''
	if load=='local':
		mad.chdir('/home/tprebiba/eos/Optics/PS_Optics/acc-models-leir-%s/'%(year))
		mad.call('leir.seq')
		#mad.call('new_leir_seq.seq')
		mad.call('leir.dbx')
		mad.call(scenario)
		mad.call(beam)
	else:
		mad.chdir('/eos/project/a/acc-models/public/leir/%s'%year)
		mad.call('leir.seq')
		#mad.call('new_leir_seq.seq')
		mad.call('leir.dbx')
		mad.call(scenario)
		mad.call(beam)
		#mad.input('use,sequence=leir')
		mad.input('brho := beam->pc * 3.3356 * (208./54.);')
		mad.call('_scripts/macros.madx')

	if makethin:
		mad.use("leir")
		mad.input("seqedit, sequence=leir;")
		mad.input("flatten;")
		mad.input("endedit;")
		mad.use("leir")
		mad.input("select, flag=makethin, slice=%i, thick=false;"%slice)
		mad.input("makethin, sequence=leir, style=teapot, makedipedge=false;")
		mad.use("leir")

	#twtable = get_twiss_table(mad)
	twtable = global_coupling_correction_matching(mad)
	return twtable

def global_coupling_correction_matching(mad, QH_target=1.82, QV_target=2.72):

    mad.input(f'''
	      TuneH = {QH_target}; TuneV = {QV_target};
	      ! Optical parameters at the center of the electron cooler. ;
	      DiscEC = -0.00; BetHEC = 5.00; BetVEC = 5.00;
	      use, sequence=LEIR; exec, global_correction;
	      ''')
    twtable = get_twiss_table(mad)
    return twtable
