import numpy as np
import copy
from abel.direct import direct_transform
import beamtools.distributions as btdist
import beamtools.utility as btu


class Particles:
    # A simple object to hold the particles corrdinates
    def __init__(self, x:np.ndarray, px:np.ndarray, y:np.ndarray, py:np.ndarray, delta:np.ndarray):
        if ((not isinstance(x,np.ndarray)) or (not isinstance(px,np.ndarray)) or \
            (not isinstance(y,np.ndarray)) or (not isinstance(py,np.ndarray)) or \
            (not isinstance(delta,np.ndarray))):
            raise TypeError("Particle coordinates must be a NumPy arrays")
        self.x = x
        self.px = px
        self.y = y
        self.py = py
        self.delta = delta # delta p over p

    def __getitem__(self, index):
        if index == 0:
            return self.x
        elif index == 1:
            return self.px
        elif index == 2:
            return self.y
        elif index == 3:
            return self.py
        elif index == 4:
            return self.delta
        else:
            raise IndexError("Particle index out of range")
    
    def __setitem__(self, index, value):
        if index == 0:
            self.x = value
        elif index == 1:
            self.px = value
        elif index == 2:
            self.y = value
        elif index == 3:
            self.py = value
        elif index == 4:
            self.delta = value
        else:
            raise IndexError("Particle index out of range")


class one_turn_map():
    # A simple 4D one-turn map
    def __init__(self, alfa_x=0, beta_x=1, mu_x=4.40, alfa_y=0, beta_y=1, mu_y=4.45,
                 d_x=0, dp_x=0, co_x=0, cop_x=0, d_y=0, dp_y=0, co_y=0, cop_y=0):
        self.M_x = self._init_map(alfa_x, beta_x, mu_x, d_x, dp_x)
        self.M_y = self._init_map(alfa_y, beta_y, mu_y, d_y, dp_y)
        self.co_in = np.array([co_x, cop_x, co_y, cop_y, 0])
        self.co_out = np.array([co_x, cop_x, co_y, cop_y, 0])

    def _init_map(self, alfa, beta, mu, d, dp):
        M = np.zeros((3,3))
        s = np.sin(2*np.pi*mu)
        c = np.cos(2*np.pi*mu)
        gamma = (1+alfa**2)/beta
        M = np.zeros((3,3))
        M[0,0] = c + alfa*s
        M[0,1] = beta*s
        M[0,2] = (1-c-alfa*s)*d - beta*s*dp
        M[1,0] = -gamma*s
        M[1,1] = c - alfa*s
        M[1,2] = gamma*s*d + (1-c+alfa*s)*dp
        M[2,2] = 1
        return M

    def track(self,p):
        try:
            # if custom Particles object
            pnew = copy.deepcopy(p)
        except:
            # if Particles object of Xpart
            pnew = p.copy() 
        [pnew.x,pnew.px,pnew.delta] = (np.tensordot(self.M_x, (np.array([p.x,p.px,p.delta]).T-self.co_in[[0,1,4]]), axes=(1,-1)).T + self.co_out[[0,1,4]]).T
        [pnew.y,pnew.py,pnew.delta] = (np.tensordot(self.M_y, (np.array([p.y,p.py,p.delta]).T-self.co_in[[2,3,4]]), axes=(1,-1)).T + self.co_out[[2,3,4]]).T
        return pnew


def generate_beam(optics_ring, beam_params):
    # 5-D Gaussian beam given matched to the Twiss parameters

    alfa_x = optics_ring['alfa_x']
    beta_x = optics_ring['beta_x']
    d_x    = optics_ring['d_x']
    dp_x   = optics_ring['dp_x']
    alfa_y = optics_ring['alfa_y']
    beta_y = optics_ring['beta_y']
    d_y    = optics_ring['d_y']
    dp_y   = optics_ring['dp_y']

    eps_x   = beam_params['eps_x']
    eps_y   = beam_params['eps_y']
    dpp_rms = beam_params['dpp_rms']
    n_part  = int(beam_params['n_part'])
    xcenter = beam_params['xcenter']
    ycenter = beam_params['ycenter']

    particles = Particles(*np.zeros((5,n_part)))
    particles.delta = np.random.normal(0,dpp_rms,n_part)
    particles.x = np.random.normal(xcenter,np.sqrt(eps_x*beta_x),n_part) + particles.delta*d_x
    particles.px = np.random.normal(0,np.sqrt(eps_x/beta_x),n_part) + particles.delta*dp_x - alfa_x*particles.x/beta_x
    particles.y = np.random.normal(ycenter,np.sqrt(eps_y*beta_y),n_part) + particles.delta*d_y
    particles.py = np.random.normal(0,np.sqrt(eps_y/beta_y),n_part) + particles.delta*dp_y - alfa_y*particles.y/beta_y

    return particles


def generate_beam_qGaussian(optics_ring, beam_params, q):
    # Gaussian in dp_p, x, px and Q-Gaussian in y-py matched to the Twiss parameters 
    # Based on Abel analysis of Elleanor and Guido
    # Generates unrealistic real space distributions?
    
    particles = generate_beam(optics_ring, beam_params)

    beamsize_y = 1 #np.sqrt(beta_y)*np.sqrt(eps_y)
    R_MAX = 12*beamsize_y # in sigma 
    NR = 5000  # nb of grid points 
    r = np.linspace(0, R_MAX, NR+1)
    r_plus= np.linspace(0, R_MAX, int(beam_params['n_part'])+1)
    
    beta_init = 1/((5-3*1.0)*beamsize_y**2.0) # 3*1.0 to be checked again
    init_profile = btdist.qgauss(r, mu=0, q=q, beta=beta_init, A=1)
    init_distri = direct_transform(init_profile, dr=np.diff(r)[0], direction="inverse", correction=True)*2*np.pi*r
    init_distri_plus=np.interp(r_plus, r, init_distri)
    rho_r=np.random.choice(r_plus, p=init_distri_plus/sum(init_distri_plus), size=int(beam_params['n_part']))
    rho_phi=np.random.rand(len(rho_r))*2*np.pi
    Y=rho_r*np.cos(rho_phi)
    YP=rho_r*np.sin(rho_phi)
    
    particles.y, particles.py = np.array([[np.sqrt(optics_ring['beta_y'])*np.sqrt(beam_params['eps_y']), 0],
                                          [-optics_ring['alfa_y']/np.sqrt(optics_ring['beta_y'])*np.sqrt(beam_params['eps_y']), 
                                           np.sqrt(beam_params['eps_y'])/np.sqrt(optics_ring['beta_y'])]])@np.array([Y,YP])
    
    return particles, {'r':r, 'init_profile':init_profile, 'init_distri': init_distri,
                       'r_plus':r_plus, 'init_distri_plus':init_distri_plus, 'Y':Y, 'YP':YP}


# Function of Elleanor
def generate_full_q_gaussian(optics_ring, beam_params, q, beta, n_part):
    '''
    Generates a 4D distribution with n_part particles for q-Gaussian with 1<q<3. 
    args: q, beta, n_part
    returns: x, px, y, py
    '''
    
    f_F, F = btu.ABEL_f_F(q, beta) #  4D distribution
    g_F = btu.ABEL_g_F(f_F, F) # PDF of 4D distribution
    cdf_g = btu.ABEL_cdf_g(g_F, F) # CDF
    F_G = btu.ABEL_F_G(n_part, cdf_g, F) # Inverse function
    
    A_x, A_y = btu.random_A(F_G) # random generator distributed like F_G
    
    x = []
    y = []
    px = []
    py = []

    for i in range(len(F_G)):
        beta_x = np.random.uniform(0,2*np.pi, 1)
        beta_y = np.random.uniform(0,2*np.pi, 1)
        x.append(A_x[i]*np.cos(beta_x)[0])
        px.append(A_x[i]*(-np.sin(beta_x))[0])
        y.append(A_y[i]*(-np.cos(beta_y))[0])
        py.append(A_y[i]*(-np.sin(beta_y))[0])
    
    #return x, px, y, py 
    
    #!!!! this part to be checked again !!!!!
    particles = generate_beam(optics_ring, beam_params)
    particles.x, particles.px = np.array([[np.sqrt(optics_ring['beta_x'])*np.sqrt(beam_params['eps_x']), 0],
                                          [-optics_ring['alfa_x']/np.sqrt(optics_ring['beta_x'])*np.sqrt(beam_params['eps_x']), 
                                           np.sqrt(beam_params['eps_y'])/np.sqrt(optics_ring['beta_y'])]])@np.array([np.array(x),np.array(px)])
    particles.y, particles.py = np.array([[np.sqrt(optics_ring['beta_y'])*np.sqrt(beam_params['eps_y']), 0],
                                          [-optics_ring['alfa_y']/np.sqrt(optics_ring['beta_y'])*np.sqrt(beam_params['eps_y']), 
                                           np.sqrt(beam_params['eps_y'])/np.sqrt(optics_ring['beta_y'])]])@np.array([np.array(y),np.array(py)])
    
    return particles, {'x':x, 'px':px, 'y':y, 'py':py}


def particle_line(n_part,xmax=2,ymax=2,pxmax=0,pymax=0):
    # Particles distributed in a straight line    
    return Particles(
                     np.linspace(0.0001,xmax,n_part),
                     np.linspace(0.0001,pxmax,n_part),
                     np.linspace(0.0001,ymax,n_part),
                     np.linspace(0.0001,pymax,n_part),
                     np.zeros(n_part)
                     )


def sckick(x,sigma,Nb,beta_rel=0.5198,gamma_rel=1.171):
    # A simple space charge kick ("frozen")
    e_radius = 2.82e-15 # classical electron radius in m
    K = 2*e_radius*Nb/beta_rel**2.0/gamma_rel**2.0/np.sqrt(2*np.pi)/sigma
    X = x/np.sqrt(2)/sigma
    return -K*x*(1-np.exp(-X**2))/X**2.0