# A set of functions to handle the SPS mad-x lattice

def get_twiss_table(mad, centre=False):

    mad.input('twiss, centre=%s;'%centre)
    twtable = mad.table.twiss
    return twtable


def get_MADX_lattice(mad, year='2021',
		     makethin=False, scenario='scenarios/lhc_proton/0_q20/sps_lhc_q20.str',
		     load='eos', slice=1):
    '''
    Loads the PS lattice in MAD-X matched to the desired working point
    Returns the ks and the twiss table
    '''
    if load=='local':
    	mad.chdir('/home/tprebiba/eos/Optics/SPS_Optics/acc-models-sps-%s/'%(year))
    	mad.call('SPS_LS2_2020-05-26.seq')
    	#scenario = 'strengths/lhc_q20.str'
    	mad.call(scenario)
    else:
    	mad.chdir('/eos/project/a/acc-models/public/sps/%s'%year)
    	mad.call('SPS_LS2_2020-05-26.seq')
    	mad.call(scenario)
    mad.input('''
	      beam;
	      use,sequence=sps;
	      ''')
    mad.call('toolkit/macro.madx')

    if makethin:
        mad.use("sps")
        mad.input("seqedit, sequence=sps;")
        mad.input("flatten;")
        mad.input("endedit;")
        mad.use("sps")
        mad.input("select, flag=makethin, slice=%i, thick=false;"%slice)
        mad.input("makethin, sequence=sps, style=teapot, makedipedge=false;")
        mad.use("sps")

    twtable = get_twiss_table(mad)
    return twtable

def match_tune(mad, QH_target, QV_target):

    mad.input('QH_target=%f; QV_target=%f'%(QH_target, QV_target))
    mad.input('exec, sps_match_tunes(QH_target, QV_target)')
    twtable = get_twiss_table(mad)
    return twtable
