# A set of functions to handle the PS mad-x lattice

def get_twiss_table(mad, centre=False):

    mad.input('twiss, centre=%s;'%centre)
    twtable = mad.table.twiss
    return twtable


def get_MADX_lattice(mad, year='2021',
		     makethin=False, scenario='scenarios/lhc/1_flat_bottom/ps_fb_lhc.str',
		     beam = 'scenarios/lhc/1_flat_bottom/ps_fb_lhc.beam',
		     load='eos', slice=1):
    '''
    Loads the PS lattice in MAD-X matched to the desired working point
    Returns the ks and the twiss table
    '''
    if load=='local':
        mad.chdir('/home/tprebiba/eos/Optics/PS_Optics/acc-models-sps-%s/'%(year))
        mad.call('ps_mu.seq')
        mad.call('ps_ss.seq')
        mad.call(scenario)
        mad.call(beam)
    else:
        mad.chdir('/eos/project/a/acc-models/public/ps/%s'%year)
        if year == '2023':
            mad.call('ps.seq')
        else:
            mad.call('ps_mu.seq')
            mad.call('ps_ss.seq')
        mad.call(scenario)
        mad.call(beam)
    mad.input('brho := beam->pc*3.3356;')
    mad.input('use,sequence=ps')
    mad.call('_scripts/macros.madx')

    if makethin:
        mad.use("ps")
        mad.input("seqedit, sequence=ps;")
        mad.input("flatten;")
        mad.input("endedit;")
        mad.use("ps")
        mad.input("select, flag=makethin, slice=%i, thick=false;"%slice)
        mad.input("makethin, sequence=ps, style=teapot, makedipedge=false;")
        mad.use("ps")

    twtable = get_twiss_table(mad)
    return twtable

def match_tune(mad, QH_target, QV_target):

    mad.input('QH_target=%f; QV_target=%f'%(QH_target, QV_target))
    mad.input('exec, match_tunes(QH_target, QV_target,0,0)')
    twtable = get_twiss_table(mad)
    return twtable
