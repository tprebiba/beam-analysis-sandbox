# A set of functions to handle the psb mad-x lattice

def get_twiss_table(mad, centre=False):

    #mad.input('exec, ptc_twiss_macro(2,0,0);')
    #twtable = mad.table.ptc_twiss
    # after madx version >= 5.06.01
    #a
    mad.input('twiss, centre=%s;'%centre)
    twtable = mad.table.twiss
    return twtable


def get_MADX_lattice(mad, QH_target, QV_target,
		     year='2021', makethin=False,
		     scenario='psb_inj_lhc_kmod.madx' , load = 'eos',
		     refer='entry', remove_rbend=False, slice=1):
    '''
    Loads the PSB lattice in MAD-X matched to the desired working point
    Returns the ks and the twiss table
    '''

    if refer=='center':
    	# PSB lattice at (4.40, 4.45)
    	# The tune correction strengths (kBRQFCORR, kBRQDCORR) and the beta-beating correction strengths (kBRQD3CORR, kBRQD14CORR) are zero
    	# The BSW/KSW are unasigned internally
    	mad.chdir('/home/tprebiba/eos/Optics/PSB_Optics/acc-models-psb-%s/operation/k_modulation/'%year)
    	mad.call('psb_inj_lhc_kmod_referCENTER_forXSuite.madx')
    else:
    	if load == 'local':
    		mad.chdir('/home/tprebiba/eos/Optics/PSB_Optics/acc-models-psb-%s/operation/k_modulation/'%year)
    		mad.call(scenario)
    	else:
    		mad.chdir('/eos/project/a/acc-models/public/psb/%s/operation/k_modulation/'%year)
    		mad.call(scenario)

    if makethin:
        mad.use("psb1")
        mad.input("seqedit, sequence=psb1;")
        mad.input("flatten;")
        mad.input("endedit;")
        mad.use("psb1")
        mad.input("select, flag=makethin, slice=%i, thick=false;"%slice)
        mad.input("makethin, sequence=psb1, style=teapot, makedipedge=false;")
        mad.use("psb1")
    if remove_rbend:
    	mad.use("psb1")
    	mad.input("select, flag=seqedit, class=BR_MSBSW002;")
    	mad.input("select, flag=seqedit, class=BR_MSBSW003;")
    	mad.input("select, flag=seqedit, class=BR_MSBSW004;")
    	mad.input("select, flag=seqedit, class=BR_MSMIF001;")
    	mad.input("seqedit, sequence=psb1;")
    	mad.input("flatten;")
    	mad.input("remove, element=selected;")
    	mad.input("flatten;")
    	mad.input("endedit;")
    	mad.use("psb1;")

    # Tune matching using TWISS (madx version >= 5.06.01)
    mad.input('QH_target=%f; QV_target=%f;'%(QH_target, QV_target))
    mad.input('exec, psb_match_tunes(QH_target, QV_target);')
    twtable = get_twiss_table(mad)

    return twtable


# to be improved
def get_MADX_old_lattice(mad,QH_target, QV_target):

    mad.chdir('/home/tprebiba/eos/Optics/PSB_Optics_old/')
    mad.call('PSB_for_pyOrbit.madx')
    twtable = get_twiss_table(mad)

    return twtable


def assign_chicane_perturbations(QH_target, QV_target, mad, assign_chicane_tablerow, use_BSW_alignment):
    '''
    Assigns the injection chicane perturbations (only BSW)
    '''

    mad.input('readtable, file="BSW_collapse.tfs";')
    mad.input('SETVARS,TABLE=BSWTABLE,ROW=%d;'%assign_chicane_tablerow)
    mad.input('exec, assign_BSW_strength;')
    mad.input('use_BSW_alignment = %s;'%use_BSW_alignment)
    mad.input('if (use_BSW_alignment==1){exec, assign_BSW_alignment;};')

    #mad.input('QH_target=%f; QV_target=%f;'%(QH_target, QV_target))
    #mad.input('exec, psb_match_tunes(QH_target, QV_target)')



def set_model_strengths(QH_target, QV_target, mad, ctime):

    mad.input('kBRQD = kBRQD_ref;')
    mad.input('kBRQF = kBRQF_ref;')
    mad.input('kBRQD3CORR = 0;')
    mad.input('kBRQD14CORR = 0;')
    mad.input('kBRQDCORR = 0;')
    mad.input('kBRQFCORR = 0;')

    if ((ctime>=275) & (ctime<=280)):
        row_in_BSW_table  = int((ctime-275)*10+1)
        use_BSW_alignment = 1
        assign_chicane_perturbations(QH_target, QV_target, mad, row_in_BSW_table, use_BSW_alignment)
    else:
        pass



def get_twiss_specific_element(mad, twtable, element):
    try:
        indx = [i for i,n in enumerate(twtable['name']) if element in n]
        gamma_rel = twtable.summary.gamma
        beta_rel = np.sqrt(1-1/gamma_rel**2.0)
        d = {'name': twtable['name'][indx[0]],
             'gamma_rel': gamma_rel, 'beta_rel': beta_rel,
             'q1': twtable.summary.q1, 'q2': twtable.summary.q2,
             'dq1': twtable.summary.dq1*beta_rel, 'dq2': twtable.summary.dq2*beta_rel,
             's': twtable['s'][indx[0]],
             'betx': twtable['betx'][indx[0]], 'bety': twtable['bety'][indx[0]],
             'alfx': twtable['alfx'][indx[0]], 'alfy': twtable['alfy'][indx[0]],
             'dx': twtable['dx'][indx[0]]*beta_rel, 'dy': twtable['dy'][indx[0]],
             'mux': twtable['mux'][indx[0]], 'muy': twtable['muy'][indx[0]]}
        return d
    except Exception as ex:
        pass
    return
