from setuptools import setup, find_packages

#########
# Setup #
#########

setup(
    name='optics',
    version='0.0.1',
    description='A set of functions to handle the model optics of PSB, PS and SPS.',
    url='',
    author='Tirsi Prebibaj',
    packages=find_packages(),
    install_requires=[
        'numpy',
        ]
    )

