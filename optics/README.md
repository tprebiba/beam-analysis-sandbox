A set of functions to load the PSB, PS and SPS optics using cpymad. 

Install using:
pip install git+https://gitlab.cern.ch/tprebiba/beam-analysis-sandbox.git#subdirectory=optics

Contact: 
tirsi.prebibaj@cern.ch
