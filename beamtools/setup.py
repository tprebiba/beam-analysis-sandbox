from setuptools import setup, find_packages

#########
# Setup #
#########

setup(
    name='beamtools',
    version='0.0.1',
    description='A set of tools for beam dynamics data analysis.',
    url='https://gitlab.cern.ch/tprebiba/beam-analysis-sandbox.git#subdirectory=beamtools',
    author='Tirsi Prebibaj',
    packages=find_packages(),
    install_requires=[
        'numpy>=1.23.4',
        'scipy>=1.9.3',
        #'math',
        'pandas>=2.2.1',
        'pyarrow>=11.0.0',
        #'sklearn',
        'scikit-learn>=1.1.3',
        'matplotlib>=3.8.2',
        #'pylab',
        ]
    )

