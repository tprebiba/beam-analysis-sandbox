A set of tools for beam dynamics data analysis.

Install using:
pip install git+https://gitlab.cern.ch/tprebiba/beam-analysis-sandbox.git#subdirectory=beamtools

Contact:
tirsi.prebibaj@cern.ch
