import numpy as np

# MAKERULE

c_matrix = (
    # horizontal
    np.array([[0.99991100, 3.3021790],
              [-7.5700e-5, 0.9998390]]),

    # vertical
    np.array([[0.97287200, 3.2698120],
              [-0.0229030, 0.9509070]])
)


# Difference that should be applied in K or L in F /0.99905935 --> corresponds to 0.00047412911004740543 that should be subtracted from the adjusent drifts

# Difference that should be applied in K or L in D /1.002529 --> corresponds to 0.0011086159402869944 that should be added to the adjusent drifts. 

def v_matrix(k_f, k_d):
    #l_3 = 0.65300-0.00047412911004740543/2+0.0011086159402869944/2
    #l_f = 0.50357/0.99905935
    #l_d = 0.43947/1.002529

    l_3 = 0.65300
    l_f = 0.50357
    l_d = 0.43947

    sqrt_k_f = np.sqrt(k_f)
    sqrt_k_d = np.sqrt(-k_d)

    # horizontal
    a = np.cosh(sqrt_k_d * l_d)
    b = np.sinh(sqrt_k_d * l_d) / sqrt_k_d
    c = np.sinh(sqrt_k_d * l_d) * sqrt_k_d
    d = np.cos(sqrt_k_f * l_f)
    f = np.sin(sqrt_k_f * l_f) / sqrt_k_f
    g = -np.sin(sqrt_k_f * l_f) * sqrt_k_f
    matr_h = np.array([[a * d + (a * l_3 + b) * g, a * f + (a * l_3 + b) * d],
                       [c * d + (c * l_3 + a) * g, c * f + (c * l_3 + a) * d]])

    # vertical
    a = np.cos(sqrt_k_d * l_d)
    b = np.sin(sqrt_k_d * l_d) / sqrt_k_d
    c = -np.sin(sqrt_k_d * l_d) * sqrt_k_d
    d = np.cosh(sqrt_k_f * l_f)
    f = np.sinh(sqrt_k_f * l_f) / sqrt_k_f
    g = np.sinh(sqrt_k_f * l_f) * sqrt_k_f
    matr_v = np.array([[a * d + (a * l_3 + b) * g, a * f + (a * l_3 + b) * d],
                       [c * d + (c * l_3 + a) * g, c * f + (c * l_3 + a) * d]])

    return matr_h, matr_v


def calc_q(k_f, k_d):
    tunes = ()
    for c, v in zip(c_matrix, v_matrix(k_f, k_d)):  # iterate over two-plane tuples
	#cell = v @ c
        cell = np.matmul(v , c)

        tune = (8 / np.pi) * np.arccos(cell[0, 0] * cell[1, 1] + cell[1, 0] * cell[0, 1])
        tunes += (tune,)
    return tunes


def tune_eq(qx, qy):
    target = np.array([qx, qy])
    return lambda k: np.array(calc_q(k[0], k[1])) - target

def I_of_k(k,cal,lq):
    Brho = 2320.5e-4*8.23887
    g_k = k*Brho
    I_k = g_k/(cal/lq)
    return I_k

def k_of_I(I_k,cal,lq):
    Brho = 2320.5e-4*8.23887
    g_k = I_k*(cal/lq)
    return g_k/Brho

calF_dict = {'internal': 6.9915e-4, 'external': 7.00231e-4}
calD_dict = {'internal': 1.2245e-3, 'external': 1.2241e-3}
l_f = 0.50357
l_d = 0.43947
lqd_dict = {'R1': 0.878, 'R2': 0.8784, 'R3': 0.8784, 'R4': 0.878} # divide by two
lqf_dict = {'R1': 0.503, 'R2': 0.5015, 'R3': 0.5015, 'R4': 0.503}