import scipy.constants as const
# Parameters concerning PS Booster (at extraction energy) and BT-BTM Transfer line

# Relativistic Parameters at PSB extraction energy
beta_rel  = 0.9160; 
gamma_rel = 2.4925;
Ekin      = 1.4000; # GeV
Energy    = 2.3380; # GeV
p         = 2.1410; # GeV
freq_initial=1746270 # frequency in Hz
gamma_tr=4.0510824243
compaction_factor_PSB=1/(gamma_tr)**2 #for gamma 4.05
gamma_extr_PSB=(1400+938.27)/938.27
eta_PSB=compaction_factor_PSB-1/(gamma_extr_PSB**2)


# The distances between the SEM grids is 2.500 m according to layout at the BTM line
# They have been corrected according to the survey measurements.
La = 0.000;      
Lb = La + 2.485; 
Lc = Lb + 2.487; 


# measured dispersions for the SEMGrids (by Fanouria and Tirsi 10/2018) in meters
dispersion_measured_SEM = {'r1_pH_SEM1': 1.48755, 'r1_pH_SEM2': 0.03710, 'r1_pH_SEM3': 1.28909,  #lin. fit: SEM2 0.09009
                           'r2_pH_SEM1': 1.18919, 'r2_pH_SEM2': 0.06463, 'r2_pH_SEM3': 1.22983,  #lin. fit: SEM2 0.02567
                           'r3_pH_SEM1': 1.12082, 'r3_pH_SEM2': 0.08989, 'r3_pH_SEM3': 1.20063,  #lin. fit: SEM2 0.04519
                           'r4_pH_SEM1': 1.44343, 'r4_pH_SEM2': 0.02153, 'r4_pH_SEM3': 1.26820,  #lin. fit: SEM2 0.08000
                           'r1_pV_SEM1': 0.42521, 'r1_pV_SEM2': 0.00389,'r1_pV_SEM3': 0.47574,
                           'r2_pV_SEM1': 0.29362, 'r2_pV_SEM2': 0.06870,'r2_pV_SEM3': 0.13049,
                           'r3_pV_SEM1': 0.02758, 'r3_pV_SEM2': 0.01763,'r3_pV_SEM3': 0.00186,
                           'r4_pV_SEM1': 0.19978, 'r4_pV_SEM2': 0.02814,'r4_pV_SEM3': 0.30191,}
dispersion_measured_SEM_errors = {'r1_pH_SEM1': 0.047,  'r1_pH_SEM2': 0.0010, 'r1_pH_SEM3': 0.011,  #lin. fit: SEM2 0.09009
                                  'r2_pH_SEM1': 0.029,  'r2_pH_SEM2': 0.0055, 'r2_pH_SEM3': 0.0097, #lin. fit: SEM2 0.02567
                                  'r3_pH_SEM1': 0.0032, 'r3_pH_SEM2': 0.0065, 'r3_pH_SEM3': 0.010,  #lin. fit: SEM2 0.04519
                                  'r4_pH_SEM1': 0.060,  'r4_pH_SEM2': 0.0071, 'r4_pH_SEM3': 0.013,  #lin. fit: SEM2 0.08000
                                  'r1_pV_SEM1': 0.0052, 'r1_pV_SEM2': 0.0042, 'r1_pV_SEM3': 0.0076,
                                  'r2_pV_SEM1': 0.0017, 'r2_pV_SEM2': 0.0012, 'r2_pV_SEM3': 0.0049,
                                  'r3_pV_SEM1': 0.0017, 'r3_pV_SEM2': 0.0026, 'r3_pV_SEM3': 0.0049,
                                  'r4_pV_SEM1': 0.0009, 'r4_pV_SEM2': 0.0010, 'r4_pV_SEM3': 0.0022}

# measured dispersions for the WS (by Fanouria and Tirsi 10/2018) in meters
dispersion_measured_WS = {'r1_pH': 1.38284,'r1_pV': -0.08737,
                          'r2_pH': 1.37712,'r2_pV': -0.09601,
                          'r3_pH': 1.37321,'r3_pV': -0.08379,
                          'r4_pH': 1.37192,'r4_pV': -0.04245}
# model dispersion for the WS
dispersion_model_WS = {'r1_pH': 1.465799, 'r1_pV': 0.00000,
                       'r2_pH': 1.465799, 'r2_pV': 0.00000,
                       'r3_pH': 1.465799, 'r3_pV': 0.00000,
                       'r4_pH': 1.465799, 'r4_pV': 0.00000}
# model beta optics at the WS
beta_optics_model_WS = {'r1_pH': 5.699084, 'r1_pV': 4.251833,
                        'r2_pH': 5.699084, 'r2_pV': 4.251833,
                        'r3_pH': 5.699084, 'r3_pV': 4.251833,
                        'r4_pH': 5.699084, 'r4_pV': 4.251833}

# measured dispersions for the SEM Grids and the dispersion free optics
dispersion_measured_SEM_disp_free = {'r3_pH_SEM1': 0.01491,'r3_pH_SEM2': 0.00632,'r3_pH_SEM3': 0.01251}


# Measured 2021 at the LIU WS from GP
DxLIUWSH = {'R1': -1.446, 'R2': -1.505, 'R3': -1.486, 'R4':-1.467}
DxLIUWSH_err = {'R1': 0.008, 'R2': 0.007, 'R3': 0.006, 'R4': 0.006}
#R1H: Dx = -1.446 +/- 0.008 m (expected 1.4687 m) --> 1.5%
#R2H: Dx = -1.505 +/- 0.007 m (expected 1.469 m) --> 2.45%
#R3H: Dx = -1.486 +/- 0.006 m (expected 1.4687 m) --> 1.18%
#R4H: Dx = -1.467 +/- 0.006 m (expected 1.469 m) --> 0.1% 


# Other constants and units

# Numbers
const.pi

# Physical Constants
const.c # speed of light in Meters/Sec
const.epsilon_0 # vacuum permittivity in Farad/Meters = Coulomb/Volt/Meters
const.mu_0 # vacuum pereability in Newton/Ampere = Joule/Ampere/Meters
const.h # Planck's constant in Joule*Sec
const.hbar # Planck's constant in Joule*Sec divided by 2pi

const.e # electron charge in C = Ampere*Sec = Joule/Volt
const.m_e # electron mass in kg
mass_e_GeV = 0.000510999 # electron mass in GeV
const.m_p # proton mass in kg
mass_p_GeV = 0.938272 # proton mass in GeV
e_radius = 2.82e-15 # classical electron radius in m
mass_mu_GeV = 0.105658 # muon mass in GeV

alpha_fine = 1.0/137.0 # fine structure constant (approximation)

# Units
eV_to_joule = const.e
joule_to_eV = 1.0/const.e