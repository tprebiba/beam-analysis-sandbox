import numpy as np
import scipy as sp
import beamtools.fitting as btf
from scipy.special import gamma as gam
from scipy import stats


def filtered(a,window=15):
    try:
        b=(sp.signal.savgol_filter(a,window,1))
    except Exception as ex:
        print(ex)
        b=np.nan
    return b


def findBaseline(x,y,left_x_range,right_x_range):
    x = np.array(x)
    y = np.array(y)
    
    xleft_mask = np.where((x>left_x_range[0]) & (x<left_x_range[1]))
    xright_mask = np.where((x>right_x_range[0]) & (x<right_x_range[1]))
    
    xbaseline = list(x[xleft_mask])+list(x[xright_mask])
    ybaseline = list(y[xleft_mask])+list(y[xright_mask])
    
    p,pe,xfit,yfit = btf.linfit_with_errors(xbaseline,ybaseline,None,len(x))
    
    return {'offset_wotilt': np.mean(ybaseline), 'offset_wotilt_err': np.std(ybaseline),
            'offset_wtilt': p[1], 'offset_wtilt_err': pe[1], 'tilt': p[0], 'tilt_err': pe[0],
            'xbaseline': xbaseline, 'ybaseline': ybaseline, 'xfit': xfit, 'yfit': yfit}


def rel_factors(rest_mass, kinetic_energy_GeV):
    gamma_rel = (kinetic_energy_GeV + rest_mass)/rest_mass
    beta_rel  = np.sqrt(1-1/(gamma_rel**2.0))
    return beta_rel, gamma_rel


def momentum_2_KE(rest_mass, momentum_GeV):
    return (np.sqrt(momentum_GeV**2.0+rest_mass**2.0)-rest_mass)


def extrap1d(interpolator):
    xs = interpolator.x
    ys = interpolator.y
    def pointwise(x):
        if x < xs[0]:
            return ys[0]+(x-xs[0])*(ys[1]-ys[0])/(xs[1]-xs[0])
        elif x > xs[-1]:
            return ys[-1]+(x-xs[-1])*(ys[-1]-ys[-2])/(xs[-1]-xs[-2])
        else:
            return interpolator(x)
    def ufunclike(xs):
        return np.array(map(pointwise, np.array(xs)))
    return ufunclike


###################################
# Some helper functions for the 
# optics.toy_lattice.generate_full_q_gaussian function
# Written by Elleanor
###################################
def ABEL_f_F(q, beta):
    F = np.linspace(0,3000,100000) # important numerically the limits and number of slices. Large q, larger upper limit of F. 
    b = beta
    f_1 = -b*((1 + 1/(b*F*(-1+q)))**(-(3/2) + 1/(1-q))) 
    f_2 = (-3 + q)*((1/(b*(-1 + q)))**((1/(1 - q)))) 
    f_3 = (b*(-1+q))**((1+q)/(2-2*q))*(b*F*(-1 + q))**(1/(1 - q))
    f_4 = (1 + q)*gam((q/(-1 + q)))
    f_5 = 4*F**(3/2)*np.pi**2*gam(1/(-1 + q))
    f_F = f_1*f_2*f_3*f_4/f_5
    
    return f_F, F


def ABEL_g_F(f_F, F):
    f_F[0] = 0
    f_F[-1] = 0
    g_F = np.pi**2*f_F*F
    return g_F


def ABEL_cdf_g(g_F, F):
    cdf_g = []
    for i in range(len(F)):
        integral = sp.integrate.trapz(y=g_F[0:i], x=F[0:i]) 
        cdf_g.append(integral)
    return cdf_g


def ABEL_F_G(Np, cdf_g, F):
    G_sample = np.random.uniform(0,0.999,Np)  # this can be tuned 
    y2 = sp.interpolate.interp1d(x=cdf_g, y=F, kind='nearest', )
    F_G = y2(G_sample)
    return F_G


def random_A(F_G):
    A_y = []
    A_x = []
    for i in range(len(F_G)):
        limit_F = np.sqrt(F_G[i])
        A_X_SQ = np.random.uniform(0,F_G[i])
        A_x.append(np.sqrt(A_X_SQ))
        A_y.append(np.sqrt(F_G[i]-A_x[i]**2))
    return A_x, A_y 


def random_beta(F_G):
    for i in range(len(F_G)):
        beta_x = np.random.uniform(0,2*np.pi, 1)
        beta_y = np.random.uniform(0,2*np.pi, 1)
    return beta_x, beta_y
