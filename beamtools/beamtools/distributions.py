#--------------------------------------------------------#
# A set of functions used mostly for fitting data
#--------------------------------------------------------#
import numpy as np
import scipy

# Simple Gaussian function (3 parameters)
def Gaussian(x, A, mu, sig):
    """Gaussian(x,A,mu,sig) """
    return A/np.sqrt(2*np.pi)/sig*np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

# Gaussian function with 5-parameters (non-zero baseline)
def Gaussian_5_parameters(x, A, mu, sig, c, m):
    """gaussian_5_parameter(x, c, m, A, mu, sig)"""
    return c+m*x+A/np.sqrt(2*np.pi)/sig*np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

# Double Gaussian with the same center (5-parameters)
def doubleGaussian(x,mu,A1,sig1,A2,sig2):
    gauss1 = A1/np.sqrt(2*np.pi)/sig1*np.exp(-np.power(x - mu, 2.)/(2*np.power(sig1, 2.)))
    gauss2 = A2/np.sqrt(2*np.pi)/sig2*np.exp(-np.power(x - mu, 2.)/(2*np.power(sig2, 2.)))
    return (gauss1+gauss2)

# Double Gaussian with the same center and non-zero baseline
def doubleGaussian_7_parameters(x,c,m,mu,A1,sig1,A2,sig2):
    gauss1 = A1/np.sqrt(2*np.pi)/sig1*np.exp(-np.power(x - mu, 2.)/(2*np.power(sig1, 2.)))
    gauss2 = A2/np.sqrt(2*np.pi)/sig2*np.exp(-np.power(x - mu, 2.)/(2*np.power(sig2, 2.)))
    return (c+m*x)+(gauss1+gauss2)

# q-Gaussian (4 and 6 parameters)
def C_q(q):
    Gamma=scipy.special.gamma
    sqrt=np.sqrt
    pi=np.pi
    if q<1:
        return (2*sqrt(pi))/((3-q)*sqrt(1-q))*(Gamma((1)/(1-q)))/(Gamma((3-q)/(2*(1-q))))
    elif q==1:
        return sqrt(pi)
    elif q<3:
        return (sqrt(pi))/(sqrt(q-1))*(Gamma((3-q)/(2*(q-1))))/(Gamma((1)/(q-1)))
    else:
        raise Exception('Please q<3!!!')

def e_q(x,q):
    eq = np.zeros(len(x))
    for i,xx in enumerate(x):
        if ((q!=1) and (1+(1-q)*xx)>0):
            eq[i] = (1+(1-q)*xx)**(1/(1-q))
        elif q==1:
            eq[i] = np.exp(xx)
        else:
            eq[i] = 0
    return eq

def qgauss(x, mu, q, beta, A):
    return A*np.sqrt(beta)/C_q(q)*e_q(-beta*(x-mu)**2,q)

def qgauss_6p(x, mu, q, beta, A, c0, sl0):
    return A*np.sqrt(beta)/C_q(q)*e_q(-beta*(x-mu)**2,q)+c0+sl0*x

# Exponential
def custom_exp(x, a, b, c):
        return a*np.exp(-b*x)+c

