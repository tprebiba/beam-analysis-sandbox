#--------------------------------------------------------#
# A set of functions for fitting data
#--------------------------------------------------------#
import numpy as np
from scipy.optimize import curve_fit
import beamtools.distributions as btdist
import beamtools.statistics as bst


# Simple Gaussian fit (3-parameters)
def makeGaussianFit(X,Y,Y_threshold=None):
    X=np.array(X)
    Y=np.array(Y)
    if Y_threshold != None:
        i = np.where(Y>Y_threshold)
        X=X[i].astype(float)
        Y=Y[i].astype(float)

    # guessing initial fit parameters to converge faster
    mean = bst.findMean(X,Y) 
    sigma = bst.findSigma(X,Y)[0]

    popt,pcov = curve_fit(btdist.Gaussian, X, Y, p0=[np.max(Y), mean, sigma])
    popte = np.sqrt(np.diag(pcov))
    x_gaussianfit = np.linspace(min(X), max(X), 1000)
    y_gaussianfit = btdist.Gaussian(x_gaussianfit, *popt)

    return popt,pcov,popte,x_gaussianfit,y_gaussianfit


# 5-parameters Gaussian fit
def makeGaussianFit_5_parameters(X,Y):
    X=np.array(X)
    Y=np.array(Y)

    # guessing initial fit parameters to converge faster
    indx_max = np.argmax(Y)
    mu0 = X[indx_max]
    offs0 = min(Y)
    slope = 0
    ampl = max(Y)-offs0
    window = 10
    x_tmp = X[indx_max-window:indx_max+window]; y_tmp = Y[indx_max-window:indx_max+window]
    x1 = x_tmp[np.searchsorted(y_tmp[:window], offs0+ampl/2)]
    x2 = x_tmp[np.searchsorted(-y_tmp[window:], -offs0+ampl/2)]
    FWHM = x2-x1
    sigma0 = np.abs(2*FWHM/2.355)
    ampl *= np.sqrt(2*np.pi)*sigma0

    popt,pcov = curve_fit(btdist.Gaussian_5_parameters, X, Y, p0=[ampl,mu0,sigma0,offs0,slope])
    popte = np.sqrt(np.diag(pcov))
    x_gaussianfit = np.linspace(min(X), max(X), 1000)
    y_gaussianfit = btdist.Gaussian_5_parameters(x_gaussianfit, *popt)

    return popt,pcov,popte,x_gaussianfit,y_gaussianfit

# Q-Gaussian fit with zero baseline (4-parameters)
def makeQGaussianFit(X,Y, p0, Y_threshold=None):
    X=np.array(X)
    Y=np.array(Y)
    if Y_threshold != None:
        i = np.where(Y>Y_threshold)
        X=X[i].astype(float)
        Y=Y[i].astype(float)

    mu = p0[0]
    q = p0[1]
    beta = p0[2]
    A = p0[3]

    x_qgaussianfit = np.linspace(min(X), max(X), 1000)
    popt,pcov = curve_fit(btdist.qgauss, X, Y, p0=[mu,q,beta,A], bounds=((-np.inf,-np.inf,-np.inf,-np.inf),(np.inf,2.999,np.inf,np.inf)))
    y_qgaussianfit = btdist.qgauss(x_qgaussianfit, *popt)
    popte = np.sqrt(np.diag(pcov))

    return popt, pcov, popte, x_qgaussianfit, y_qgaussianfit


# Q-Gaussian fit with non-zero baseline (6-parameters)
def makeQGaussianFit_6p(X,Y, p0, Y_threshold=None):
    X=np.array(X)
    Y=np.array(Y)
    if Y_threshold != None:
        i = np.where(Y>Y_threshold)
        X=X[i].astype(float)
        Y=Y[i].astype(float)

    mu = p0[0]
    q = p0[1]
    beta = p0[2]
    A = p0[3]
    c = p0[4]
    sl = p0[5]

    x_qgaussianfit = np.linspace(min(X), max(X), 1000)
    popt,pcov = curve_fit(btdist.qgauss_6p, X, Y, p0=[mu,q,beta,A,c,sl])
    y_qgaussianfit = btdist.qgauss_6p(x_qgaussianfit, *popt)
    popte = np.sqrt(np.diag(pcov))

    return popt, pcov, popte, x_qgaussianfit, y_qgaussianfit


# Double Gaussian fit; to be improved
def makeDoubleGaussianFit(X,Y,baseline_flag):
    try:
        X=np.array(X); Y=np.array(Y)

        indx_max = np.argmax(Y)
        mu0 = X[indx_max]
        slope = (Y[-1]-Y[0])/(X[-1]-X[0])
        window = 20
        x_tmp = X[indx_max-window:indx_max+window]
        y_tmp = Y[indx_max-window:indx_max+window]
        offs0 = min(y_tmp)
        ampl = max(y_tmp)-offs0
        x1 = x_tmp[np.searchsorted(y_tmp[:window], offs0+ampl/2)]
        x2 = x_tmp[np.searchsorted(-y_tmp[window : ], -offs0+ampl/2)]
        FWHM = x2-x1
        sigma0 = np.abs(2*FWHM/2.355)
        ampl *= np.sqrt(2*np.pi)*sigma0

        if baseline_flag == 'zero':
            popt,pcov = curve_fit(btdist.doubleGaussian, X, Y, p0=[mu0,ampl,sigma0,0,max(X)])
            if abs(popt[2])<abs(popt[4]):
                A1 = popt[1]
                sig1 = abs(popt[2])
                A2 = popt[3]
                sig2 = abs(popt[4])
            else:
                A1 = popt[3]
                sig1 = abs(popt[4])
                A2 = popt[1]
                sig2 = abs(popt[2])
            mu = popt[0]
            c = 0.0
            m = 0.0

        elif baseline_flag == 'non-zero':
            popt,pcov = curve_fit(btdist.doubleGaussian_7_parameters, X, Y, p0=[offs0,slope,mu0,ampl,sigma0,0,max(X)])
            if abs(popt[4])<abs(popt[6]):
                A1 = popt[3]
                sig1 = abs(popt[4])
                A2 = popt[5]
                sig2 = abs(popt[6])
            else:
                A1 = popt[5]
                sig1 = abs(popt[6])
                A2 = popt[3]
                sig2 = abs(popt[4])
            mu = popt[2]
            c = popt[0]
            m = popt[1]

        return {'c': c, 'm': m, 'mu': mu, 'A1': A1, 'sigma1': sig1, 'A2': A2, 'sigma2': sig2, 'pcov': pcov, 'popt': popt}

    except Exception as ex:

        print('Double gaussian fitting failed, here is the exception: %s'%ex)
        return {k: np.nan for k in ['c', 'm', 'mu', 'A1', 'sigma1', 'A2', 'sigma2','pcov', 'popt']}


# Simple linear fit with errors
def linfit_with_errors(X, Y, X_threshold=None, x_fit_nr=100):
    X = np.array(X)
    Y = np.array(Y)
    if X_threshold != None:
        i = np.where(np.array(X)>X_threshold)
        X = X[i]
        Y = Y[i]

    p,e = np.polyfit(X, Y, 1, cov=True)
    pe=np.sqrt(np.diag(e))
    x_fit = np.linspace(np.min(X), np.max(X), x_fit_nr)
    y_fit = [p[0]*xx+p[1] for xx in x_fit]

    return p, pe, x_fit, y_fit


# Simple quadratic fit with errors
def quadfit_with_errors(X, Y, X_threshold=None, x_fit_nr=100):
    X = np.array(X)
    Y = np.array(Y)
    if X_threshold != None:
        i = np.where(np.array(X)>X_threshold)
        X = X[i]
        Y = Y[i]

    p,e = np.polyfit(X, Y, 2, cov=True)
    pe=np.sqrt(np.diag(e))
    x_fit = np.linspace(np.min(X), np.max(X), x_fit_nr)
    y_fit = [p[0]*xx**2+p[1]*xx+p[2] for xx in x_fit]

    return p, pe, x_fit, y_fit
