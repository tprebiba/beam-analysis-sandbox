#############################################
# A set of functions to facilitate the 
# reading of data published by various SPS 
# devices. Data acquired with pyjapcscout
#############################################
import glob
import numpy as np
import pandas as pd
import pyarrow
import beamtools.utility as btu
import beamtools.fitting as btf
from scipy.interpolate import interp1d


def datafiles2dataframe(path, format='parquet', engine='pyarrow'):
    # not the fastest; to be revised
    files = glob.glob(path+f'*.{format}')
    df = pd.DataFrame()
    if format=='json':
        for file in files:
            df_temp = pd.read_json(file, orient='index').transpose()
            df = pd.concat([df,df_temp], axis=0, join='outer', ignore_index=True) 
    else:
        for file in files:
            df_temp = pd.read_parquet(file, engine=engine)
            df = pd.concat([df,df_temp], axis=0, join='outer')
    df = df.sort_index(ascending=True) # to be understood why rows are not ordered...
    return df

def sync_injector_dataframes(dataframes, nr_of_injections, master_df_index = -1):

    df = pd.DataFrame(index=dataframes[master_df_index].index)
    for df1, multiplier in zip(dataframes, nr_of_injections):
        for col in df1.columns:
            for ii in range(multiplier):
                #reshaped_values = df1[col].values.reshape(-1, multiplier).tolist()
                reshaped_values = df1[col].values[ii::multiplier]
                df[f"{col}.{ii}"] = reshaped_values

    return df



def readBCT(df, index, key, value_at_ctime=None):
    samples = df[key].iloc[index]['value']['totalIntensity']#*np.power(10, df[key].iloc[index]['value']['totalIntensity_unitExponent'])
    ctimes = df[key].iloc[index]['value']['measStamp']#*np.power(10, df[key].iloc[index]['value']['measStamp_unitExponent'])
    if value_at_ctime != None:
        sample_at_ctime = samples[np.where(ctimes==value_at_ctime)[0]]
        return ctimes, samples, sample_at_ctime
    else:
        return ctimes, samples