from scipy import interpolate
from scipy.optimize import curve_fit
import numpy as np
import beamtools.fitting as btf
import beamtools.distributions as btdist
import beamtools.statistics as bts

def gaussian_subtraction(measured_beam_size_mm, measured_beam_size_mm_error,
                         dispersion_m, dispersion_m_error,
                         dp_p):
    
    betatronic_beam_size_mm       = np.sqrt( measured_beam_size_mm**2.0 - (dispersion_m*1000.0*dp_p)**2.0 )
    betatronic_beam_size_mm_error = (1.0/betatronic_beam_size_mm)*np.sqrt((measured_beam_size_mm*measured_beam_size_mm_error)**2  +  ((dp_p)**2*1000*dispersion_m*dispersion_m_error*1000)**2 )
    
    return betatronic_beam_size_mm, betatronic_beam_size_mm_error

# Assuming only the Betatronic profile to be Gaussian
# From 2018
def deconvolution(beam_position_mm, beam_profile_arb_unit,
                  dispersion_m,
                  off_momentum_distribution_arb_unit, deltaP_P):
    
    x_raw = beam_position_mm 
    y_raw = beam_profile_arb_unit
    dispersion_mm = dispersion_m*1e3 # m to mm
    
    popt,pcov,popte,x_gaussianfit,y_gaussianfit = btf.makeGaussianFit_5_parameters(x_raw, y_raw) 
    x1 = x_raw; y1 = btdist.Gaussian_5_parameters(x1, popt[0], popt[1], popt[2], popt[3], popt[4])
    x2 = x_raw - popt[3]; y2 = y_raw - popt[0]-popt[1]*x_raw
    position_step2_centering_mm=x2
    profile_step2_dropping_baseline=y2
    f = interpolate.interp1d(x2, y2, bounds_error=0, fill_value=0)
    limit = 5*popt[4]; 
    x3 = np.linspace(-limit, limit, 1000); y3 = f(x3)
    position_step3_interpolation_mm=x3 
    profile_step3_interpolation=y3
    y4 = y3/np.trapz(y3, x3)
    #y4 = y4/np.max(y4)
    profile_step4_normalization=y4
    # make symmetric?
    y5 = (y4+y4[::-1])/2
    profile_step5_symmetric=y5
    
    # Measured dispersive profile
    dispersive_position_step1_raw_mm=deltaP_P*dispersion_mm # useful for the normalization and the interpolation below
    dispersive_profile_step1_normalized=off_momentum_distribution_arb_unit/np.trapz(off_momentum_distribution_arb_unit,dispersive_position_step1_raw_mm)
    #dispersive_profile_step1_normalized = dispersive_profile_step1_normalized/np.max(dispersive_profile_step1_normalized) # extra line
    f = interpolate.interp1d(dispersive_position_step1_raw_mm,dispersive_profile_step1_normalized, bounds_error=0, fill_value=0)
    dispersive_position_step2_mm=position_step3_interpolation_mm # same as position as of the beam profile
    dispersive_step2_interpolation=f(dispersive_position_step2_mm)
    # make symmetric?
    dispersive_step3_symmetric=(dispersive_step2_interpolation+dispersive_step2_interpolation[::-1])/2
    
    def myConvolution(position_step3_interpolation_mm, sigma):
        myConv=np.convolve(dispersive_step3_symmetric, btdist.Gaussian(position_step3_interpolation_mm,1,0,sigma), 'same')
        myConv/=np.trapz(myConv, position_step3_interpolation_mm) # normalize
        return myConv 
    def myError(sigma):
        myConv = myConvolution(position_step3_interpolation_mm, sigma)
        aux=myConv-profile_step5_symmetric
        return np.std(aux), aux, myConv
    
    # Single parameter (the unknown beam size sigma)
    popt,pcov = curve_fit(myConvolution,position_step3_interpolation_mm, profile_step5_symmetric, p0=[1]) 
    sigma=popt[0]
    a = myError(sigma)
    
    return { 'betatronic_beam_size_mm':sigma,
             'beam_position_mm':position_step3_interpolation_mm, 'beam_profile_arb_unit': profile_step5_symmetric, 
             'dispersive_position_mm':dispersive_position_step2_mm, 'dispersive_profile':dispersive_step3_symmetric,
             'profile_raw': profile_step4_normalization,
             'convolutionBackComputed':myConvolution(position_step3_interpolation_mm,sigma),
             'betatronicProfile':btdist.Gaussian(position_step3_interpolation_mm,1,0,sigma),
             'a':a
           }


# Assuming the betatronic profile to be Q-Gaussian
def deconvolution_qGaussian(beam_position_mm, beam_profile_arb_unit,
                            dispersion_m, off_momentum_distribution_arb_unit, deltaP_P,
                            remove_measured_baseline=True, center_measured_profile=True, mask_positive_yvalues=True, 
                            q0 = 1.3, restrict_measured_position_range = [-np.inf, np.inf],
                            center_dispersive_profile=False,
                            interpolation_fill_value='extrapolate', interpolation_kind='linear', force_zero_extrapolation=False,
                            symmetrisize_dispersive_profile=False
                            ):
    
    # Measured distribution
    sort_index = np.argsort(beam_position_mm)
    xm = np.array(beam_position_mm[sort_index])
    ym = np.array(beam_profile_arb_unit[sort_index])
    poptm,_,_,_,_ = btf.makeGaussianFit_5_parameters(xm, ym) # make 5-parameters gaussian fit
    if remove_measured_baseline:
        ym = ym - poptm[3] - poptm[4]*xm
    if center_measured_profile:
        xm = xm - poptm[1]
    if mask_positive_yvalues:
        mask_positive = np.where(ym>0)
        xm = xm[mask_positive].astype(float)
        ym = ym[mask_positive].astype(float)
    p0 = [poptm[1], q0,  1/poptm[2]**2/(5-3*q0), poptm[0]] # initial mu, q, beta, A
    poptqm,_,_,_,_ = btf.makeQGaussianFit(xm,ym,p0) # Make q-Gaussian fit for p0 of convolution
    mask_position = (xm>restrict_measured_position_range[0]) & (xm<restrict_measured_position_range[1])
    xm = xm[mask_position]
    ym = ym[mask_position]
    ym = ym/np.trapz(ym, xm) # normalize area to 1

    # Dispersive distribution
    xd = np.array(deltaP_P)*dispersion_m*1e3 # mm
    yd = np.array(off_momentum_distribution_arb_unit)
    sort_index = np.argsort(xd)
    xd = np.array(xd[sort_index])
    yd = np.array(yd[sort_index])
    poptd,_,_,_,_ = btf.makeGaussianFit(xd, yd)
    if center_dispersive_profile:
        #xd = xd - bts.findMean(xd, yd) # center it by mean
        xd = xd - poptd[1] # center it by Gaussian fit
    if mask_positive_yvalues:
        mask_positive = np.where(yd>0) # keep only positive values
        xd = xd[mask_positive].astype(float)
        yd = yd[mask_positive].astype(float)
    f = interpolate.interp1d(xd, yd, fill_value=interpolation_fill_value, kind=interpolation_kind)
    yd = f(xm)
    if force_zero_extrapolation:
        yd[xm<min(xd)] = 0
        yd[xm>max(xd)] = 0
    xd = np.copy(xm)
    # If dispersive profile is not symmetric, the convolution will be asymmetric
    # and possibly with an offset. To avoid this, we symmetrize the dispersive profile
    # Use with at your own risk...
    if symmetrisize_dispersive_profile:
        zero_index = np.argmin(np.abs(xd))
        xd_neg = xd[xd < xd[zero_index]]
        yd_neg = yd[xd < xd[zero_index]]
        xd_pos = xd[xd > xd[zero_index]]
        yd_pos = yd[xd > xd[zero_index]]
        interpolator = interpolate.interp1d(xd_neg, yd_neg, fill_value="extrapolate")
        # Create mirrored negative values for positive xd
        yd_neg_interp = interpolator(-xd_pos)
        # Take the average to make symmetric
        yd_symmetric = (yd_pos + yd_neg_interp) / 2
        xd_sym = np.concatenate([-xd_pos[::-1], xd[xd == xd[zero_index]], xd_pos])
        yd_sym = np.concatenate([yd_symmetric[::-1], yd[xd == xd[zero_index]], yd_symmetric])
        xd = xd_sym
        yd = yd_sym
    yd = yd/np.trapz(yd, xd) # normalize area to 1

    # Numerical convolution function with a q-Gaussian centered at zero
    def myConvolution(xm, q, beta, A): 
        myConv=np.convolve(yd, btdist.qgauss(xm, 0, q, beta, A), mode='same')
        myConv/=np.trapz(myConv, xm) # normalize area to 1
        return myConv 
    
    # Find optimal betatronic distribution
    poptqb,pcovqb = curve_fit(myConvolution,xm, ym, p0=poptqm[1::],
                              bounds=((-5,0,0), (2.5,np.inf,np.inf)))
    xb = np.copy(xm)
    yb = btdist.qgauss(xb, 0,*poptqb)
    yb = yb/np.trapz(yb, xb) # normalize area to 1

    # Error
    convolutionBackComputed = myConvolution(xm, *poptqb)
    aux = convolutionBackComputed-ym
    
    return {
            'xm': xm, 'ym': ym, 'qm': poptqm[1], 'sigmam': poptm[2],
            'xd': xd, 'yd': yd,
            'xb': xb, 'yb': yb,
            'poptqb': [0,*poptqb], 'pcovqb': [0,*pcovqb], 'qb': poptqb[0],
            'convolutionBackComputed': convolutionBackComputed, 'aux': aux
           }
