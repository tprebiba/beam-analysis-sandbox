################################################################
# A module to simulate the wire scanner measurement and 
# wire scattering induced effects (+ analytical estimations). 
################################################################
import numpy as np
import scipy.constants as const


#############################
# Physical constants
#############################
alpha_fine = const.alpha
NA = const.Avogadro
e_radius = 2.82e-13 # in cm


class WireScanner():

    def __init__(self, wire_d=38e-6, wire_shape = 'square',
                       theta=0.47e-6, angle_distribution = 'gaussian',
                       wire_pos=np.arange(-0.02,0.02,0.0005), Trev = 1e-6, wire_speed = 0,
                       plane='y'):
        """
        Wire scanner class constructor.

        Parameters:
            wire_d (float): width of the wire in m 
            wire_shape (string): flag to control the shape of the wire cross section
                                    'square': for square wire
                                    'circular': for circular wire
            theta (float): scattering angle in rad, estimated analytically or experimentally
            angle_distribution (string): flag to control the distribution of the scattering angles
                                             'gaussian': normal distribution centered at 0 with 1sigma=theta
                                             'constant': all particles receive kicks equal to theta
                                             'uniform': uniform distribution between -theta and theta
            wire_pos (array): turn-by-turn wire position in m
            Trev (float): beam revolution period in s
            wire_speed (float): wire speed in m/s. If is >0, wire_pos is re-defined
            plane (string): 'x' for horizontal and 'y' for vertical wire scanner
        
        Returns:
            Nothing
        """
        self.wire_d = wire_d
        self.wire_shape = wire_shape
        self.theta = theta
        self.angle_distribution = angle_distribution
        self.wire_pos = wire_pos
        self.Trev = Trev
        self.wire_speed = wire_speed
        self.plane = plane

        if self.wire_speed > 0:
            # re-define the wire position based on wire_speed and Trev (uniform wire displacement)
            self.wire_pos = np.arange(wire_pos[0], wire_pos[-1], wire_speed*Trev)

        self.set_wire_pos(0) # Set wire position at its resting position
        self.ws_signal_pos = [] # to save the mimiced WS recorded signal
        self.ws_signal_amp = [] # to save the mimiced WS recorded signal
        

    def set_wire_pos(self,i):
        """
        Sets the current wire position.

        Parameters:
            i (int): index of the self.wire_position
        
        Returns:
            Nothing
        """
        self.current_wire_pos = self.wire_pos[i]

    
    def track(self, particles):
        """
        Applies kicks to the particles intercepting the wire and updates the WS signal

        Parameters:
            particles: particles object from xsuite

        Returns:
            Nothing
		"""
        mask = self.on_wire(getattr(particles, self.plane))
        # wire scanner signal proportional to number of particles that interacted with the wire
        normalization_factor = 1./float(self.wire_d*len(getattr(particles, self.plane)))
        self.ws_signal_pos.append(self.current_wire_pos)
        self.ws_signal_amp.append(len(mask)*normalization_factor)
        t = self.calculate_kicks(len(mask))
        if self.wire_shape == 'circular':
            # scale scattering angles based on the path length the particles travel through the circular wire
            d = getattr(particles, self.plane)[mask]-self.current_wire_pos
            circle_chord = 2*np.sqrt((self.wire_d/2)**2-d**2)
            t *= circle_chord/self.wire_d
        getattr(particles, f'p{self.plane}')[mask] += t


    def on_wire(self, particles_coord):
        """
        Masks the particles that are intercepting the wire.

        Parameters:
            particles_coord (array): the horizontal or vertical coordinates of the particles
        
        Returns:
            An array of the indices of particles_coord that are intercepting the wire. 
        """
        return np.where(((self.current_wire_pos-self.wire_d/2.)<particles_coord) & (particles_coord<(self.current_wire_pos+self.wire_d/2.)))[0]


    def calculate_kicks(self, length):
        """
        Calculates the scattering kicks

        Parameters:
            length: the number of kicks
        
        Returns:
            array
        """
        if self.angle_distribution == 'gaussian':
            return np.random.normal(0,self.theta,length)
        elif self.angle_distribution == 'contstant':
            return np.array([self.theta]*length)
        elif self.angle_distribution == 'uniform':
            return np.random.uniform(-self.theta,self.theta,length)
        else:
            raise ValueError('Set angle_distribution to "gaussian" or "constant" or "uniform".')
    

    def profile(self, particles, bins_flag='uniform'):
        """
        Masks the particles that are within the wire.

        Parameters:
            particles: particles object from xsuite
            bins_flag (string): 'uniform' or 'non-unifor'
        
        Returns:
            TBA
        """
        if bins_flag == 'uniform':
            # Old version; only uniform bins
            n_bins = len(self.wire_pos)
            a = np.diff(self.wire_pos)[0]/2.
            #a = np.mean(np.diff(wire_pos))/2.
            n_range = (self.wire_pos[0]-a, self.wire_pos[-1]+a)
            p, bin_edges = np.histogram(getattr(particles, self.plane), n_bins, n_range, normed=True) # review normed

        elif bins_flag == 'non-uniform':
            wire_pos = list(self.wire_pos)
            wire_pos.append(wire_pos[-1]+np.abs(np.diff(wire_pos)[-1]))
            wire_pos = np.array(wire_pos)
            p, bin_edges = np.histogram(getattr(particles, self.plane), wire_pos, normed=True) # review normed

        return p


    def calculate_emittance(self, particles, beta_gamma=1):
        yy = np.nanmean(getattr(particles, self.plane)*getattr(particles, self.plane)) - np.nanmean(getattr(particles, self.plane)*particles.delta)**2/np.nanmean(particles.delta*particles.delta)
        yyp = np.nanmean(getattr(particles, self.plane)*getattr(particles, f'p{self.plane}')) - np.nanmean(getattr(particles, self.plane)*particles.delta)*np.nanmean(getattr(particles, f'p{self.plane}')*particles.delta)/np.nanmean(particles.delta*particles.delta)
        ypyp = np.nanmean(getattr(particles, f'p{self.plane}')*getattr(particles, f'p{self.plane}')) - np.nanmean(getattr(particles, f'p{self.plane}')*particles.delta)**2/np.nanmean(particles.delta*particles.delta)
        return np.sqrt(yy*ypyp - yyp**2)*beta_gamma


    def rejection_sampling(self, n, xmin, xmax, ymin, ymax, pdf):
        n = int(n)
        distr = np.zeros(n)
        for ii in range(n):
            x = np.random.random()*(xmax-xmin) + xmin
            fx = np.random.random()*(ymax-ymin)+ymin
            try:
                # q-gaussian is not defined as a normal function; to be seen again
                while fx > pdf([x])[0]:
                    x = np.random.random()*(xmax-xmin)+xmin
                    fx = np.random.random()*(ymax-ymin)+ymin
                distr[ii] = x
            except:
                while fx > pdf(x):
                    x = np.random.random()*(xmax-xmin)+xmin
                    fx = np.random.random()*(ymax-ymin)+ymin
                distr[ii] = x
        return distr


################################################################
# Analytical estimations
################################################################
def radiation_length(Z, A, rho, target_atom_radius, target_nucleus_radius):
    """
    Compute the radiation length of a material based on (5.8) of F. Roncarolo's PhD thesis.
    ----------
    Input:
        Z : int
            Atomic number of the material.
        A : float
            Atomic mass of the material.
        rho : float 
            Density of the material in units g/cm3.
        target_atom_radius : float
            Atomic radius of the material in picometers.
        target_nucleus_radius : float
            Nucleus radius of the material in picometers.
    Returns:
        Lrad : float
            Radiation length of the material in cm.
    """
    one_over_Lrad = 2*alpha_fine*(NA/A)*rho*Z**2*e_radius**2*np.log(target_atom_radius/target_nucleus_radius) # units 1/cm
    Lrad = 1/one_over_Lrad # units cm
    return Lrad


def scattering_angle_variance(p, beta_rel, d, Lrad):
    """
    Compute the variance of the scattering angle based on (5.12) of F. Roncarolo's PhD thesis.
    ----------
    Input:
        p : float
            Momentum of the incident particle in units MeV/c.
        beta_rel : float
            Relativistic beta of incident particle.
        d : float
            Thickness of the material in m.
        Lrad : float
            Radiation length of the material in cm.
    Returns:
        sigma_theta_sq : float
            Variance of the scattering angle in rad^2.
    """
    Lrad = Lrad*1e-2 # convert to m
    rms_theta_squared = (13.6/p/beta_rel)**2.0*(d/Lrad)*(1+0.038*np.log(d/Lrad)) # units rad^2
    return rms_theta_squared


def wire_scanner_emittance_growth(d, wire_speed, frev, betaTwiss, rms_theta_squared, betagamma):
    """
    Compute the normalized emittance growth from a wire scan 
    based on (5.23) of F. Roncarolo's PhD thesis.
    ----------
    Input:
        d : float
            Thickness of the wire in m.
        wire_speed : float
            Speed of the wire in m/s.
        frev : float
            Revolution frequency of the machine in Hz.
        betaTwiss : float
            Twiss beta at location of the wire in m.
        rms_theta_squared : float
            Variance of the scattering angle in rad^2.
        betagamma : float
            Relativistic beta*gamma of the incident particle.
    Returns:
        epsilon_growth : float
            Normalized emittance growth in mm mrad.
    """
    dequiv = np.pi*d/4 # equivalent thickness of the wire in m
    den = dequiv*frev/wire_speed*0.5*betaTwiss*rms_theta_squared*betagamma # emittance increase normalized
    return den*1e6 # in mm mrad
