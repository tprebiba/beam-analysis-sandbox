import numpy as np
from scipy.fftpack import fft
#import nafflib

# Get spectrume from scipy
def beam_spectrum(data):
	data = np.array(data)
	N = len(data)
	freqs = np.linspace(0,1,N)
	ampls = fft(data)
	tunes = freqs[:N//2]
	norm_ampls = abs(ampls[:N//2])/np.max(abs(ampls))
	return {'freqs':freqs,'ampls':ampls,'tunes':tunes,'norm_ampls':norm_ampls}

