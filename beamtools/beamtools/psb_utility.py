#############################################
# A set of functions to facilitate the 
# reading of data published by various PSB 
# devices. Data acquired with pyjapcscout
#############################################
import glob
import numpy as np
import pandas as pd
import pyarrow
import beamtools.utility as btu
import beamtools.fitting as btf
from scipy.interpolate import interp1d


def datafiles2dataframe(path, format='parquet', engine='pyarrow'):
    # not the fastest; to be revised
    files = glob.glob(path+f'*.{format}')
    df = pd.DataFrame()
    if format=='json':
        for file in files:
            df_temp = pd.read_json(file, orient='index').transpose()
            df = pd.concat([df,df_temp], axis=0, join='outer', ignore_index=True) 
    else:
        for file in files:
            df_temp = pd.read_parquet(file, engine=engine)
            df = pd.concat([df,df_temp], axis=0, join='outer')
    df = df.sort_index(ascending=True) # to be understood why rows are not ordered...
    return df


def readSampler(df, index, key, value_at_ctime=None):
    samples = df[key].iloc[index]['value']['samples'][0]
    firstSampleTime = df[key].iloc[index]['value']['firstSampleTime']
    samplingTrain = round(df[key].iloc[index]['value']['samplingTrain'],5) # rounding to avoid inconsistencies with ctime length
    ctimes = np.arange(firstSampleTime,firstSampleTime+len(samples)*samplingTrain,samplingTrain)
    if value_at_ctime != None:
        sample_at_ctime = samples[np.where(ctimes==value_at_ctime)[0]]
        return ctimes, samples, sample_at_ctime
    else:
        return ctimes, samples
    

def readTuples(df, index, key, value_at_ctime=None):
    # New naming conventions for samplers
    ctimes = df[key].iloc[index]['value']['tuples'][0]['X']
    samples = df[key].iloc[index]['value']['tuples'][0]['Y']
    if value_at_ctime != None:
        sample_at_ctime = samples[np.where(ctimes==value_at_ctime)[0]]
        return ctimes, samples, sample_at_ctime
    else:
        return ctimes, samples


def readLSA(df, index, key):
    # to be improved
    try:
        X = df[key].iloc[index]['value']['X']
        Y = df[key].iloc[index]['value']['Y']
    except:
        try:
            X = df[key].iloc[index]['value'][0]['X']
            Y = df[key].iloc[index]['value'][0]['Y']
        except:
            try:
                X = df[key].iloc[index]['value']['VALUE'][0]['X']
                Y = df[key].iloc[index]['value']['VALUE'][0]['Y']
            except:
                try:
                    X = df[key].iloc[index]['value']['VALUE']['X']
                    Y = df[key].iloc[index]['value']['VALUE']['Y']
                except:
                    X = df[key].iloc[index]['value']['input'][0]['X']
                    Y = df[key].iloc[index]['value']['input'][0]['Y']
    return X,Y


def readOASIS(df, index, key):
    samples = df[key].iloc[index]['value']['DATA']
    interval_ms = round(df[key].iloc[index]['value']['INTERVAL_NS']*1e-6,5) # rounding to avoid inconsistencies with ctime length
    ctimes = np.arange(0,len(samples)*interval_ms,interval_ms)
    return ctimes, samples


def readLIUWS(df, index, key, PM=2, plane = 'V',
              plane2baseline={'H': [[-30,-20], [20,30]], 'V': [[-20,-15], [15,20]]}, # empirical
              plane_to_wschannels = {'H': [0,-1], 'V': [0,-1]} # empirical
              ):
    pos_raw = np.array(df[key].iloc[index]['value']['projPositionSet1'][0])
    amp_raw = np.array(df[key].iloc[index]['value']['projDataSet1'][0][(PM-1)*len(pos_raw):(PM)*len(pos_raw)])
    pos = np.copy(pos_raw[plane_to_wschannels[plane][0]:plane_to_wschannels[plane][1]]) # to remove spikes
    amp = np.copy(amp_raw[plane_to_wschannels[plane][0]:plane_to_wschannels[plane][1]])
    amp = btu.filtered(amp) # savitzky-golay filter
    bsln = btu.findBaseline(pos,amp,plane2baseline[plane][0],plane2baseline[plane][1])
    amp = amp-bsln['offset_wtilt']-pos*bsln['tilt'] # remove baseline
    popt,pcov,popte,x_gaussianfit,y_gaussianfit = btf.makeGaussianFit_5_parameters(pos,amp)
    pos = pos-popt[1] # center
    #data = data-popt[4]*pos-popt[3] # remove baseline from Gaussian fit
    f1_raw = interp1d(pos_raw,amp_raw,kind='quadratic',fill_value="extrapolate")
    f1 = interp1d(pos,amp,kind='quadratic',fill_value="extrapolate")

    return pos_raw, amp_raw, pos, amp, f1_raw, f1, popt