import numpy as np
import matplotlib.pyplot as plt
import beamtools.psb_utility as psbu
from scipy.interpolate import interp1d


df = psbu.datafiles2dataframe('example_data/PSB_POPSB', format='parquet', engine='pyarrow')
ring = 'R1'
plotBetween = [275, 700] # in ms
f, axs = plt.subplots(2,1,figsize=(8,10))
for i in range(len(df)):
    
    for mps,color in zip(['14', '23'], ['red', 'green']):
        ctimes, value = psbu.readOASIS(df, i, f'BR{mps}.MPS/LOG.OASIS.I_MEAS')
        set_ctimes, set_value = psbu.readLSA(df, i, f'logical.BR{mps}.MPS/I')
        ff = interp1d(set_ctimes, set_value)
        mask = np.where((ctimes>=plotBetween[0]) & (ctimes<=plotBetween[1]))
        axs[0].plot(ctimes[mask], value[mask]-ff(ctimes[mask]), '-', c=color, lw=1)

    for plane,color in zip(['H', 'V'],['blue','orange']):
        Qmeas_ctimes, Qmeas_value = psbu.readSampler(df, i, f'B{ring}.BQ-{plane}-ST/Samples')
        Qset_ctimes, Qset_value = psbu.readLSA(df, i, f'PSBBEAM{ring[1]}/Q{plane}')
        Qset_value = Qset_value - 4 # to keep only decimal part
        ff = interp1d(Qset_ctimes, Qset_value, fill_value='extrapolate')
        mask = np.where((Qmeas_ctimes>=plotBetween[0]) & (Qmeas_ctimes<=plotBetween[1]))
        axs[1].plot(Qmeas_ctimes[mask], Qmeas_value[mask]-ff(Qmeas_value[mask]), c=color)
f.tight_layout()
plt.show()