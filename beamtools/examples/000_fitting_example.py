import matplotlib.pyplot as plt
import numpy as np
import beamtools.fitting as bft

pos = np.load('example_data/SPS_BWS-51638-H_pos.npy')
amp = np.load('example_data/SPS_BWS-51638-H_amp.npy')


popt,pcov,popte,x_gaussianfit,y_gaussianfit = bft.makeGaussianFit_5_parameters(pos,amp)
q0 = 1.3 # just a guess of overpopulated tails
p0 = [popt[1], q0,  1/popt[2]**2/(5-3*q0), popt[0], popt[3], popt[4]] # mu, q, beta, A, offset, slope
poptq, pcovq, poptqe, x_qgaussianfit, y_qgaussianfit = bft.makeQGaussianFit_6p(pos,amp,p0)


plt.plot(pos,amp,'.',c='blue')
plt.plot(x_gaussianfit, y_gaussianfit,'-',c='orange')
plt.plot(x_qgaussianfit, y_qgaussianfit,'-',c='green')
plt.show()