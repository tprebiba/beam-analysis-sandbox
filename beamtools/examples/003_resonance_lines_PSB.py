import beamtools.resonance_lines as brl
import matplotlib.pyplot as plt

tune_diagram=brl.resonance_lines([3.9,4.3],[3.9,4.45], [1,2,3,4], 16)
fig=tune_diagram.plot_resonance()
plt.title('Tune Diagram', fontsize='20')
plt.plot([4.2],[4.4],'ko',zorder=1e5)
plt.xlabel('$\mathrm{Q_x}$', fontsize='20')
plt.ylabel('$\mathrm{Q_y}$', fontsize='20')
plt.tick_params(axis='both', labelsize='18')
plt.tight_layout()
plt.show()
