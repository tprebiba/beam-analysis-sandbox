#%%
# Testing q-Gaussian fitting stability around q=1
import matplotlib.pyplot as plt
import numpy as np
import beamtools.distributions as btd
import beamtools.fitting as btf
import scipy 
from scipy.optimize import curve_fit

# Original q-Gaussian fitting functions
def C_q(q):
    Gamma=scipy.special.gamma
    sqrt=np.sqrt
    pi=np.pi
    if q<1:
        return (2*sqrt(pi))/((3-q)*sqrt(1-q))*(Gamma((1)/(1-q)))/(Gamma((3-q)/(2*(1-q))))
    elif q==1:
        return sqrt(pi)
    elif q<3:
        return (sqrt(pi))/(sqrt(q-1))*(Gamma((3-q)/(2*(q-1))))/(Gamma((1)/(q-1)))
    else:
        raise Exception('Please q<3!!!')

def e_q(x,q):
    eq = np.zeros(len(x))
    for i,xx in enumerate(x):
        if ((q!=1) and (1+(1-q)*xx)>0):
            eq[i] = (1+(1-q)*xx)**(1/(1-q))
        elif q==1:
            eq[i] = np.exp(xx)
        else:
            eq[i] = 0
    return eq

def qgauss(x, mu, q, beta, A):
    return A*np.sqrt(beta)/C_q(q)*e_q(-beta*(x-mu)**2,q)

def makeQGaussianFit(X,Y, p0, Y_threshold=None):
    X=np.array(X)
    Y=np.array(Y)
    if Y_threshold != None:
        i = np.where(Y>Y_threshold)
        X=X[i].astype(float)
        Y=Y[i].astype(float)
    mu = p0[0]
    q = p0[1]
    beta = p0[2]
    A = p0[3]
    x_qgaussianfit = np.linspace(min(X), max(X), 1000)
    popt,pcov = curve_fit(qgauss, X, Y, p0=[mu,q,beta,A])
    y_qgaussianfit = qgauss(x_qgaussianfit, *popt)
    popte = np.sqrt(np.diag(pcov))
    return popt, pcov, popte, x_qgaussianfit, y_qgaussianfit

# Numerically stable q-Gaussian fitting functions
def C_q_new(q):
    Gamma=scipy.special.gamma
    sqrt=np.sqrt
    pi=np.pi
    if q<0.999:
        return (2*sqrt(pi))/((3-q)*sqrt(1-q))*(Gamma((1)/(1-q)))/(Gamma((3-q)/(2*(1-q))))
    elif ((q>0.999) and (q<1.001)):
        return sqrt(pi)
    elif q<3:
        return (sqrt(pi))/(sqrt(q-1))*(Gamma((3-q)/(2*(q-1))))/(Gamma((1)/(q-1)))
    else:
        raise Exception('Please q<3!!!')

def e_q_new(x,q):
    eq = np.zeros(len(x))
    for i,xx in enumerate(x):
        if ((q!=1) and (1+(1-q)*xx)>0):
            eq[i] = (1+(1-q)*xx)**(1/(1-q))
        elif ((q>0.999) and (q<1.001)):
            eq[i] = np.exp(xx)
        else:
            eq[i] = 0
    return eq

def qgauss_new(x, mu, q, beta, A):
    return A*np.sqrt(beta)/C_q_new(q)*e_q_new(-beta*(x-mu)**2,q)

def makeQGaussianFit_new(X,Y, p0, Y_threshold=None):
    X=np.array(X)
    Y=np.array(Y)
    if Y_threshold != None:
        i = np.where(Y>Y_threshold)
        X=X[i].astype(float)
        Y=Y[i].astype(float)
    mu = p0[0]
    q = p0[1]
    beta = p0[2]
    A = p0[3]
    x_qgaussianfit = np.linspace(min(X), max(X), 1000)
    popt,pcov = curve_fit(qgauss_new, X, Y, p0=[mu,q,beta,A])
    y_qgaussianfit = qgauss_new(x_qgaussianfit, *popt)
    popte = np.sqrt(np.diag(pcov))
    return popt, pcov, popte, x_qgaussianfit, y_qgaussianfit


#%%
x0 = np.arange(-20,20,0.1)
q0s = np.arange(0.96, 1.04, 0.001) # range of q-values to look at
iterations = 100 # number of iterations to perform the fit for each q0
noise_level = 1e-4 # noise to the y-data
_mean_deltaq = []
_std_deltaq = []
_mean_deltaq_new = []
_std_deltaq_new = []
for q0 in q0s:

    # Generate a typical Gaussian with noise
    y0 = qgauss(x0, mu=0, q=q0, beta=1/(5-3*q0)/2**2, A=1)+noise_level*np.random.uniform(-1,1,len(x0))

    # Make a Gaussian fit to have a good p0 guess
    try:
        popt, _, _, _, _ = btf.makeGaussianFit(x0, y0)
    except:
        pass
    #p0 = [popt[1], q0, 1/(5-3*q0)/popt[2]**2, popt[0]] # float initial q0
    p0 = [popt[1], 1.3, 1/(5-3*1.3)/popt[2]**2, popt[0]] # fixed initial q0

    # Iterate over many fits to get the mean and std of the fitted q
    fitted_qs = []
    fitted_qs_new = []
    for i in range(iterations):
        y0 = qgauss(x0, mu=0, q=q0, beta=1/(5-3*q0)/2**2, A=1)+noise_level*np.random.uniform(-1,1,len(x0))
        # Try a fit with the original q-Gaussian
        try:
            poptq, _, _, _, _ = makeQGaussianFit(x0,y0, p0)
            fitted_qs.append(poptq[1])
        except:
            fitted_qs.append(np.nan)
        # Try a fit with the numerically stable q-Gaussian
        try:
            poptq, _, _, _, _ = makeQGaussianFit_new(x0,y0, p0)
            fitted_qs_new.append(poptq[1])
        except:
            fitted_qs_new.append(np.nan)
    _mean_deltaq.append(np.mean(np.array(fitted_qs)-q0)*1e2)
    _std_deltaq.append(np.std(np.array(fitted_qs)-q0)*1e2)
    _mean_deltaq_new.append(np.mean(np.array(fitted_qs_new)-q0)*1e2)
    _std_deltaq_new.append(np.std(np.array(fitted_qs_new)-q0)*1e2)


# %%
f, axs = plt.subplots(2,1, figsize=(7,7), sharex=True)
fontsize=15
f.suptitle('Noise amplitute %1.1e, iterations %i'%(noise_level, iterations), fontsize=fontsize)

ax = axs[0]
ax.set_ylabel('$< q_{fit} - q_0 >$ $[10^{-2}]$', fontsize=fontsize)
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.plot(q0s, _mean_deltaq, 'o-', color='blue', label='Original q-Gaussian')
#ax.plot(q0s, _mean_deltaq_new, 'o-', color='red', label='Numerically stable q-Gaussian')
ax.axvline(1.0, color='black', linestyle='-')

ax = axs[1]
ax.set_xlabel('$q_0$', fontsize=fontsize)
ax.set_ylabel('$\sigma_{q_{fit} - q_0}$ $[10^{-2}]$', fontsize=fontsize)
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.plot(q0s, _std_deltaq, 'o-', color='blue', label='Original q-Gaussian')
ax.plot(q0s, _std_deltaq_new, 'o-', color='red', label='Numerically stable q-Gaussian')
ax.axvline(1.0, color='black', linestyle='-')
#ax.set_xlim(0.95, 1.05)

f.tight_layout()
# %%
