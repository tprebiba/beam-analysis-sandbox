import beamtools.wire_scanner as bws
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.constants as const


##################################################
# Load PSB cycle
##################################################
psbbeam_betagamma = pd.read_csv('example_data/PSB_2GeVCycle_betagamma.csv')
ctimes_betagamma = psbbeam_betagamma['PSBBEAM/BETAGAMMA#value'].values[1::].astype(float)
betagamma = psbbeam_betagamma['PSBBEAM/BETAGAMMA#value.1'].values[1::].astype(float)
gamma = np.sqrt(betagamma**2+1)
beta = np.sqrt(1-1/gamma**2)
kinetic_energy = (gamma-1)*0.9382720813 # GeV
total_energy = kinetic_energy + 0.9382720813 # GeV
momentum = np.sqrt(total_energy**2-0.9382720813**2) # GeV/c

print('Momentum (GeV/c) at PSB injection energy of 160 MeV: ', momentum[list(ctimes_betagamma).index(275)])
print('Relativistic beta at PSB injection energy of 160 MeV: ', beta[list(ctimes_betagamma).index(275)])


##################################################
# Some parameters for the wire and the beam
##################################################
rho = 1.8 # amorphous carbon: 1.8–2.1 g/cm3, graphite: 2.267 g/cm3, diamond: 3.515 g/cm3
Z = 6
A = 12.0096 # between 12.0096 - 12.0116
target_nucleus_radius = 2.4702*1e-3 # in pm; 2.4702 or between 2.4614 and 2.5025 fm or (2.7 fm ?)
target_atom_radius = 70 # empirical: 70 pm, calculated: 67 pm
Lrad = bws.radiation_length(Z, A, rho, target_atom_radius, target_nucleus_radius)
print(f'Lrad = {Lrad} cm')

wire_speed = 25 # m/s, on average
d = 38*1e-6 # in m
betaTwiss = 4.3730 # beta function at wire scanner, m


##################################################
# Calculate emittance increase throughout PSB cycle
##################################################
_ctimes = []
_den = []
#for ctime in [275]:
for ctime in [275, 300, 350, 400, 450, 500, 600, 770]:
    _ctimes.append(ctime)

    p = momentum[list(ctimes_betagamma).index(ctime)]*1e3 # particle momentum in MeV/c
    beta_rel = beta[list(ctimes_betagamma).index(ctime)] # relativistic beta
    rms_theta_squared = bws.scattering_angle_variance(p, beta_rel, d, Lrad)
    #print('rms_theta_squared (rad**2):', rms_theta_squared)
    #theta_mrad = np.sqrt(rms_theta_squared)*1e3
    #print('theta_mrad:', theta_mrad)

    frev = beta_rel*const.c/157.08 # in Hz
    betagamma0 = betagamma[list(ctimes_betagamma).index(ctime)] # relativistic betagamma factor
    den = bws.wire_scanner_emittance_growth(d, wire_speed, frev, betaTwiss, rms_theta_squared, betagamma0)
    print(f'Emittance growth at C{ctime}: {den} um')
    _den.append(den)

plt.plot(_ctimes, _den)
plt.xlabel('Time (ms)')
plt.ylabel('Emittance growth (um)')
plt.show()