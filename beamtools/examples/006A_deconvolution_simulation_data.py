import matplotlib.pyplot as plt
import numpy as np
import beamtools.distributions as btd
import beamtools.fitting as btf
import scipy 
import beamtools.deconvolution as btdc

noise_level = 2e-3 # only in y

# Betatronic distribution (q-Gaussian)
x0 = np.arange(-20,20,0.1)
sigma0q = 3
q0 = 1.3
y0 = btd.qgauss(x0, mu=0, q=q0, beta=1/(5-3*q0)/sigma0q**2, A=1) + noise_level*np.random.randn(len(x0))
y0 = y0/np.trapz(y0, x0) # normalize area to 1
popt0, _, _, _, _ = btf.makeGaussianFit(x0, y0)
sigma0 = popt0[2] # gaussian Sigma

# Dispersive distribution
xd = np.copy(x0)
sigmad = sigma0q/2 # dispersive/betatronic ratio of 0.5
qd = 0.6 # parabolic-like
yd = btd.qgauss(xd, mu=0, q=qd, beta=1/(5-3*qd)/sigmad**2, A=1)+ noise_level*np.random.randn(len(xd))
yd = yd/np.trapz(yd, xd) # normalize area to 1

# Measured distribution (numerical convolution)
xm = np.copy(x0)
ym = np.convolve(y0, yd, mode='same')
ym = ym/np.trapz(ym, x0) # normalize area to 1
poptm, _, _, _, _ = btf.makeGaussianFit(xm,ym)
qm0 = 1.3 # just a guess of overpopulated tails
p0 = [poptm[1], qm0,  1/poptm[2]**2/(5-3*q0), poptm[0]] # mu, q, beta, A
poptqm, _, _, _, _ = btf.makeQGaussianFit(xm,ym,p0)
qm = poptqm[1]
sigmam = poptm[2]

f, ax = plt.subplots(1,1,figsize=(8,5))
fontsize=18
ax.set_xlabel('position [mm]',fontsize=fontsize)
ax.set_ylabel('amplitude [arb. unit]',fontsize=fontsize)
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.plot(x0, y0, 'o', ms=7, color='blue',  label='$q_0=%1.2f$, $\sigma_0=%1.2f$'%(q0,sigma0))
ax.plot(xd, yd, 'o', ms=7, color='red',   label='$q_d=%1.2f$'%qd)
ax.plot(xm, ym, 'o', ms=7, color='green', label='$q_m=%1.2f$, $\sigma_m=%1.2f$'%(qm, sigmam))
ax.legend(fontsize=fontsize-5, loc=0, numpoints=1)

# Deconvolution of dispersive distribution from measured distribution 
# assuming q-Gaussian betatronic distribution
deconvolution = btdc.deconvolution_qGaussian(beam_position_mm=xm, beam_profile_arb_unit=ym,
                                            dispersion_m=1.0,
                                            off_momentum_distribution_arb_unit=yd, deltaP_P=xd*1e-3, # in meters
                                            remove_measured_baseline=False, center_measured_profile=False,
                                            mask_positive_yvalues=False,
                                            q0 = 1.3)
xb = deconvolution['xb']
yb = deconvolution['yb']
poptqb = deconvolution['poptqb']
qb = deconvolution['qb'] # same as poptqb[1]
poptb, _, _, _, _ = btf.makeGaussianFit(xb, yb)
sigmab = poptb[2] # gaussian Sigma

f, axs = plt.subplots(2,1,figsize=(8,10))
fontsize=18
ax = axs[0]
ax.set_xlabel('position [mm]',fontsize=fontsize)
ax.set_ylabel('Betatronic [a.u.]',fontsize=fontsize)
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.plot(x0, y0, 'o', ms=7, color='blue',   label='$q_0=%1.2f$, $\sigma_0=%1.2f$'%(q0,sigma0))
ax.plot(xb, yb, '-', lw=3, color='orange', label='$q_b=%1.2f$, $\sigma_b=%1.2f$'%(qb,sigmab))
ax2 = ax.twinx()
ax2.set_ylabel('aux [a.u.]',fontsize=fontsize)
ax2.plot(x0, y0-yb, '-', lw=1, color='grey')
ax2.set_ylim(min(y0-yb), max(y0-yb)*5)
ax.legend(fontsize=fontsize-5, loc=0, numpoints=1)
ax = axs[1]
ax.set_xlabel('position [mm]',fontsize=fontsize)
ax.set_ylabel('Error [a.u.]',fontsize=fontsize)
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.plot(xm, ym, 'o', ms=7, color='green', label='$q_m=%1.2f$, $\sigma_m=%1.2f$'%(qm, sigmam))
ax.plot(xm, deconvolution['convolutionBackComputed'], '-', lw=3, color='pink', label='Convolution back computed')
ax2 = ax.twinx()
ax2.set_ylabel('aux [a.u.]',fontsize=fontsize)
ax2.plot(xm, deconvolution['aux'], '-', lw=1, color='grey')
ax2.set_ylim(min(deconvolution['aux']), max(deconvolution['aux'])*5)
ax.legend(fontsize=fontsize-5, loc=0, numpoints=1)


# %%
