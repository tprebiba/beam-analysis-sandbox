import optics.toy_lattice as otl
import beamtools.wire_scanner as bws
import beamtools.fitting as btf
import json
import numpy as np
import matplotlib.pyplot as plt
import xpart as xp
import xtrack as xt
import xobjects as xo
import time


##################################################
# Get custom Particles object and simple one-turn
# map. Beam parameters at PSB injection (C290).
# Machine optics at the position of the vertical 
# LIU WS (BWSV11L1). Betas/alfas from MADX, 
# dispersion from the WS application (all values 
# at 4.17/4.23)
##################################################
optics_ring = {'alfa_x':-0.14684,'beta_x':5.8096,'mu_x':4.17, 'd_x':1.4687, 'dp_x':0,
               'alfa_y':-0.19932,'beta_y':4.3730,'mu_y':4.23, 'd_y':0.0000, 'dp_y':0}
map = otl.one_turn_map(**optics_ring)
plane='V'
beta_gamma = 0.519753*1.170526
emit = 0.4535e-6 # normalized
n_part = int(1e5)
beam_params = {'eps_x': emit/beta_gamma, 'eps_y': emit/beta_gamma,
               'dpp_rms': 1.85E-3, 'n_part': n_part, 'xcenter': 0, 'ycenter': 0} # recheck dpp
p = otl.generate_beam(optics_ring, beam_params) # matched Gaussian
#p,_ = otl.generate_beam_qGaussian(optics_ring, beam_params, q=1.5) # matched qGaussian in y
##################################################
# Get Particles from Xsuite
##################################################
#context = xo.ContextCpu()
#line = xt.Line.from_json('example_data/psb_line_4p17_4p23_BWSV11L1.json')
#line.build_tracker(_context=context)
#p = xp.generate_matched_gaussian_bunch(num_particles=n_part, total_intensity_particles=10e10, 
#                                       nemitt_x=0.45e-6, nemitt_y=0.45e-6, sigma_z=(271/4)*0.525*0.3,
#                                       line=line)


##################################################
# Create a wire scanner
##################################################
wire_d = 38e-6 # in m, typical value for the PSB
theta=0.47e-3 # in rad, typical value for the PSB
with open('example_data/PSB_MD5465_R3V_C290_avg_profile.json','r') as f:
    data = json.load(f)
# minus sign: I change the IN scan (right to left) to an OUT (left to right) scan to aggree with the definition of the ws.profile() function
# it is equivalent nevertheless
wire_pos = -np.array(data['mean_pos'])*1e-3 # real wire displacement in the PSB (not uniform speed)
#wire_pos = np.arange(-0.02, 0.02, 0.00003) # constant speed at 30 m/s (uniform wire displacement)
# Reducing the wire_pos to fasten the simulation
mask = np.where((wire_pos<0.01) & (wire_pos>-0.01))
wire_pos = wire_pos[mask]
ws = bws.WireScanner(
                     wire_d=wire_d, wire_shape='square', 
                     theta=theta, angle_distribution='gaussian',
                     wire_pos=wire_pos,
                     #wire_speed = 30 # in m/s, optional, if >0 will re-define wire_pos
                     plane='y'
                     )


##################################################
# Simulate wire scan
##################################################
v_profile_init = ws.profile(p, bins_flag='non-uniform')
start = time.process_time()
emit_x = np.zeros_like(wire_pos)
num_turns = len(wire_pos)
for ii in range(num_turns):
    print(f'turn {ii}/{num_turns}')

    # track one turn using a simple one-turn map
    # If p is generated using the otl.generate_beam(), it is matched to the given optics
    # If p is generated as a matched distribution at psb_line_4p17_4p23_BWSV11L1.json it 
    # might not be perfectly matched in the one-turn map
    p = map.track(p)

    # displace wire and make scattering
    ws.set_wire_pos(ii)
    ws.track(p)
    emit_x[ii] = ws.calculate_emittance(p, beta_gamma=beta_gamma)

print(time.process_time()-start)
v_profile_final = ws.profile(p, bins_flag='non-uniform')


##################################################
# Plots
##################################################
f,ax = plt.subplots(1,1,figsize=(6,5))
fontsize=15
lw=2.0
ax.set_ylabel('Amplitude [a.u.]', fontsize=fontsize)
ax.set_xlabel('y [mm]', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
ax.tick_params(axis='y', labelleft=False, left=False)
ax.plot(wire_pos*1e3, v_profile_init, '-', lw=lw, c='blue', label='Initial')
popt,pcov,popte,x_gaussianfit,y_gaussianfit = btf.makeGaussianFit(wire_pos*1e3, v_profile_init)
ax.plot(x_gaussianfit, y_gaussianfit, '-', lw=1.0, c='blue')
ax.plot(wire_pos*1e3, v_profile_final, '-', lw=lw, c='red', label='Final')
ax.plot(np.array(ws.ws_signal_pos)*1e3, ws.ws_signal_amp, '-', lw=lw, c='green', label='WS signal')
ax.legend(loc=0, fontsize=fontsize-3)
f.tight_layout()
plt.show()
