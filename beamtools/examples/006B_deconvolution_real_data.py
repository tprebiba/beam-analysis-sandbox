#%%
import matplotlib.pyplot as plt
import numpy as np
import beamtools.distributions as btd
import beamtools.fitting as btf
import scipy 
import beamtools.deconvolution as btdc
import beamtools.statistics as bts

# Measured distriution
[xm, ym] = np.load('example_data/PSB_LIUWS_R3H.npy')
poptm, _, _, xgaussian_m, ygaussian_m = btf.makeGaussianFit_5_parameters(xm,ym)
qm0 = 1.3 # just a guess of overpopulated tails
p0 = [poptm[1], qm0,  1/poptm[2]**2/(5-3*qm0), poptm[0], poptm[3], poptm[4]] # mu, q, beta, A, offset, slope
poptqm, _, _, xqgaussian_m, yqgaussian_m = btf.makeQGaussianFit_6p(xm,ym,p0)
qm = poptqm[1]
sigmam = poptm[2]

# Dispersive distribution
dispersion_m = 1.4689
[xd, yd] = np.load('example_data/PSB_LIUWS_R3H_tomoscope.npy')
poptd, _, _, xgaussian_d, ygaussian_d = btf.makeGaussianFit_5_parameters(xd*dispersion_m*1e3,yd)
qd0 = 0.8 # just a guess of overpopulated tails
p0 = [poptd[1], qd0,  1/poptd[2]**2/(5-3*qd0), poptd[0], poptd[3], poptd[4]] # mu, q, beta, A, offset, slope
poptqd, _, _, xqgaussian_d, yqgaussian_d = btf.makeQGaussianFit_6p(xd*dispersion_m*1e3,yd,p0)
qd = poptqd[1]
#sigmad = np.sqrt(1/(5-3*qd)/poptqd[2])
sigmad,_ = bts.findSigma(xd*dispersion_m*1e3, yd)

f, axs = plt.subplots(2,1,figsize=(8,10))
fontsize=18
ax = axs[0]
ax.set_xlabel('position [mm]',fontsize=fontsize)
ax.set_ylabel('Measured distr. [arb. unit]',fontsize=fontsize)
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.plot(xm, ym, '.', ms=2, color='green')
ax.plot(xgaussian_m, ygaussian_m, '-', lw=2, color='blue',   label='Gaussian $\sigma_m=%1.2f$'%sigmam)
ax.plot(xqgaussian_m, yqgaussian_m, '-', lw=2, color='orange', label='q-Gaussian $q_m=%1.2f$'%qm)
ax.legend(fontsize=fontsize-5, loc=0, numpoints=1)
ax.set_xlim(-15, 25)
ax = axs[1]
ax.set_xlabel('position [mm]',fontsize=fontsize)
ax.set_ylabel('Dispersive distr. [arb. unit]',fontsize=fontsize)
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.plot(xd*dispersion_m*1e3, yd, '.', ms=8, color='red')
ax.plot(xqgaussian_d, yqgaussian_d, '-', lw=2, color='orange', label='$q_d=%1.2f$, $\sigma_d^{2M}=%1.2f$, $dp/p_{RMS}^{2M}=%1.2f$'%(qd, sigmad, sigmad/dispersion_m))
ax.legend(fontsize=fontsize-5, loc=1, numpoints=1)
ax.set_xlim(-15, 25)

#%%
# Deconvolution of dispersive distribution from measured distribution 
# assuming q-Gaussian betatronic distribution
deconvolution = btdc.deconvolution_qGaussian(beam_position_mm=xm, beam_profile_arb_unit=ym,
                                            dispersion_m=1.4689, off_momentum_distribution_arb_unit=yd, deltaP_P=xd,
                                            remove_measured_baseline=True, center_measured_profile=True, mask_positive_yvalues=True,
                                            q0 = 0.8, restrict_measured_position_range = [-20, 20],
                                            center_dispersive_profile=True,
                                            force_zero_extrapolation=True,
                                            symmetrisize_dispersive_profile=True
                                            )
poptb, _, _, _, _ = btf.makeGaussianFit(deconvolution['xb'], deconvolution['yb'])
sigmab = poptb[2] # gaussian Sigma


#%%
f, axs = plt.subplots(2,1,figsize=(8,10))
fontsize=18
ax = axs[0]
ax.set_xlabel('position [mm]',fontsize=fontsize)
ax.set_ylabel('Betatronic [a.u.]',fontsize=fontsize)
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.plot(deconvolution['xm'], deconvolution['ym'], '.', ms=1, color='green', label='$q_m=%1.2f$, $\sigma_m=%1.2f$'%(deconvolution['qm'],deconvolution['sigmam']))
ax.plot(deconvolution['xd'], deconvolution['yd'], '.', ms=1, color='red', label='$q_d=%1.2f$, $\sigma_d^{2M}=%1.2f$'%(qd, bts.findSigma(deconvolution['xd'],deconvolution['yd'])[0]))
ax.plot(deconvolution['xb'], deconvolution['yb'], '-', lw=2, color='orange', label='$q_b=%1.2f$, $\sigma_b=%1.2f$'%(deconvolution['qb'],sigmab))
ax.legend(fontsize=fontsize-5, loc=0, numpoints=1)

ax = axs[1]
ax.set_xlabel('position [mm]',fontsize=fontsize)
ax.set_ylabel('Error [a.u.]',fontsize=fontsize)
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.plot(deconvolution['xm'], deconvolution['ym'], '.', ms=1, color='green',   label='$q_m=%1.2f$, $\sigma_m=%1.2f$'%(deconvolution['qm'],deconvolution['sigmam']))
# convolution back computed
xcbc = deconvolution['xm']
ycbc = deconvolution['convolutionBackComputed']
poptcbc, _, _, _, _ = btf.makeGaussianFit(xcbc,ycbc)
qcbc0 = qm0 # just a guess of overpopulated tails
p0 = [poptcbc[1], qcbc0,  1/poptcbc[2]**2/(5-3*qcbc0), poptcbc[0]] # mu, q, beta, A
poptqcbc, _, _, _, _ = btf.makeQGaussianFit(xcbc,ycbc,p0)
qcbc = poptqcbc[1]
sigmacbc = poptcbc[2]
ax.plot(xcbc, ycbc, '-', lw=2, color='blue', label='$q_{cbc}=%1.2f$, $\sigma_{cbc}=%1.2f$'%(qcbc,sigmacbc))
ax.legend(fontsize=fontsize-5, loc=0, numpoints=1)


# %%
