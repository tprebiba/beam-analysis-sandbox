import numpy as np
import matplotlib.pyplot as plt
import beamtools.psb_utility as psbu
import beamtools.fitting as btf
import beamtools.statistics as bts
import pandas as pd


#######################
# Load datafiles
#######################
df_raw = psbu.datafiles2dataframe('example_data/PSB_LIUWS', format='parquet', engine='pyarrow')


#######################
# Re-shape data
#######################
ring = 'R3'
plane2liuws = {'H': '4L1', 'V': '11L1'}
plane = 'V'
df_data_list = []
for i in range(len(df_raw)):
    
    # General
    ws_flag = f'B{ring}.BWS.{plane2liuws[plane]}.{plane}/Acquisition'
    ctime_IN_scan = int(df_raw[ws_flag].iloc[i]['value']['delays'][0])/1000 # in ms, IN scan
    
    # BCT
    bct_ctimes, bct_samples, intensity_at_ctime_IN_scan = psbu.readSampler(df_raw, i, f'B{ring}.BCT-ST/Samples', value_at_ctime=ctime_IN_scan)
    intensity_at_inj = bct_samples[np.where(bct_ctimes==275)[0]]
    losses = (intensity_at_inj-intensity_at_ctime_IN_scan)/intensity_at_inj*100

    # QNOs
    QNO816_x_set, QNO816_y_set = psbu.readLSA(df_raw, i, f'B{ring}.QNO816L3/REF.TABLE.FUNC#VALUE')
    indx816 = list(QNO816_x_set).index(276.0)
    k816 = QNO816_y_set[indx816]
    QNO412_x_set, QNO412_y_set = psbu.readLSA(df_raw, i, f'B{ring}.QNO412L3/REF.TABLE.FUNC#VALUE')
    indx412 = list(QNO412_x_set).index(276.0)
    k412 = QNO412_y_set[indx412]

    # Profiles and fits
    pos_raw, amp_raw, pos, amp, f1_raw, f1, popt = psbu.readLIUWS(df_raw, i, ws_flag, PM=2,
                                                                  plane_to_wschannels = {'H': [1,-1], 'V': [100,-100]})
    second_order_moment, second_order_moment_err = bts.second_moment_2(pos,amp,popt[4],n_sigmas=6)
    popt,pcov,popte,x_gaussianfit,y_gaussianfit = btf.makeGaussianFit(pos,amp)
    RMSE, rRMSEg = bts.rmse(f1(x_gaussianfit), y_gaussianfit)
    q0 = 1.3 # just a guess of overpopulated tails
    p0 = [popt[1], q0,  1/popt[2]**2/(5-3*q0), popt[0]] # mu, q, beta, A
    poptq, pcovq, poptqe, x_qgaussianfit, y_qgaussianfit = btf.makeQGaussianFit(pos,amp,p0=p0)

    if losses<100:
        d = {
            'ctime_IN_scan':ctime_IN_scan, 'intensity_at_ctime_IN_scan':intensity_at_ctime_IN_scan,
            'bct_ctimes':bct_ctimes, 'bct_samples':bct_samples, 'losses':losses,
            'k816':k816, 'k412':k412,
            'pos_raw':pos_raw, 'amp_raw':amp_raw, 'pos':pos, 'amp': amp, 'f1':f1, 'f1_raw':f1_raw,
            'second_order_moment':second_order_moment, 'second_order_moment_err':second_order_moment_err, 
            'popt':popt, 'popte':popte, 'x_gaussianfit':x_gaussianfit, 'y_gaussianfit':y_gaussianfit, 'rRMSEg': rRMSEg, 'measured_sigma_gaussian': popt[2],
            'poptq':poptq, 'poptqe':poptqe, 'x_qgaussianfit':x_qgaussianfit, 'y_qgaussianfit':y_qgaussianfit, 'q':poptq[1]
        }
        df_data_list.append(d)

df = pd.DataFrame(df_data_list)


#######################
# Plots
#######################
f, ax = plt.subplots(1,1,figsize=(8,5))
#ax.plot(df['pos_raw'].iloc[0], df['amp_raw'].iloc[0], '-', lw=1, c='black')
ax.plot(df['pos'].iloc[0], df['amp'].iloc[0], '-', lw=2, c='blue')
ax.plot(df['x_gaussianfit'].iloc[0], df['y_gaussianfit'].iloc[0], '-', lw=1, c='orange')
ax.plot(df['x_qgaussianfit'].iloc[0], df['y_qgaussianfit'].iloc[0], '-', lw=1, c='green')
plt.show()