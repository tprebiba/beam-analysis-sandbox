import matplotlib.pyplot as plt
import beamtools.longitudinal as btl
import glob


source_dir = 'example_data/'
dat_files = glob.glob(source_dir+'*tomoscope_example.dat')

for dat_file in dat_files:
    print(dat_file)
    temp = btl.extractProfile(dat_file)
    d = {'cycleStamp': temp['cycleStamp'],'ctime': temp['ctime'],
         'deltaP_P_RMS': temp['deltaP_P_RMS'],'deltaP_P': temp['deltaP_P'],'myProfile': temp['myProfile']}
    
plt.plot(d['deltaP_P'], d['myProfile'])
plt.show()