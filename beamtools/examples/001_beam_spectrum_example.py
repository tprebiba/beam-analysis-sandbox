import beamtools.spectrum as bsp
import numpy as np
import matplotlib.pyplot as plt
import nafflib
from scipy.signal import argrelextrema

tbt = np.load('example_data/PSB_BBQ_TbT.npy')
spect = bsp.beam_spectrum(tbt)
norder = 3 # number of the most dominant frequencies
qs, A, B = nafflib.get_tunes(tbt, norder)
maximas = argrelextrema(spect['norm_ampls'], np.greater)
inds = spect['norm_ampls'][maximas].argsort()
max_ampls = spect['norm_ampls'][maximas][inds][::-1]
max_tunes = spect['tunes'][maximas][inds][::-1]


f,axs = plt.subplots(1,2,figsize=(12,4))
fontsize=15
ax = axs[0]
ax.set_xlabel('Turn nr.', fontsize=fontsize)
ax.set_ylabel('Pickup (a.u.)', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
ax.plot(tbt, '.', ms=1.0, c='blue')

ax = axs[1]
ax.set_xlabel('Tune', fontsize=fontsize)
ax.set_ylabel('Normalized Amplitude (a.u.)', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
ax.plot(spect['tunes'], spect['norm_ampls'], '-', c='red', lw=1.0, label='Scipy fft ($q=%1.3f$)'%max_tunes[0])
ax.plot(max_tunes[0:1], max_ampls[0:1], '*', ms=10, color='green')
ax.stem(qs, abs(A/np.max(A)), label='NAFFlib ($q=%1.3f$)'%qs[0])
ax.legend(loc=0, fontsize=fontsize-3)

f.tight_layout()
plt.show()




# %%
