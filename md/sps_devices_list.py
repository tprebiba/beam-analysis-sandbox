# Thanks to Ingrid Mases Sole for the initial scripts

signalsToMonitor = []

# BCTs
signalsToMonitor.append('SPS.BCTDC.41435/Acquisition')
signalsToMonitor.append('SPS.BCTDC.51454/Acquisition')
signalsToMonitor.append('SPS.BCTDC.51456/Acquisition')
signalsToMonitor.append('SPS.BCTDC.31458/Acquisition')

# Fast BCTs
signalsToMonitor.append('SPS.BCTW.31931/Acquisition') # Ring
signalsToMonitor.append('TT2.BCTFI.212/CaptureAcquisition') # TT2
signalsToMonitor.append('TT10.BCTFI.102834/CaptureAcquisition') # TT10

# Wire scanners
signalsToMonitor.append('SPS.BWS.51637.H/Acquisition')
signalsToMonitor.append('SPS.BWS.51638.H/Acquisition')
signalsToMonitor.append('SPS.BWS.41677.V/Acquisition')
signalsToMonitor.append('SPS.BWS.41678.V/Acquisition')

# BBQ
signalsToMonitor.append('SPS.BQ.QC/Acquisition') # continuous
signalsToMonitor.append('SPS.BQ.CONT/ContinuousAcquisition') # continuous


# BPMs
signalsToMonitor.append('BPLOFSBA5/GetCapData')

# MR
signalsToMonitor.append('SMR.SCOPE13.CH01/Acquisition')

# ABWLM
signalsToMonitor.append('ABWLMSPS/Acquisition')

# BQM
signalsToMonitor.append('SPSBQMSPSv1/Acquisition')
signalsToMonitor.append('SPSBQMSPSv1/InjectionQuality')

# Other
signalsToMonitor.append('SPSBEAM/GAMMA')
