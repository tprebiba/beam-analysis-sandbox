from pyjapcscout import PyJapcScout
#from pyjapcscout import PyJapcScoutData
import glob
import numpy as np
import pyjapc as pyjapc1

import matplotlib.pyplot as plt
#import numpy as np
import pandas as pd
import os
import pjlsa
#import pyjapc
import pickle5 as pkl5

import json


def get_deltak(lsa, lsa_context, quad_parameter_id):

	cycle = lsa_context
	with lsa.java_api():
		from cern.lsa.client import ServiceLocator, ContextService, ParameterService, SettingService
		from cern.lsa.domain.settings import ContextSettingsRequest, Settings
		from java.util import Collections

		contextService = ServiceLocator.getService(ContextService)
		parameterService = ServiceLocator.getService(ParameterService)
		settingService = ServiceLocator.getService(SettingService)

		cycle = contextService.findStandAloneCycle(cycle)
		parameter = parameterService.findParameterByName("logical.%s/K"%quad_parameter_id)
		cycleSettings = settingService.findContextSettings(
			ContextSettingsRequest.byStandAloneContextAndParameters(cycle, Collections.singleton(parameter)))

		function = Settings.getFunction(cycleSettings, parameter)
		ctimes = np.array(function.toXArray()).tolist()
		deltak = np.array(function.toYArray()).tolist()

	return ctimes, deltak


def set_deltak(lsa, lsa_context, ctime_list, deltak_list, quad_parameter_id, setDescription):
	try:
		cycle = lsa_context
		with lsa.java_api():
			from cern.lsa.client import ServiceLocator, ContextService, ParameterService, TrimService
			from cern.lsa.domain.settings import TrimRequest
			from cern.accsoft.commons.value import ValueFactory

			contextService = ServiceLocator.getService(ContextService)
			parameterService = ServiceLocator.getService(ParameterService)
			trimService = ServiceLocator.getService(TrimService)

			cycle = contextService.findStandAloneCycle(cycle)
			parameter = parameterService.findParameterByName("logical.%s/K"%quad_parameter_id)

			function = ValueFactory.createFunction(
				ctime_list,
				deltak_list
			)

			trimRequest = TrimRequest.builder() \
				.setContext(cycle) \
				.addFunction(parameter, function) \
				.setDescription(setDescription) \
				.build()
			trimService.trimSettings(trimRequest)

	except Exception as ex:
		print('LSA setting failed. Here is the exception:')
		print(ex)
		print('Skipping this setting (data will not be saved)')






measure = True
plot = False
test = False


########################################################################################################
# Start PyJapcScout, PyJapc and LSA
########################################################################################################
if measure:
	server = 'PSB'
	mySelector = '%s.USER.MD5'%server
	myPyJapc = PyJapcScout(incaAcceleratorName=server)
	myPyJapc.setDefaultSelector(mySelector)
	myPyJapc.rbacLogin()
	japc = pyjapc1.PyJapc(noSet=False, selector=mySelector)
	japc.rbacLogin()
	print('PyJapcScout and PyJapc instantiated on %s selector.'%mySelector)
	lsa = pjlsa.LSAClient(server=server.lower())
	lsa_context = 'MD9483_LHC25#36b_2023'
	print('LSA instantiated on "%s" server.' % server)



########################################################################################################
# Create my monitor
########################################################################################################
signalsToMonitor = []
rings = ['R3']
for ring in rings:
	# BCT
	signalsToMonitor.append('B%s.BCT-ST/Samples'%ring)
	# Multipole correctors
	signalsToMonitor.append('B%s.QNO412L3/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.QNO816L3/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.QNO412L3/REF.TABLE.FUNC#VALUE'%ring)
	signalsToMonitor.append('B%s.QNO816L3/REF.TABLE.FUNC#VALUE'%ring)
	signalsToMonitor.append('B%s.XNO4L1/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.XNO9L1/LOG.OASIS.I_MEAS'%ring)	
	signalsToMonitor.append('B%s.ONO311L1/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.ONO816L1/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.XSK2L4/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.XSK4L1/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.XSK6L4/LOG.OASIS.I_MEAS'%ring)
	# Set tunes
	signalsToMonitor.append('PSBBEAM%s/QH'%ring[1])
	signalsToMonitor.append('PSBBEAM%s/QV'%ring[1])
	# BBQ
	#signalsToMonitor.append('B%s.BQ-H-ST/Samples'%ring)
	#signalsToMonitor.append('B%s.BQ-V-ST/Samples'%ring)
	# LIU WS
	signalsToMonitor.append('B%s.BWS.4L1.H/Acquisition'%ring)
	signalsToMonitor.append('B%s.BWS.11L1.V/Acquisition'%ring)
	# Q-strips
	signalsToMonitor.append('B%s.QCF.1/REF.TABLE.FUNCLIST'%ring)
	signalsToMonitor.append('B%s.QCD.1/REF.TABLE.FUNCLIST'%ring)
	signalsToMonitor.append('B%s.QCD3/REF.TABLE.FUNCLIST'%ring)
	signalsToMonitor.append('B%s.QCD14/REF.TABLE.FUNCLIST'%ring)
	signalsToMonitor.append('B%s.QCD.1/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.QCF.1/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.QCD3/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.QCD14/LOG.OASIS.I_MEAS'%ring)
# Sextupole (crhoma)
signalsToMonitor.append('BR.XNOH0/LOG.OASIS.I_MEAS')



########################################################################################################
# Load model corrections
########################################################################################################
path = './'
name = 'reference_simulation_values_440445.json'
with open(path+name, 'r') as fid:
    data = json.load(fid)
print(data.keys())

if plot:
	f,ax = plt.subplots(1,1,figsize=(10,5),facecolor='white')
	fontsize=20
	ax.set_xlabel('Time [ms]', fontsize=fontsize)
	ax.set_ylabel('kl [10$^{-2} $m$^{-1}$]', fontsize=fontsize)
	ax.tick_params(labelsize=fontsize)

	ax.errorbar(np.array(data['ctimes'])+275, np.array(data['kBRQD314CORR'])*1e2, label='QDE314',lw=2)
	ax.errorbar(np.array(data['ctimes'])+275, np.array(data['kBRQDCORR'])*1e2, label='QDE',lw=2)
	ax.errorbar(np.array(data['ctimes'])+275, np.array(data['kBRQFCORR'])*1e2,label='QFO',lw=2)

	ax.grid(True)
	ax.legend(loc=0,fontsize=fontsize-5,ncol=3) 
	f.tight_layout()
	plt.show()

times = list(275+np.array(data['ctimes']))
dt = {}
dt['QDE3'] = data['kBRQD314CORR']
dt['QDE14'] = data['kBRQD314CORR']
dt['QDE'] = data['kBRQDCORR']
dt['QFO'] = data['kBRQFCORR']



########################################################################################################
# Test read and write strength
########################################################################################################
# read strength
#ring = '3'
#quad_parameter_id = 'BR%s.QDE3_CORR'%ring
#quad_parameter_id = 'BR%s.QDE14_CORR'%ring
#quad_parameter_id = 'BR%s.QDE_CORR'%ring
#quad_parameter_id = 'BR%s.QFO_CORR'%ring
#if test:
#	ctimes,deltak = get_deltak(lsa, lsa_context, quad_parameter_id)
#	print(ctimes,deltak)
#	plt.plot(ctimes,deltak)
#	plt.show()

# write strength
#for quad in ['QDE3','QDE14','QDE','QFO']:
#	quad_parameter_id = 'BR%s.%s_CORR'%(ring,quad)
#	set_deltak(lsa, lsa_context, times, dt[quad], quad_parameter_id, 'Model correction')







#'''
########################################################################################################
# MD settings preparation
########################################################################################################
k314_factors = [1.00, 0.85, 0.90, 0.95, 1.05, 1.10, 1.15, 0.8, 0.75, 0.7, 1.20, 1.25, 1.30, 0.6, 0.5, 0.4, 0.1, 1.4, 1.5, 1.6]
kkdf_factors = [1]
#kkdf_factors = [1.0,0.8,1.2,1.4]
ws_list = ['11L1.V']
planes2measure = 1 # 1 for only V WS; 2 for both V and H WS (consecutively)
global_k314_factors = []
global_kkdf_factors = []
for ii in range(len(k314_factors)):
	for jj in range(len(kkdf_factors)):
		global_k314_factors.append(k314_factors[ii])
		global_kkdf_factors.append(kkdf_factors[jj])
#global_k314_factors = k314_factors*len(kkdf_factors)*len(ws_list)
#global_kkdf_factors = kkdf_factors*len(k314_factors)*len(ws_list)
global_WS_list = ws_list*len(k314_factors)*len(kkdf_factors)
iterations = 3
global_k314_factors *= iterations
global_kkdf_factors *= iterations
global_WS_list *= iterations
total_number_of_shots = len(global_WS_list)
print('k314 factors', k314_factors)
print('kkdf factors', kkdf_factors)
print('ws_list', ws_list)
print('iterations (for statistics)',iterations)
print('Total shots', total_number_of_shots)



########################################################################################################
# Callback function
########################################################################################################
ring='R3'
vary = 'V'
def myCallback(data, h):
	global ring, global_k314_factors, global_kkdf_factors, global_WS_list, iterations,total_number_of_shots
	global lsa, lsa_context, times, dt
	print( 'Shot ' + str(len(glob.glob(h.saveDataPath + '2023*'))))
	indx = len(glob.glob(h.saveDataPath + '2023*')) 

	if indx == total_number_of_shots:
		h.stopMonitor()
		print("#####################################################")
		print('Measurement finished and monitor stopped.')

	else:
		if myMonitor.saveData==True:
			#myPyJapc.setSimpleValue('B%s.BWS.%s/PSBSetting#delays'%(ring,global_WS_list[indx]),np.array(global_WS_timing_list[indx])) # set the right WS IN/OUT timings
			japc.setParam('B%s.BWS.%s/Scan#scanSelection'%(ring,global_WS_list[indx]), 1) # launch WS
			print('Measuring with the', global_WS_list[indx])
			for quad in ['QDE3','QDE14']:
				quad_parameter_id = 'B%s.%s_CORR'%(ring,quad)
				set_deltak(lsa, lsa_context, times, 
					   list(np.array(dt[quad])*global_k314_factors[indx]), 
					   quad_parameter_id, 'Model correction %1.2f'%global_k314_factors[indx])
				print('set QDE314')
			for quad in ['QDE','QFO']:
				quad_parameter_id = 'B%s.%s_CORR'%(ring,quad)
				#set_deltak(lsa, lsa_context, times, 
				#	   list(np.array(dt[quad])*global_kkdf_factors[indx]), 
				#	   quad_parameter_id, 'Model correction %1.2f'%global_kkdf_factors[indx])
					   #list(np.array(dt[quad])*global_k314_factors[indx]), 
					   #quad_parameter_id, 'Model correction %1.2f'%global_k314_factors[indx])
				#print('set QDE/QFO')
			
#'''






########################################################################################################
# Start monitor
########################################################################################################
myMonitor = myPyJapc.PyJapcScoutMonitor(mySelector, signalsToMonitor, onValueReceived=myCallback, 
					selectorOverride = mySelector, groupStrategy = 'extended',
					allowManyUpdatesPerCycle=False, strategyTimeout=5200, 
					forceGetOnChangeAndConstantValues=False)
# saving data configuration
myMonitor.saveDataPath = './2023_05_09_data/without_QNOs/'
myMonitor.saveData = False # False or True
myMonitor.saveDataFormat = 'parquet' # or 'parquet' or 'pickle' or 'pickledict' or 'mat'


# start acquisition
myMonitor.startMonitor()
