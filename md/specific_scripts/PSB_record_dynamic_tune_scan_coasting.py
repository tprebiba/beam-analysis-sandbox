from pyjapcscout import PyJapcScout
#from pyjapcscout import PyJapcScoutData
import glob
import numpy as np
import pyjapc as pyjapc1

# start PyJapcScout and PyJapc (needed for properly setting some device fields)
mySelector = 'PSB.USER.MD2'
myPyJapc = PyJapcScout(incaAcceleratorName='PSB')
myPyJapc.setDefaultSelector(mySelector)
newPyJapc = pyjapc1.PyJapc(noSet=False, selector=mySelector)
myPyJapc.rbacLogin() # Get and RBAC tocken (needed for the OASIS data)

# device properties to be recorded 
signalsToMonitor = []
rings = ['R3']
for ring in rings:
	# BCT
	signalsToMonitor.append('B%s.BCT-ST/Samples'%ring)
	# Multipole correctors
	signalsToMonitor.append('B%s.QNO412L3/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.QNO816L3/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.QNO412L3/REF.TABLE.FUNC#VALUE'%ring)
	signalsToMonitor.append('B%s.QNO816L3/REF.TABLE.FUNC#VALUE'%ring)
	# Set tunes
	signalsToMonitor.append('PSBBEAM%s/QH'%ring[1])
	signalsToMonitor.append('PSBBEAM%s/QV'%ring[1])
	# BBQ
	#signalsToMonitor.append('B%s.BQ-H-ST/Samples'%ring)
	#signalsToMonitor.append('B%s.BQ-V-ST/Samples'%ring)
	#signalsToMonitor.append('B%s.BQ/Acquisition'%ring)
	#signalsToMonitor.append('B%s.BQ/Setting'%ring)
	#signalsToMonitor.append('B%s.BQ/Acquisition#rawDataQ'%ring)
	#signalsToMonitor.append('B%s.BQ/Acquisition#rawDataV'%ring)
	# LIU WS
	signalsToMonitor.append('B%s.BWS.4L1.H/Acquisition'%ring)
	signalsToMonitor.append('B%s.BWS.11L1.V/Acquisition'%ring)
	#signalsToMonitor.append('B%s.BWSACQ.V/LoggingCh1'%ring)
# Sextupole (crhoma)
signalsToMonitor.append('BR.XNOH0/LOG.OASIS.I_MEAS')
# QFO/QDE currents
signalsToMonitor.append('BR.QFO/REF.TABLE.FUNC')
signalsToMonitor.append('BR.QDE/REF.TABLE.FUNC')
signalsToMonitor.append('BR.QDE-ST/Samples')
signalsToMonitor.append('BR.QFO-ST/Samples')



# MD settings preparation
Q1 = 4.65
Q2 = 4.45
crossing_over = [100, 50, 25, 10, 5]
t1 = 400
t2 = list(t1+np.array(crossing_over))
ws_list = ['11L1.V', '4L1.H']
QH_ini = myPyJapc.getValues(['PSBBEAM1/QH#input'])
QV_ini = myPyJapc.getValues(['PSBBEAM1/QV#input'])
shots = 5 # IN H, IN V, OUT H, OUT V, intensity
crossing_speeds = []
for t in t2:
	crossing_speeds = crossing_speeds + [t]*shots
iterations = 1
crossing_speeds = crossing_speeds*iterations
total_number_of_shots = len(crossing_speeds)
print('Crossing over', crossing_over)
print('Shots (intensity, INH, INV, OUTH, OUTV)', shots)
print('Iterations', iterations)
print('Total number of shots', total_number_of_shots)

# Callback function
ring='R3'
counter = 0
def myCallback(data, h):
	global ring, t1, crossing_speeds, counter, total_number_of_shots
	print( 'Shot ' + str(len(glob.glob(h.saveDataPath + '2022*'))))
	indx = len(glob.glob(h.saveDataPath + '2022*')) 

	if indx == total_number_of_shots:
		h.stopMonitor()
		print("#####################################################")
		print('Measurement finished and monitor stopped.')

	else:
		if myMonitor.saveData==True:
			
			counter+=1
			print('Counter', counter)
			print('Measuring the intensity.')			
			IN_time = (t1-5)
			OUT_time = (crossing_speeds[indx]+5)
			if counter==1: # Trigger IN H
				myPyJapc.setSimpleValue('BR3.BWS.4L1.H/PSBSetting#delays',np.array([IN_time*1e3, (IN_time+200)*1e3]))
				print('4L1.H WS set IN time to C', IN_time)
				newPyJapc.setParam('BR3.BWS.4L1.H/Scan#scanSelection', 1)
				print('Triggered 4L1.H WS...')
			if counter==2: # Trigger IN V
				myPyJapc.setSimpleValue('BR3.BWS.11L1.V/PSBSetting#delays',np.array([IN_time*1e3, (IN_time+200)*1e3]))
				print('11L1.V WS set IN time to C', IN_time)
				newPyJapc.setParam('BR3.BWS.11L1.V/Scan#scanSelection', 1)
				print('Triggered 11L1.V WS...')
			if counter==3: # Trigger OUT H	
				myPyJapc.setSimpleValue('BR3.BWS.4L1.H/PSBSetting#delays',np.array([OUT_time*1e3, (OUT_time+200)*1e3]))
				print('4L1.H WS set IN time to C', OUT_time)
				newPyJapc.setParam('BR3.BWS.4L1.H/Scan#scanSelection', 1)
				print('Triggered 4L1.H WS...')
			if counter==4: # Trigger OUT V
				myPyJapc.setSimpleValue('BR3.BWS.11L1.V/PSBSetting#delays',np.array([OUT_time*1e3, (OUT_time+200)*1e3]))
				print('11L1.V WS set IN time to C', OUT_time)
				newPyJapc.setParam('BR3.BWS.11L1.V/Scan#scanSelection', 1)
				print('Triggered 11L1.V WS...')
			if counter==5: # Trigger tune change
				QV_ini['PSBBEAM1/QV#input']['value'][0]['X'][2] = crossing_speeds[indx]
				myPyJapc.setSimpleValue('PSBBEAM1/QV#input', QV_ini['PSBBEAM1/QV#input']['value'])
				myPyJapc.setSimpleValue('PSBBEAM2/QV#input', QV_ini['PSBBEAM1/QV#input']['value'])
				myPyJapc.setSimpleValue('PSBBEAM3/QV#input', QV_ini['PSBBEAM1/QV#input']['value'])
				myPyJapc.setSimpleValue('PSBBEAM4/QV#input', QV_ini['PSBBEAM1/QV#input']['value'])
				print('Changing crossing speed to', crossing_speeds[indx])
				counter = 0
				

myMonitor = myPyJapc.PyJapcScoutMonitor(mySelector, signalsToMonitor, onValueReceived=myCallback, 
					selectorOverride = mySelector, groupStrategy = 'extended',
					allowManyUpdatesPerCycle=False, strategyTimeout=5200, 
					forceGetOnChangeAndConstantValues=False)

# saving data configuration
myMonitor.saveDataPath = './coasting/'
myMonitor.saveData = False
#myMonitor.saveData = True
myMonitor.saveDataFormat = 'parquet' # or 'parquet' or 'pickle' or 'pickledict' or 'mat'

# start acquisition
myMonitor.startMonitor()
