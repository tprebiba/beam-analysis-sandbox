from pyjapcscout import PyJapcScout
#from pyjapcscout import PyJapcScoutData
import glob
import numpy as np
import pyjapc as pyjapc1

# start PyJapcScout and PyJapc (needed for properly setting some device fields)
mySelector = 'PSB.USER.MD2'
myPyJapc = PyJapcScout(incaAcceleratorName='PSB')
myPyJapc.setDefaultSelector(mySelector)
newPyJapc = pyjapc1.PyJapc(noSet=False, selector=mySelector)
myPyJapc.rbacLogin() # Get and RBAC tocken (needed for the OASIS data)

# device properties to be recorded 
signalsToMonitor = []
rings = ['R3']
for ring in rings:

	# BCT
	signalsToMonitor.append('B%s.BCT-ST/Samples'%ring)

	# Multipole correctors
	signalsToMonitor.append('B%s.QNO412L3/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.QNO816L3/LOG.OASIS.I_MEAS'%ring)
	signalsToMonitor.append('B%s.QNO412L3/REF.TABLE.FUNC#VALUE'%ring)
	signalsToMonitor.append('B%s.QNO816L3/REF.TABLE.FUNC#VALUE'%ring)

	# k for reconstructing the tunes
	signalsToMonitor.append('logical.B%s.QFO/K'%ring)
	signalsToMonitor.append('logical.B%s.QDE/K'%ring)

	# BBQ TBA?
	signalsToMonitor.append('B%s.BQ-H-ST/Samples'%ring)
	signalsToMonitor.append('B%s.BQ-V-ST/Samples'%ring)

	# LIU WS
	signalsToMonitor.append('B%s.BWS.4L1.H/Acquisition'%ring)
	signalsToMonitor.append('B%s.BWS.11L1.V/Acquisition'%ring)


# MD settings preparation
ws_list = ['11L1.V', '4L1.H']
ws = ws_list[0]
ring = 'R3'

k816_ini = -3.0 # A
k412_ini = 5.94 # A

global_k816_list = []
global_k412_list = []

excitations2measure = 32
delta_ampl = 5.0
for i in range(excitations2measure):
	phi = i/excitations2measure*2.0*np.pi
	global_k816_list.append(k816_ini+delta_ampl*np.cos(phi))
	global_k412_list.append(k412_ini+delta_ampl*np.sin(phi))

iterations = 3
global_k816_list = global_k816_list*iterations
global_k412_list = global_k412_list*iterations 

print('Number of different excitations', excitations2measure)
print('Iterations', iterations)
total_number_of_shots = excitations2measure*iterations


# callback function
ring='R3'
vary = 'V'
k816_read = myPyJapc.getValues(['B%s.QNO816L3/REF.TABLE.FUNC'%(ring)])
k412_read = myPyJapc.getValues(['B%s.QNO412L3/REF.TABLE.FUNC'%(ring)])
def myCallback(data, h):
	global ring, vary, global_k816_list, global_k412_list, total_number_of_shots, ws 
	print( 'Shot ' + str(len(glob.glob(h.saveDataPath + '2021*'))))
	indx = len(glob.glob(h.saveDataPath + '2021*')) 

	if indx == total_number_of_shots:
		h.stopMonitor()
		print("#####################################################")
		print('Measurement finished and monitor stopped.')

	else:
		if myMonitor.saveData==True:
			k816_read['B%s.QNO816L3/REF.TABLE.FUNC'%(ring)]['value']['Y'][2]=global_k816_list[indx]
			k816_read['B%s.QNO816L3/REF.TABLE.FUNC'%(ring)]['value']['Y'][3]=global_k816_list[indx]
			myPyJapc.setSimpleValue('B%s.QNO816L3/REF.TABLE.FUNC#VALUE'%(ring), k816_read['B%s.QNO816L3/REF.TABLE.FUNC'%(ring)]['value']) 

			k412_read['B%s.QNO412L3/REF.TABLE.FUNC'%(ring)]['value']['Y'][2]=global_k412_list[indx]
			k412_read['B%s.QNO412L3/REF.TABLE.FUNC'%(ring)]['value']['Y'][3]=global_k412_list[indx]
			myPyJapc.setSimpleValue('B%s.QNO412L3/REF.TABLE.FUNC#VALUE'%(ring), k412_read['B%s.QNO412L3/REF.TABLE.FUNC'%(ring)]['value']) 

			newPyJapc.setParam('B%s.BWS.%s/Scan#scanSelection'%(ring,ws), 1) # launch WS
			print('Launching WS.')

myMonitor = myPyJapc.PyJapcScoutMonitor(mySelector, signalsToMonitor, onValueReceived=myCallback, 
					                    selectorOverride = mySelector, groupStrategy = 'extended',
		                                allowManyUpdatesPerCycle=False, strategyTimeout=5200, 
					                    forceGetOnChangeAndConstantValues=False)

# saving data configuration
myMonitor.saveDataPath = './data_deltaAmpl5_515ms_4turnsinj_fromAbove/'
myMonitor.saveData = False
#myMonitor.saveData = True
myMonitor.saveDataFormat = 'parquet' # or 'parquet' or 'pickle' or 'pickledict' or 'mat'

# start acquisition
myMonitor.startMonitor()
