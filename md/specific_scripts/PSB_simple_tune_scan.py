from pyjapcscout import PyJapcScout
#from pyjapcscout import PyJapcScoutData
import glob
import numpy as np
import pyjapc as pyjapc1



# start PyJapcScout and PyJapc (needed for properly setting some device fields)
mySelector = 'PSB.USER.MD4'
myPyJapc = PyJapcScout(incaAcceleratorName='PSB')
myPyJapc.setDefaultSelector(mySelector)
newPyJapc = pyjapc1.PyJapc(noSet=False, selector=mySelector)
myPyJapc.rbacLogin() # Get an RBAC token (needed for the OASIS data)
newPyJapc.rbacLogin()



# device properties to be recorded 
signalsToMonitor = []
#rings = ['R1','R2','R3','R4']
rings = ['R3']
for ring in rings:

    # BCT
    signalsToMonitor.append('B%s.BCT-ST/Samples'%ring)

    # Set tunes
    signalsToMonitor.append('PSBBEAM%s/QH'%ring[1])
    signalsToMonitor.append('PSBBEAM%s/QV'%ring[1]) 
    # k for reconstructing the tunes
    signalsToMonitor.append('logical.B%s.QFO/K'%ring)
    signalsToMonitor.append('logical.B%s.QDE/K'%ring)
    
    # Q-strips (set and measured)
    signalsToMonitor.append('B%s.QCF.1/REF.TABLE.FUNCLIST'%ring)
    signalsToMonitor.append('B%s.QCD.1/REF.TABLE.FUNCLIST'%ring)
    signalsToMonitor.append('B%s.QCF.1/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.QCD.1/LOG.OASIS.I_MEAS'%ring)

    # Special Q-strips (set and measured)
    signalsToMonitor.append('B%s.QCD3/REF.TABLE.FUNCLIST'%ring)
    signalsToMonitor.append('B%s.QCD14/REF.TABLE.FUNCLIST'%ring)
    signalsToMonitor.append('B%s.QCD3/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.QCD14/LOG.OASIS.I_MEAS'%ring)

    # Multipole correctors
    #signalsToMonitor.append('B%s.QNO412L3/LOG.OASIS.I_MEAS'%ring)
    #signalsToMonitor.append('B%s.QNO816L3/LOG.OASIS.I_MEAS'%ring)
    #signalsToMonitor.append('B%s.ONO4L1/LOG.OASIS.I_MEAS'%ring)
    #signalsToMonitor.append('B%s.ONO12L1/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.ONO311L1/LOG.OASIS.I_MEAS'%ring) # enabled
    signalsToMonitor.append('B%s.ONO816L1/LOG.OASIS.I_MEAS'%ring) # enabled
    #signalsToMonitor.append('B%s.XNO311L1/LOG.OASIS.I_MEAS'%ring) 
    #signalsToMonitor.append('B%s.XNO816L1/LOG.OASIS.I_MEAS'%ring)
    #signalsToMonitor.append('B%s.XNO6L1/LOG.OASIS.I_MEAS'%ring)
    #signalsToMonitor.append('B%s.XNO12L1/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.XSK2L4/LOG.OASIS.I_MEAS'%ring) # enabled
    signalsToMonitor.append('B%s.XSK6L4/LOG.OASIS.I_MEAS'%ring) # enabled

    # LIU WS
    signalsToMonitor.append('B%s.BWS.4L1.H/Acquisition'%ring)
    signalsToMonitor.append('B%s.BWS.11L1.V/Acquisition'%ring)

    # BPMs
    signalsToMonitor.append('B%s.BPM/AcquisitionOrbit'%ring)

# QFO/QDE currents
signalsToMonitor.append('BR.QFO/REF.TABLE.FUNC')
signalsToMonitor.append('BR.QDE/REF.TABLE.FUNC') 
signalsToMonitor.append('BR.QDE/LOG.OASIS.I_MEAS') 
signalsToMonitor.append('BR.QFO/LOG.OASIS.I_MEAS')

# MPS
signalsToMonitor.append('logical.BR14.MPS/I')
signalsToMonitor.append('logical.BR23.MPS/I')
signalsToMonitor.append('BR14.MPS/LOG.OASIS.I_MEAS')
signalsToMonitor.append('BR23.MPS/LOG.OASIS.I_MEAS')

# Set B-field
signalsToMonitor.append('PSBBEAM/B')


# MD settings preparation
QV = [4.43, 4.42, 4.41, 4.40, 4.39, 4.38, 4.37, 4.36, 4.35] # for testing simulation tunes ([0], [1], [2])
ws_list = ['11L1.V', '4L1.H']
global_ws_list = []
global_qv_list = []
for qv in QV:
	for ws in ws_list:
		global_qv_list.append(qv)
		global_ws_list.append(ws)
iterations = 1
global_qv_list *= iterations
global_ws_list *= iterations	
total_number_of_shots = len(global_ws_list)
print('total number of shots: ', total_number_of_shots)
ring = rings[0]
QH_ini = myPyJapc.getValues(['PSBBEAM%s/QH#input'%ring[-1]])
QV_ini = myPyJapc.getValues(['PSBBEAM%s/QV#input'%ring[-1]])
QH_ini0 = myPyJapc.getValues(['PSBBEAM%s/QH#input'%ring[-1]])
QV_ini0 = myPyJapc.getValues(['PSBBEAM%s/QV#input'%ring[-1]])
print(QV_ini)



# Callback function
def myCallback(data, h):
	global total_number_of_shots, ring, global_qv_list, global_ws_list
	print( 'Shot ' + str(len(glob.glob(h.saveDataPath + '2023*'))))
	indx = len(glob.glob(h.saveDataPath + '2023*')) 

	if indx == total_number_of_shots:
		h.stopMonitor()
		print("#####################################################")
		print('Measurement finished and monitor stopped.')

	else:
		if myMonitor.saveData==True:
			print('Measuring...')
			newPyJapc.setParam('B%s.BWS.%s/Scan#scanSelection'%(ring,global_ws_list[indx]), 1)
			print('Triggered %s WS...'%global_ws_list[indx])
			QV_ini['PSBBEAM%s/QV#input'%ring[-1]]['value'][0]['Y'][0] = global_qv_list[indx]
			QV_ini['PSBBEAM%s/QV#input'%ring[-1]]['value'][0]['Y'][1] = global_qv_list[indx]
			QV_ini['PSBBEAM%s/QV#input'%ring[-1]]['value'][0]['Y'][2] = global_qv_list[indx]
			#QV_ini['PSBBEAM%s/QV#input'%ring[-1]]['value'][0]['Y'][3] = global_qv_list[indx]
			for ii in range(4):
				pass
				#QV_ini['PSBBEAM%s/QV#input'%ring[-1]]['value'][0]['Y'][ii] = QV_ini0['PSBBEAM%s/QV#input'%ring[-1]]['value'][0]['Y'][ii] + global_qv_list[indx]
			myPyJapc.setSimpleValue('PSBBEAM%s/QV#input'%ring[-1], QV_ini['PSBBEAM3/QV#input']['value'])
			print('Set tune to: ', global_qv_list[indx])


myMonitor = myPyJapc.PyJapcScoutMonitor(mySelector, signalsToMonitor, 
               onValueReceived=myCallback, selectorOverride=mySelector,
               groupStrategy='extended',allowManyUpdatesPerCycle=False,
               strategyTimeout=5200, forceGetOnChangeAndConstantValues=False)



# saving data configuration
myMonitor.saveDataPath = './003_test_simulation_tune_evolution/nominal_multipole_configuration/'
myMonitor.saveData = False
myMonitor.saveDataFormat = 'parquet' # 'parquet' or 'pickle' or 'pickledict' or 'mat'
myMonitor.startMonitor()
