from pyjapcscout import PyJapcScout
import glob


#######################################
# Start PyJapcScout
#######################################
mySelector = 'CPS.USER.MD7'
myPyJapc = PyJapcScout(incaAcceleratorName='PS')
myPyJapc.setDefaultSelector(mySelector)
myPyJapc.rbacLogin() # (optional) get and RBAC tocken


#######################################
# Device properties to be recorded 
#######################################
signalsToMonitor = []
# BCT
signalsToMonitor.append('PR.BCT-ST/Samples') # ring
signalsToMonitor.append('F16.BCT212-ST/Samples') # at F16 transfer line
# Wire scanners
signalsToMonitor.append('PR.BWS.54.H/Acquisition') # appropriate for seeing the islands in SFTPRO
# Tunes
signalsToMonitor.append('PSBEAM/QX_LEQ') 
signalsToMonitor.append('PSBEAM/QY_LEQ')
# Octupoles
# Octupoles to excite the resonance and control the detuning in SFTPRO, they ramp-down after ~C720
signalsToMonitor.append('logical.PR.ODN/I') 
signalsToMonitor.append('logical.PR.ONO39/I')
signalsToMonitor.append('logical.PR.ONO55/I')
# Sextupoles
# Used for compensating non-linear coupling and chromaticity in SFTPRO
signalsToMonitor.append('logical.PR.XNO39/I') # 
signalsToMonitor.append('logical.PR.XNO55/I') # also rotates the islands for better extraction in SFTPRO at ~C800
# SFTPRO observables
signalsToMonitor.append('rda3://UCAP-NODE-PS-TEST/PS_SFTPRO_SPILL_MONITOR/SftproSpill')
signalsToMonitor.append('rda3://UCAP-NODE-PS-TEST/PS_SFTPRO_SPILL_MONITOR/SftproSpillQuality')
# Transverse damper
signalsToMonitor.append('PA.TFB-DSPU-H/BlowupCtrl')


#######################################
# MD settings preparation
#######################################
toSet = False # if False, only a simple reading
to_scan = 'TFB_gain' # 'TFB_gain' 'TFB_frequency', 'TFB_endBLU      
scan_device_dict = {'TFB_gain': 'PA.TFB-DSPU-H/BlowupCtrl',
                    'TFB_frequency': 'PA.TFB-DSPU-H/BlowupCtrl',
                    'TFB_endBLU': 'PA.TFB-DSPU-H/BlowupCtrl'}
scan_parameter_dict = {'TFB_gain': 'excDDS1gain',
                       'TFB_frequency': 'excDDS1harmonic',
                       'TFB_endBLU': 'endBLU'}
scan_values_dict = {'TFB_gain': [0.1, 0.2, 0.3, 0.4, 0.5, 0.6], # to be refined
		            'TFB_frequency': [6.2502, 6.2503], # to be refined
                    'TFB_endBLU': [720, 730, 740, 745, 750]} # to be refined   
scan_device = scan_device_dict[to_scan] # device to be scanned
scan_parameter = scan_parameter_dict[to_scan] # parameter of device to be scanned
initial_value = myPyJapc.getSimpleValue(scan_device) # an initial reading of the device
scan_values = scan_values_dict[to_scan] # values of the parameter to be scanned
iterations = 2 # to get statistics
scan_values = scan_values * iterations
total_number_of_shots = len(scan_values)
print('Total number of shots: ' + str(total_number_of_shots))


#######################################
# Callback function
#######################################
def myCallback(data, h):
    # use of globals; to be improved
    global total_number_of_shots, scan_values, scan_device, scan_parameter, initial_value, toSet
    print( 'Shot ' + str(len(glob.glob(h.saveDataPath + '2023*'))))
    indx = len(glob.glob(h.saveDataPath + '2023*')) 

    if indx == total_number_of_shots:
        h.stopMonitor()
        print("#####################################################")
        print('Measurement finished and monitor stopped.')

    else:
        if toSet:
            #myPyJapc.setSimpleValue('PR.BWS.54.H/Scan#scanSelection', 1) # to launch the Wire Scanner
            initial_value['value'][scan_parameter] = scan_values[indx]
            myPyJapc.setSimpleValue(scan_device, initial_value)
            print('Setting ' + scan_parameter + ' of ' + scan_device + ' to ' + str(scan_values[indx]))    


#######################################
# Create and start monitor
#######################################
myMonitor = myPyJapc.PyJapcScoutMonitor(mySelector, signalsToMonitor, onValueReceived=myCallback, 
					selectorOverride = mySelector, groupStrategy = 'extended',
		            allowManyUpdatesPerCycle=False, strategyTimeout=5200, 
					forceGetOnChangeAndConstantValues=False)
# saving data configuration
myMonitor.saveDataPath = './data/'
myMonitor.saveData = False
myMonitor.saveDataFormat = 'parquet' # or 'parquet' or 'pickle' or 'pickledict' or 'mat'
myMonitor.startMonitor() # start acquisition
#myMonitor.stopMonitor() # stop acquisition