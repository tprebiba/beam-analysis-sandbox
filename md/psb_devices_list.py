signalsToMonitor = []
rings = ['R1','R2','R3','R4']
for ring in rings:
    
    # BCT (from sampler)
    #signalsToMonitor.append('B%s.BCT-ST/Samples'%ring) # old
    signalsToMonitor.append('B%s.BCT-ST/Tuples'%ring)

    # B-field (to be reviewed)
    signalsToMonitor.append('B%s.BMEAS-B-SD/Samples'%ring)
    signalsToMonitor.append('B%s.BMEAS-BDOT-SD/Samples'%ring)
    signalsToMonitor.append('BA%s.FREV-SD/Samples'%ring[-1])

    # Q-strips (set and measured)
    signalsToMonitor.append('B%s.QCF/REF.TABLE.FUNCLIST'%ring)
    signalsToMonitor.append('B%s.QCD/REF.TABLE.FUNCLIST'%ring)
    signalsToMonitor.append('B%s.QCD3/REF.TABLE.FUNCLIST'%ring)
    signalsToMonitor.append('B%s.QCD14/REF.TABLE.FUNCLIST'%ring)
    signalsToMonitor.append('B%s.QCF-IMEAS-SD/Samples'%ring)
    signalsToMonitor.append('B%s.QCD-IMEAS-SD/Samples'%ring)
    signalsToMonitor.append('B%s.QCD3/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.QCD14/LOG.OASIS.I_MEAS'%ring)

    # Tunes (set and measured from BBQ)
    signalsToMonitor.append('PSBBEAM%s/QH'%ring[1])
    signalsToMonitor.append('PSBBEAM%s/QV'%ring[1]) 
    signalsToMonitor.append('B%s.BQ-H-ST/Samples'%ring)
    signalsToMonitor.append('B%s.BQ-V-ST/Samples'%ring)
    signalsToMonitor.append('B%s.BQ/Acquisition#rawDataQ'%ring)
    signalsToMonitor.append('B%s.BQ/Acquisition#rawDataV'%ring)
    signalsToMonitor.append('B%s.BQ/Acquisition#fftDataQ'%ring)
    signalsToMonitor.append('B%s.BQ/Acquisition#fftDataV'%ring)
    signalsToMonitor.append('B%s.BQ/Acquisition#rawDataH'%ring)
    signalsToMonitor.append('B%s.BQ/Acquisition#fftDataH'%ring)
    signalsToMonitor.append('B%s.BDL/REF.TABLE.FUNCLIST'%ring)
    signalsToMonitor.append('B%s.BDL-IMEAS-SD/Samples'%ring)

    # Multipole correctors (normal quadrupoles, normal sextupoles, 
    # skew sextupoles, normal octupoles). Note that there might be
    # ring-to-ring differences. 
    signalsToMonitor.append('B%s.QNO412L3/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.QNO816L3/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.XNO311L1/LOG.OASIS.I_MEAS'%ring) 
    signalsToMonitor.append('B%s.XNO816L1/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.XNO6L1/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.XNO12L1/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.XSK2L4/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.XSK6L4/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.ONO4L1/LOG.OASIS.I_MEAS'%ring)
    signalsToMonitor.append('B%s.ONO12L1/LOG.OASIS.I_MEAS'%ring)

    # k for reconstructing the tunes
    signalsToMonitor.append('logical.B%s.QFO/K'%ring)
    signalsToMonitor.append('logical.B%s.QDE/K'%ring)

    # LIU WS
    signalsToMonitor.append('B%s.BWS.4L1.H/Acquisition'%ring)
    signalsToMonitor.append('B%s.BWS.11L1.V/Acquisition'%ring)
    signalsToMonitor.append('B%s.BWSACQ.V/LoggingCh1'%ring) # raw WS data

    # Legacy WS
    signalsToMonitor.append('B%s.BWS.2L1.H_ROT/Acquisition'%ring)
    signalsToMonitor.append('B%s.BWS.2L1.V_ROT/Acquisition'%ring)

    # BPMs and orbits
    signalsToMonitor.append('B%s.BPM/AcquisitionOrbit'%ring)
    signalsToMonitor.append('B%s.BPM/AcquisitionTrajectoryBBB'%ring)
    signalsToMonitor.append('BA%s.RPOS-SD/Samples'%ring[-1])
    signalsToMonitor.append('BA%s.RPOS-COR-SD/Samples'%ring[-1])
    signalsToMonitor.append('B%s.MRP-H-ST/Tuples'%ring)
    signalsToMonitor.append('B%s.MRP-V-ST/Tuples'%ring)

    # Transverse Feedback system (TFB)
    signalsToMonitor.append('BA%s.TFBV-FB-SD/Samples'%ring[-1])
    signalsToMonitor.append('BA%s.TFBV-PU1-SD/Samples'%ring[-1])
    signalsToMonitor.append('BR.SCOPE06.CH02/Acquisition')
    signalsToMonitor.append('BR.SCOPE06.CH02/Sensibility')
    signalsToMonitor.append('BR.SCOPE06.CH02/SignalConfig')

    # Vertical magnets at injection misteering
    signalsToMonitor.append('BI%s.DVT50/LOG.OASIS.I_MEAS#DATA'%ring[1])
    signalsToMonitor.append('BI%s.DVT70/LOG.OASIS.I_MEAS#DATA'%ring[1])
    signalsToMonitor.append('BI%s.DVT50/LOG.OASIS.I_MEAS_FAST#DATA'%ring[1])
    signalsToMonitor.append('BI%s.DVT70/LOG.OASIS.I_MEAS_FAST#DATA'%ring[1])

    # Scrappers (position, timing delay and power supplies in Amps)
    signalsToMonitor.append('PSBBEAM%s/SHAVER_POSITION_X_MM'%ring[1:2])
    signalsToMonitor.append('PSBBEAM%s/SHAVER_POSITION_Y_MM'%ring[1:2])
    signalsToMonitor.append('B%sX.FDSHAH7L4/Delay'%ring) 
    signalsToMonitor.append('B%sX.FDSHAV7L4/Delay'%ring)
    signalsToMonitor.append('B%s.DSHAH7L4/SettingPPM'%ring)
    signalsToMonitor.append('B%s.DSHAV7L4/SettingPPM'%ring)

# QFO/QDE currents
signalsToMonitor.append('BR.QFO/REF.TABLE.FUNC')
signalsToMonitor.append('BR.QDE/REF.TABLE.FUNC')
signalsToMonitor.append('BR.QDE/LOG.OASIS.I_MEAS')
signalsToMonitor.append('BR.QFO/LOG.OASIS.I_MEAS') 

# MPS
signalsToMonitor.append('logical.BR14.MPS/I')
signalsToMonitor.append('logical.BR23.MPS/I')
signalsToMonitor.append('BR14.MPS/LOG.OASIS.I_MEAS')
signalsToMonitor.append('BR23.MPS/LOG.OASIS.I_MEAS')

# Sextupoles for crhomaticity correction
signalsToMonitor.append('BR.XNOH0/LOG.OASIS.I_MEAS')

# Octupoles	(set and measured)
signalsToMonitor.append('BR.ONOH0/REF.TABLE.FUNC#VALUE')
signalsToMonitor.append('BR.ONOH0/LOG.OASIS.I_MEAS#DATA')

# Other
signalsToMonitor.append('PSBBEAM/B')