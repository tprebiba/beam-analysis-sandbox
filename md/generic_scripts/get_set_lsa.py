# Functions to set strength of quadrupoles in the PSB
import pjlsa

def get_deltak(lsa, lsa_context, quad_parameter_id):

	cycle = lsa_context
	with lsa.java_api():
		from cern.lsa.client import ServiceLocator, ContextService, ParameterService, SettingService
		from cern.lsa.domain.settings import ContextSettingsRequest, Settings
		from java.util import Collections

		contextService = ServiceLocator.getService(ContextService)
		parameterService = ServiceLocator.getService(ParameterService)
		settingService = ServiceLocator.getService(SettingService)

		cycle = contextService.findStandAloneCycle(cycle)
		parameter = parameterService.findParameterByName("logical.%s/K"%quad_parameter_id)
		cycleSettings = settingService.findContextSettings(
			ContextSettingsRequest.byStandAloneContextAndParameters(cycle, Collections.singleton(parameter)))

		function = Settings.getFunction(cycleSettings, parameter)
		ctimes = np.array(function.toXArray()).tolist()
		deltak = np.array(function.toYArray()).tolist()

	return ctimes, deltak


def set_deltak(lsa, lsa_context, ctime_list, deltak_list, quad_parameter_id, setDescription):
	try:
		cycle = lsa_context
		with lsa.java_api():
			from cern.lsa.client import ServiceLocator, ContextService, ParameterService, TrimService
			from cern.lsa.domain.settings import TrimRequest
			from cern.accsoft.commons.value import ValueFactory

			contextService = ServiceLocator.getService(ContextService)
			parameterService = ServiceLocator.getService(ParameterService)
			trimService = ServiceLocator.getService(TrimService)

			cycle = contextService.findStandAloneCycle(cycle)
			parameter = parameterService.findParameterByName("logical.%s/K"%quad_parameter_id)

			function = ValueFactory.createFunction(
				ctime_list,
				deltak_list
			)

			trimRequest = TrimRequest.builder() \
				.setContext(cycle) \
				.addFunction(parameter, function) \
				.setDescription(setDescription) \
				.build()
			trimService.trimSettings(trimRequest)

	except Exception as ex:
		print('LSA setting failed. Here is the exception:')
		print(ex)
		print('Skipping this setting (data will not be saved)')