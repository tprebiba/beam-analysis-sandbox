# See: https://gitlab.cern.ch/scripting-tools/pjlsa
from cern.lsa.client import ServiceLocator, ContextService, OpticService
from cern.accsoft.commons.domain.particletransfers import SpsParticleTransfer
from cern.lsa.domain.optics.factory import TwissesRequestBuilder


contextService = ServiceLocator.getService(ContextService)
opticService = ServiceLocator.getService(OpticService)

cycle = contextService.findStandAloneCycle("ISOHRS_1.4GeV")
spsFunctionBpType = cycle.getIntersections() \
    .getIntersectedFunctionBeamProcess(SpsParticleTransfer.SPSRING, 0.0) \
    .getBeamProcess().getTypeName()
opticsTables = opticService.findContextOpticsTables(cycle)
spsRingOpticTable = [ot for ot in opticsTables if ot.getBeamProcessTypeName() == spsFunctionBpType][0]

opticsTableItems = spsRingOpticTable.getOpticsTableItems()
firstOptic = opticsTableItems[0]
print(firstOptic)

twisses = opticService.findTwisses(TwissesRequestBuilder.byOpticName(firstOptic.getOpticName()))
print(twisses)