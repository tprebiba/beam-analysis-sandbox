from pyjapcscout import PyJapcScout
#from pyjapcscout import PyJapcScoutData
import pyjapc as pyjapc1
import pjlsa
from get_set_lsa import *
import glob
import json


#######################################
# Start PyJapcScout, PyJapc and pjlsa
#######################################
mySelector = 'PSB.USER.MD5'
lsa_context = 'MD9483_LHC25#36b_2023'
myPyJapc = PyJapcScout(incaAcceleratorName='PSB')
myPyJapc.setDefaultSelector(mySelector)
myPyJapc.rbacLogin() # Get and RBAC tocken (needed for the OASIS data)
newPyJapc = pyjapc1.PyJapc(noSet=False, selector=mySelector)
lsa = pjlsa.LSAClient(server='psb')


#######################################
# Device properties to be recorded 
#######################################
signalsToMonitor = []
rings = ['R3']
for ring in rings:
    # BCT
    signalsToMonitor.append('B%s.BCT-ST/Samples'%ring)


#######################################
# MD settings preparation
#######################################
path = './'
name = 'reference_simulation_values_440445.json'
with open(path+name, 'r') as fid:
    data = json.load(fid)
print(data.keys())
times = list(275+np.array(data['ctimes']))
dt = {}
dt['QDE3'] = data['kBRQD314CORR']
dt['QDE14'] = data['kBRQD314CORR']
dt['QDE'] = data['kBRQDCORR']
dt['QFO'] = data['kBRQFCORR']

k314_factors = [1.00, 0.90, 1.10]
ws_list = ['11L1.V', '4L1.H']
global_k314_factors = []
global_WS_list = []
for k314_factor in k314_factors:
	for ws in ws_list:
		global_k314_factors.append(k314_factor)
		global_WS_list.append(ws)
iterations = 1
global_k314_factors *= iterations
global_WS_list *= iterations
total_number_of_shots = len(global_WS_list)
print('k314 factors', k314_factors)
print('ws_list', ws_list)
print('iterations (for statistics)',iterations)
print('Total shots', total_number_of_shots)


#######################################
# Callback function
#######################################
ring = 'R3'
def myCallback(data, h):
    # use of globals; to be improved
	global ring, global_WS_list, global_k314_factors, total_number_of_shots
	global lsa, lsa_context, times, dt
	print( 'Shot ' + str(len(glob.glob(h.saveDataPath + '2023*'))))
	indx = len(glob.glob(h.saveDataPath + '2023*')) 

	if indx == total_number_of_shots:
		h.stopMonitor()
		print("#####################################################")
		print('Measurement finished and monitor stopped.')
	
	else:
		if myMonitor.saveData==True:
            newPyJapc.setParam('B%s.BWS.%s/Scan#scanSelection'%(ring,global_WS_list[indx]), 1) # launch WS (only through PyJapc)
			print('Measuring with the', global_WS_list[indx])
			for quad in ['QDE3','QDE14']:
				quad_parameter_id = 'B%s.%s_CORR'%(ring,quad)
				set_deltak(lsa, lsa_context, times, 
					   list(np.array(dt[quad])*global_k314_factors[indx]), 
					   quad_parameter_id, 'Model correction %1.2f'%global_k314_factors[indx])
				print('Multiplied %s to factor %s'%(quad,global_k314_factors[indx]))


#######################################
# Create and start monitor
#######################################
myMonitor = myPyJapc.PyJapcScoutMonitor(mySelector, signalsToMonitor, onValueReceived=myCallback, 
					selectorOverride = mySelector, groupStrategy = 'extended',
		                        allowManyUpdatesPerCycle=False, strategyTimeout=5200, 
					forceGetOnChangeAndConstantValues=False)
# saving data configuration
myMonitor.saveDataPath = './data/'
myMonitor.saveData = False
myMonitor.saveDataFormat = 'parquet' # or 'parquet' or 'pickle' or 'pickledict' or 'mat'
myMonitor.startMonitor() # start acquisition
#myMonitor.stopMonitor() # stop acquisition