from pyjapcscout import PyJapcScout
#from pyjapcscout import PyJapcScoutData
import glob


#######################################
# Start PyJapcScout
#######################################
mySelector = 'PSB.USER.MD5'
myPyJapc = PyJapcScout(incaAcceleratorName='PSB')
myPyJapc.setDefaultSelector(mySelector)
myPyJapc.rbacLogin() # Get and RBAC tocken (needed for the OASIS data)


#######################################
# Device properties to be recorded 
#######################################
signalsToMonitor = []
rings = ['R3']
for ring in rings:
    # BCT
    signalsToMonitor.append('B%s.BCT-ST/Samples'%ring)


#######################################
# MD settings preparation
#######################################
total_number_of_shots = 200


#######################################
# Callback function
#######################################
def myCallback(data, h):
    # use of globals; to be improved
	global total_number_of_shots
	print( 'Shot ' + str(len(glob.glob(h.saveDataPath + '2023*'))))
	indx = len(glob.glob(h.saveDataPath + '2023*')) 

	if indx == total_number_of_shots:
		h.stopMonitor()
		print("#####################################################")
		print('Measurement finished and monitor stopped.')


#######################################
# Create and start monitor
#######################################
myMonitor = myPyJapc.PyJapcScoutMonitor(mySelector, signalsToMonitor, onValueReceived=myCallback, 
					selectorOverride = mySelector, groupStrategy = 'extended',
		                        allowManyUpdatesPerCycle=False, strategyTimeout=5200, 
					forceGetOnChangeAndConstantValues=False)
# saving data configuration
myMonitor.saveDataPath = './data/'
myMonitor.saveData = False
myMonitor.saveDataFormat = 'parquet' # or 'parquet' or 'pickle' or 'pickledict' or 'mat'
myMonitor.startMonitor() # start acquisition
#myMonitor.stopMonitor() # stop acquisition