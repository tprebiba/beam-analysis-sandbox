# Thanks to Ingrid Mases Sole for the initial scripts

from pyjapcscout import PyJapcScout
from helper_functions import callback_core
import PlottingClassesSPS as pc


def outer_callback(plot_func_list):
    def callback(data, h):
        callback_core(data, h, plot_func_list)
    return callback


# Set up monitor
selector = 'SPS.USER.LHC50NS'
devices = []
#devices.append('SPS.BCTDC.31458/Acquisition')
devices.append('SPS.BCTDC.41435/Acquisition')
devices.append('SPS.BCTDC.51454/Acquisition')
devices.append('SPS.BCTDC.51456/Acquisition')
plot_func_list = [pc.BCT(devices=devices)]

# start PyJapcScout and so incaify Python instance
myPyJapc = PyJapcScout(incaAcceleratorName='SPS')

myMonitor = myPyJapc.PyJapcScoutMonitor(selector, devices,
                   onValueReceived=outer_callback(plot_func_list))

# Saving data configuration
myMonitor.saveDataPath = f'./data/bct/{selector}'
myMonitor.saveData = True
myMonitor.saveDataFormat = 'parquet'

# Start acquisition
myMonitor.startMonitor()


'''
if 0:
    ## for controlling data acquisition:
    #
    myMonitor.saveData = True
    # myMonitor.saveData = False
    
    ## to stop the monitor
    # myMonitor.stopMonitor()
'''