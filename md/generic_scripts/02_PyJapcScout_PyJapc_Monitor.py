from pyjapcscout import PyJapcScout
#from pyjapcscout import PyJapcScoutData
import pyjapc as pyjapc1
import glob


#######################################
# Start PyJapcScout and PyJapc 
#######################################
mySelector = 'PSB.USER.MD5'
myPyJapc = PyJapcScout(incaAcceleratorName='PSB')
myPyJapc.setDefaultSelector(mySelector)
myPyJapc.rbacLogin() # Get and RBAC tocken (needed for the OASIS data)
newPyJapc = pyjapc1.PyJapc(noSet=False, selector=mySelector)


#######################################
# Device properties to be recorded 
#######################################
signalsToMonitor = []
rings = ['R3']
for ring in rings:
    # BCT
    signalsToMonitor.append('B%s.BCT-ST/Samples'%ring)


#######################################
# MD settings preparation
#######################################
ws_list = ['11L1.V', '4L1.H']
ctimes2measure = list(np.arange(460,500,1))
global_WS_list = []
global_WS_timing_list = []
for ws in ws_list:
	for ctime2measure in ctimes2measure:
		global_WS_list.append(ws_list)
		global_WS_timing_list.append(np.array([int(ctime2measure*1e3), int(ctime2measure*1e3)+210000]))
iterations = 2
global_WS_list *= iterations
global_WS_timing_list *= iterations
print(len(ctimes2measure), 'ctimes to measure during one tune ramp')
print(len(ws_list), 'plane(s)')
print(iterations, 'iterations (for statistics)')
total_number_of_shots =len(ctimes2measure)*planes2measure*iterations
print('Total number of shots:', total_number_of_shots)


#######################################
# Callback function
#######################################
ring = 'R3'
def myCallback(data, h):
    # use of globals; to be improved
	global ring, global_WS_list, global_WS_timing_list, total_number_of_shots
	print( 'Shot ' + str(len(glob.glob(h.saveDataPath + '2023*'))))
	indx = len(glob.glob(h.saveDataPath + '2023*')) 

	if indx == total_number_of_shots:
		h.stopMonitor()
		print("#####################################################")
		print('Measurement finished and monitor stopped.')
	
	else:
		if myMonitor.saveData==True:
			myPyJapc.setSimpleValue('B%s.BWS.%s/PSBSetting#delays'%(ring,global_WS_list[indx]),np.array(global_WS_timing_list[indx])) # set the right WS IN/OUT timings
            newPyJapc.setParam('B%s.BWS.%s/Scan#scanSelection'%(ring,global_WS_list[indx]), 1) # launch WS (only through PyJapc)
			print('Measuring with the', global_WS_list[indx], 'at:', global_WS_timing_list[indx])


#######################################
# Create and start monitor
#######################################
myMonitor = myPyJapc.PyJapcScoutMonitor(mySelector, signalsToMonitor, onValueReceived=myCallback, 
					selectorOverride = mySelector, groupStrategy = 'extended',
		                        allowManyUpdatesPerCycle=False, strategyTimeout=5200, 
					forceGetOnChangeAndConstantValues=False)
# saving data configuration
myMonitor.saveDataPath = './data/'
myMonitor.saveData = False
myMonitor.saveDataFormat = 'parquet' # or 'parquet' or 'pickle' or 'pickledict' or 'mat'
myMonitor.startMonitor() # start acquisition
#myMonitor.stopMonitor() # stop acquisition