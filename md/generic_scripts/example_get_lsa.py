# See: https://gitlab.cern.ch/scripting-tools/pjlsa
import pjlsa
from get_set_lsa import *


lsa_context = 'MD9483_LHC25#36b_2023'
lsa = pjlsa.LSAClient(server='psb')
ring = 'R3'
quad_parameter_id = 'B%s.QDE3_CORR'%ring
ctimes,deltak = get_deltak(lsa, lsa_context, quad_parameter_id)
print(ctimes,deltak)