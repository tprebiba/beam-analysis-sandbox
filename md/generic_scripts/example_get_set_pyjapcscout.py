# See: https://gitlab.cern.ch/abpcomputing/sandbox/pyjapcscout
from pyjapcscout import PyJapcScout
import pickle


mySelector = 'PSB.USER.MD5'
myPyJapc = PyJapcScout(incaAcceleratorName='PSB', noSet=False, defaultSelector=mySelector)
myPyJapc.rbacLogin() # (optional) RBAC login (by location)
ring = 'R3'

# Get parameter
value = myPyJapc.getSimpleValue('B%s.BQ/Acquisition#rawDataQ'%ring)
print(value) # dict

# Set parameter (example to be adapted for PSB)
myPyJapc.setSimpleValue('CA.BHB0900/SettingPPM#current', value)

# Get list of values (example to be adapted for PSB)
parameters = ['CA.BHB0400/SettingPPM#current', 'CA.BHB0900/SettingPPM#current']
values = myPyJapc.getValues(parameters)
print(values) # dict

# Set list of values (example to be adapted for PSB)
myPyJapc.setValues(parameters, values)

# Save dict to file
with open('values.pickle', 'wb') as f:
    pickle.dump(values,f)

# Load dict from file
with open('values.pickle', 'rb') as f:
    values = pickle.load(f)
