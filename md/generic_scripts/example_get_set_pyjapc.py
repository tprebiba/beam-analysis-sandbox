# See: https://gitlab.cern.ch/scripting-tools/pyjapc
import pyjapc


mySelector = 'PSB.USER.MD5'
mypyjapc = pyjapc.PyJapc(noSet=False, selector=mySelector, incaAcceleratorName='PSB') # incaAcceleratorName not needed?
ring = 'R3'

# Get parameter
bct_samples = mypyjapc.getParam('B%s.BCT-ST/Samples'%ring)
print(bct_samples)

# Set parameter
mypyjapc.setParam('B%s.BWS.11L1.V/Scan#scanSelection'%ring, 1)