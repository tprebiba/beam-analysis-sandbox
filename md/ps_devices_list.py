signalsToMonitor = []

# BCT
#signalsToMonitor.append('PR.BCT-ST/Samples') # ring (old)
signalsToMonitor.append('PR.BCT-ST/Tuples') # ring
signalsToMonitor.append('F16.BCT212-ST/Samples') # at F16 transfer line

# Wire scanners
signalsToMonitor.append('PR.BWS.54.H/Acquisition') # appropriate for seeing the islands in SFTPRO
signalsToMonitor.append('PR.BWS.64.H/Acquisition')
signalsToMonitor.append('PR.BWS.68.H/Acquisition')
signalsToMonitor.append('PR.BWS.64.V/Acquisition')
signalsToMonitor.append('PR.BWS.68.V/Acquisition')

# B-field
signalsToMonitor.append('PR.BMEAS-B-ST/Samples')

# Tunes
signalsToMonitor.append('PSBEAM/QX_LEQ') 
signalsToMonitor.append('PSBEAM/QY_LEQ')
signalsToMonitor.append('PSBEAM/QX_PFW') # not logged?
signalsToMonitor.append('PSBEAM/QY_PFW') # not logged?
signalsToMonitor.append('PSBEAM/QX_MODEL') 
signalsToMonitor.append('PSBEAM/QY_MODEL')

# Chromaticity
signalsToMonitor.append('PSBEAM/QPH_PFW')
signalsToMonitor.append('PSBEAM/QPV_PFW')

# Octupoles
# Octupoles to excite the resonance and control the detuning in SFTPRO, they ramp-down after ~C720
signalsToMonitor.append('logical.PR.ODN/I') 
signalsToMonitor.append('logical.PR.ONO39/I')
signalsToMonitor.append('logical.PR.ONO55/I')

# Sextupoles
# Used for compensating non-linear coupling and chromaticity in SFTPRO
signalsToMonitor.append('logical.PR.XNO39/I') # 
signalsToMonitor.append('logical.PR.XNO55/I') # also rotates the islands for better extraction in SFTPRO at ~C800
signalsToMonitor.append('logical.PR.XNO39/KL') # 

# SFTPRO observables
signalsToMonitor.append('rda3://UCAP-NODE-PS-TEST/PS_SFTPRO_SPILL_MONITOR/SftproSpill')
signalsToMonitor.append('rda3://UCAP-NODE-PS-TEST/PS_SFTPRO_SPILL_MONITOR/SftproSpillQuality')

# Transverse damper
signalsToMonitor.append('PA.TFB-DSPU-H/BlowupCtrl')