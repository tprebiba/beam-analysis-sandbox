#######################################
# PSB
#######################################
rings = ['R1', 'R2', 'R3', 'R4']
ring = 'R3'
psb_wirescanners = [
    f'B{ring}.BWS.4L1.H',  # LIU horizontal wirescanner
    f'B{ring}.BWS.11L1.V', # LIU vertical wirescanner
]
psb_wirescanner = psb_wirescanners[0]
inout = '1' # '1' for IN scan, '2' for OUT scan
pm = '1'

psb_variable_list = [
    
    # Generic
    'PSB.LSA:CYCLE',              # LSA cycle of the PSB  
    'PSB.TGM:USER',               # text, timing user of the PSB beam
    'PSB.TGM:BEAMID',             # numeric, beam identification
    'PSB.TGM:DEST',               # text, PSB beam destination
    'PSB.TGM:DEST_%s'%ring,       # text, destination for specific ring
    'PSB.TGM:SCNUM',              # numeric, SC number
    
    # BCT
    'BR.BCT.S:INTENSITY',         # numeric e10, Beam intensity in BOOSTER in all rings (PROTONS)
    f'B{ring}.BCT.INJ:INTENSITY', # numeric e10, beam intensity at injection of ring
    f'B{ring}.BCT.ACC:INTENSITY', # numeric e10, Beam intensity at acceleration (extraction) of ring
    'BR.BCTDC:CYCLE_INTENSITY',       # vector e10, Total beam intensity during cycle for all PSB rings
    f'B{ring}.BCTDC:CYCLE_INTENSITY', # vector e10, total beam intensity during cycle for PSB
    
    # Wire scanner
    f'{psb_wirescanner}:Acquisition:projPositionSet{inout}',  # vector, position data of IN scan
    f'{psb_wirescanner}:Acquisition:projDataSet{inout}',      # vector, amplitude data of IN scan
    f'{psb_wirescanner}:Acquisition:bestChannelSet{inout}',
    f'{psb_wirescanner}:Acquisition:acqTimeInCycleSet{inout}',
    f'{psb_wirescanner}:Acquisition:delays', # vector, in and out scan
    f'{psb_wirescanner}:Acquisition:pmtSelection', # '5' corresponds to PMT_ALL
    f'{psb_wirescanner}:Acquisition:aqnStatus', # '2' corresponds to LAUNCH
    f'B{ring}.BWS11L1V:Logging:emittanceAverageSet{inout}PM{pm}' # average emittance
    f'B{ring}.BWS11L1V:Logging:emittanceStdSet{inout}PM{pm}'     # std emittance
    f'B{ring}.BWS11L1V:Logging:qFactorAverageSet{inout}PM{pm}'   # average q-factor
    f'B{ring}.BWS11L1V:Logging:qFactorStdSet{inout}PM{pm}'       # std q-factor
    f'B{ring}.BWS11L1V:Logging:sigmaAverageSet{inout}PM{pm}'     # average beam size
    f'B{ring}.BWS11L1V:Logging:sigmaStdSet{inout}PM{pm}'         # std beamsize
    f'B{ring}.BWS11L1V:Logging:meanAverageSet{inout}PM{pm}'      # average beam mean
    # Wire scanner from UCAP
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:position'
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:profile'
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:bestChannel' # Index of channel with best quality
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:emittanceMean' # Average emittance over all detected bunches
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:emittanceStd' # Standard deviation of the emittance across all detected bunches
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:opticBeta' # Optical beta value at scanner at time of scan
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:opticDispersion' # Dispersion value at scanner at time of scan
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:relBetaGamma' # Relativistic beta gamma at time of scan
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:totalIntensity' # Total beam intensity at time of scan
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:qFactorMean' # Average fitted Qfactor (per Gaussian component) over all detected bunches
    f'{psb_wirescanner}_UCAP:BestChannelResultSet{inout}:qFactorStd' # Standard deviation of the fitted Qfactor (per Gaussian component) across all detected bunches
]
psb_device_list = [
    'BR.BCTDC/Acquisition',            # DCBCT for all rings
    f'{psb_wirescanner}/Acquisition',  # Acquisition of wire scanner
]
psb_device_dict_select = {
    'BR.BCTDC/Acquisition': ['cyclestamp', 'selector', 
                             'totalIntensityRing1','totalIntensityRing2','totalIntensityRing3','totalIntensityRing4'],
    f'{psb_wirescanner}/Acquisition': ['cyclestamp','device','aqnStatus',
                                       f'acqTimeInCycleSet{inout}','delays',
                                       f'bestChannelSet{inout}', 'pmtSelection',
                                       f'projPositionSet{inout}', f'projDataSet{inout}'],
}


#######################################
# PS
#######################################
ps_wirescanners = [
    'PR.BWS.54.H',  # horizontal wirescanner
    'PR.BWS.64.V',  # vertical wirescanner
]
ps_wirescanner = ps_wirescanners[0]
inout = '1'
ps_variable_list = [
    
    # Generic
    'CPS.LSA:CYCLE',  # LSA cycle of the PS
    'CPS.TGM:USER',   # text, timing user of the PS beam
    'CPS.TGM:BEAMID', # numeric, beam identification
    'CPS.TGM:DEST',   # text, destination of PS beam
    'CPS.TGM:DEST2',  # text, secondary destination of PS beam (useful for EAST/TOF)
    'CPS.TGM:SCNUM',  # numeric, SC number
    
    # BCT
    'PR.DCBEFINJ_1:INTENSITY', # PS beam intensity before first injection
    'PR.DCBEFINJ_2:INTENSITY', # PS beam intensity before second injection
    'PR.DCAFTINJ_1:INTENSITY', # PS beam intensity after first injection
    'PR.DCAFTINJ_2:INTENSITY', # PS beam intensity after second injection
    'PR.DCBEFEJE_1:INTENSITY', # PS beam intensity before first ejection
    'PR.DCBEFEJE_2:INTENSITY', # PS beam intensity before second ejection
    'PR.DCAFTEJE_1:INTENSITY', # PS beam intensity after first ejection
    'PR.DCAFTEJE_2:INTENSITY', # PS beam intensity after second ejection
    'PR.BCTDC:CYCLE_INTENSITY' # vector numeric, BCT in ring
    'PR.BCT.LT:SAMPLES',
    'PR.BCT.LT:SAMPLING_TRAIN',    # -> 1 ms
    'PR.BCT.LT:FIRST_SAMPLE_TIME', # -> 100 ms
    
    # Wire scanner
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:emittance', 
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:emittanceMean',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:emittanceStd', 
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:qFactor',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:qFactorMean',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:qFactorStd',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:bunchDetected',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:totalIntensity',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:opticBeta',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:opticDispersion',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:relBetaGamma',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:pmVoltage',  
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:delay',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:profile',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:position',
    f'{ps_wirescanner}_UCAP:BestChannelResultSet{inout}:totalIntensity',
    
    # BQM
    'PS-LOG-BQM-UCAP:Quality:avgBunchLength_ns'
]


#######################################
# SPS
#######################################
sps_wirescanners = [
    'SPS.BWS.51638.H', # horizontal
    'SPS.BWS.51637.H', # horizontal
    'SPS.BWS.41678.V', # horizontal
    'SPS.BWS.41677.V', # horizontal
]
sps_wirescanner = sps_wirescanners[0]
sps_variable_list = [
    
    # Generic
    'SPS.LSA:CYCLE',  # LSA cycle of the SPS
    'SPS.TGM:USER',   # text, timing user of the SPS beam
    'SPS.TGM:BEAMID', # numeric, beam identification
    'SPS.TGM:DDEST',  # text, destination
    'SPS.TGM:SCNUM',  # numeric, SC number
    
    # BCT
    'SPS.BCTDC.51456:Acquisition:measStamp', # 5ms resolution starting from 0 being injection
    'SPS.BCTDC.51456:Acquisition:measStamp_unitExponent', 
    'SPS.BCTDC.51456:Acquisition:totalIntensity', 
    'SPS.BCTDC.51456:Acquisition:totalIntensity_unitExponent'
    'SPS.BCTDC.51456:TOTAL_INTENSITY', 
    'SPS.BCTW.31931:NB_BUNCHES', 
    
    # Wire scanners
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:emittance', # bunch-by-bunch emittances of all the bunches selected
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:qFactor',
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:bunchDetected', # array of flags indicating a bunch was detected in a slot
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:totalIntensity',
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:opticBeta',
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:opticDispersion',
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:relBetaGamma',
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:pmVoltage',  
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:delay',
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:profile',
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:position',
    f'{sps_wirescanner}_UCAP:BestChannelResultSet1:totalIntensity',
    
    # SPS Quality Control
    'SPSQC:cpsTotalExtractedIntensity',   # CPS Total extracted intensity
    'SPSQC:BCT_NAME',                     # text, BCT used
    'SPSQC:NUMBER_OF_INJECTIONS',         # number of injections
    'SPSQC:TOTAL_INJECTED_INTENSITY',     # total injected intensity
    'SPSQC:INTENSITY_START_ACCELERATION', # intensity start acceleration
    'SPSQC:TRANSMISSION',                 # transmission from last injection to start flat top
    'SPSQC:EXTRACTED_INTENSITY'           # intensity at extraction
    'SPSQC:INJECTION_INTENSITY', # vector numeric, Intensity of each injection
    'SPSQC:cpsExtractedIntensities',
    'SPSQC:ORBIT.RMS:meanH',
    'SPSQC:ORBIT.RMS:meanV',
    'SPSQC:ORBIT.RMS:rmsH',
    'SPSQC:ORBIT.RMS:rmsV',
    
    # BQM
    'SPS.BQM:BUNCH_LENGTH_INJ_MEAN', 
    'SPS.BQM:BUNCH_LENGTH_INJ_MIN', 
    'SPS.BQM:BUNCH_LENGTH_INJ_MAX',
    'SPS.BQM:BUNCH_LENGTHS_INJ',

    # BBQ
    'SPS.BQ.QC:Acquisition:estimatedTuneH', # Estimated horizontal tune
    'SPS.BQ.QC:Acquisition:estimatedTuneV', # Estimated vertical tune
    'SPS.BQ.QC:Acquisition:measStamp',      # Time stamp of the measurement
    'SPS.BQ.QC:Acquisition:doneNbOfMeas',   # Number of measurements done
]


#######################################
# Transfer lines
#######################################
tl_variables = [
    'FTA.BCT9053:INTENSITY', # Total Intensity of the AD Transfer Line in Number of Charges [E10]
    'FTA.BCT9053:Acquisition:cycleTimeStamp', # cycle stamp at the start of the AD cycle (is before the cycle stamp of the PS)
    'FTA.BCT9053:Acquisition:beamID',
    'FTA.BCT9053:Acquisition:totalIntensity_unit',
    'FTA.BCT9053:Acquisition:userName'
    'FTA.BCT9053:Acquisition:totalIntensity'
    
    'TT2.BCT.203:TOTAL_INTENSITY_LOWGAIN_INT', # Integrated Total intensity for Beam intensity from Fast BCT in the TT2 line position 203
    'TT2.BCTFI.212:INT_INJ1',  # Intensity in T2 line for first injection
    'TT2.BCTFI.212:INT_INJ2',  # Intensity in T2 line for second injection
    'TT2.BCTFI.212:INT_INJ3',  # Intensity in T2 line for third injection
    'TT2.BCTFI.212:INT_INJ4',  # Intensity in T2 line for fourth injection
    'TT2.BCTFI.212:BUNCH_INTENSITY' # matrix numeric, fast BCT
    'TT2.BCTFI.212:TOTAL_INTENSITY' # vector numeric
    'TT2.BCTFI.212:MEAS_STAMP' # vector numeric, ms
    
    'TT10.BCTFI.102834:INT_INJ1', # Intensity of TT10 for first injection
    'TT10.BCTFI.102834:INT_INJ2', # Intensity of TT10 for second injection
    'TT10.BCTFI.102834:INT_INJ3', # Intensity of TT10 for third injection
    'TT10.BCTFI.102834:INT_INJ4', # Intensity of TT10 for fourth injection
    'TT10.BCTFI.102834:TOTAL_INTENSITY', # vector numeric, total intensity
    
    'F16.BCT.126:TOTAL_INTENSITY',
    'F16.BCT.203:TOTAL_INTENSITY',
    'F16.BCT.212:TOTAL_INTENSITY',
    'F16.BCT.372:TOTAL_INTENSITY', # numeric, Total intensity for Fast BCT in the TT2 line position 372
]