A set of tools to facilitate the data extraction from CERN [NXCALS](https://nxcals-docs.web.cern.ch/current/). It can only be used in environements that have access to nxcals such as [SWAN](swan.cern.ch) or a machine with access to the Technical Network. 

Contact:
tirsi.prebibaj@cern.ch

The repository consists of:
- `fields.py`: some fields for the PSB, PS and SPS. Full list can be found in the [Controls Configuration Data Editor](ccde.cern.ch).
- `nxcals_helpers.py`: some functions for fetching data for multiple fields or device-properties. 
- examples/: examples of nxcals data fetching. 