#NXCALS Data Access API itself consist of two query builders: DataQuery and ParameterDataQuery (DevicePropertyDataQuery is deprecated) returning result dataset as an output.
    
#from cern.nxcals.pyquery.builders import *
from nxcals.api.extraction.data.builders import *
from pyspark.sql.functions import broadcast
import datetime
import pandas as pd
import numpy as np


#######################################
# Loading variables of the same type
#######################################

def get_DataQuery_by_variables(start_time, end_time,variables,
                               system='CMW'):
    '''
    Parameters can be either a string or list
    '''
    if isinstance(variables, str):
        if '%' in variables:
            # Careful: parameters need to have compatible column types
            return DataQuery.builder(spark).variables()\
                            .system(system)\
                            .nameLike(variables)\
                            .timeWindow(start_time, end_time)\
                            .build()
        else:
            return DataQuery.builder(spark).variables()\
                            .system(system)\
                            .nameEq(variables)\
                            .timeWindow(start_time, end_time)\
                            .build()
    elif isinstance(variables, list):
        # Careful: parameters need to have compatible column types
        return DataQuery.builder(spark).variables()\
                            .system(system)\
                            .nameIn(variables)\
                            .timeWindow(start_time, end_time)\
                            .build()

def DataQuery_by_variables_to_dataframe(dataquery):
    
    # Get timestamp, nxcals variable name and value
    # converts everything to pandas dataframe, might be slow. To be revised...
    df = dataquery.select("nxcals_timestamp", "nxcals_value", "nxcals_variable_name").sort("nxcals_timestamp").toPandas()
    # transform timestamp to datetime.timestamp
    df.index = pd.to_datetime(df.pop('nxcals_timestamp'), unit='ns')
    return df

def group_dataframe_variables(df):
    
    # this works for multiple variables of the same type
    
    variables = np.unique(df['nxcals_variable_name'])
    df2 = df.groupby('nxcals_timestamp').sum() # group data according to timestamp
    
    # keep only timestamp
    try:
        df2 = df2.drop(columns=['nxcals_value', 'nxcals_variable_name'])
    except:# excpetion encountered for F16 data; temporary solution...
        df2 = df2.drop(columns=['nxcals_value'])
    
    for variable in variables:
        df2[variable] = df[df['nxcals_variable_name'] == variable]['nxcals_value']
    return df2.reset_index()


#######################################
# Loading variables of different types 
# (matrices)
#######################################

def different_type_variables_to_dataframe(start_time, end_time,variables,
                                          dfmaster=None,
                                          merge_timestamps=True,
                                          on='nxcals_timestamp',direction='forward',tolerance='1.2 seconds',
                                          system='CMW'):
    # The master dataframe to attach the variables
    if dfmaster is None:
        dfmaster = pd.DataFrame()
    
    # Load each variable into a separate dataframe
    for variable in variables:
        dataquery = get_DataQuery_by_variables(start_time, end_time, variable, system=system)
        df = DataQuery_by_variables_to_dataframe(dataquery)
        if df.empty: # to cover cases in which there are no elements
            print(f'Variable {variable} is empty in the specified time range')
        else:
            df = group_dataframe_variables(df)
            if dfmaster.empty:
                dfmaster = df
            else:
                if merge_timestamps==False:
                    dfmaster[variable] = df[variable]
                else:
                    dfmaster = pd.merge_asof(dfmaster, df, on=on, direction=direction, allow_exact_matches=True, tolerance=pd.Timedelta(tolerance))
                
    return dfmaster.reset_index()


#######################################
# Loading entities 
# (device-property like)
#######################################

def get_DataQuery_by_entities(start_time, end_time,
                              parameters,
                              system='CMW'):
    '''
    Parameters can be either a dict or list
    '''
    if isinstance(parameters, dict):
        
        percent_in_keys = any('%' in str(key) for key in parameters.keys())
        percent_in_values = any('%' in str(value) for value in parameters.values())
        if percent_in_keys or percent_in_values:
            return DataQuery.builder(spark).entities()\
                            .system(system)\
                            .keyValuesLike(parameters)\
                            .timeWindow(start_time, end_time)\
                            .build()
        else:
            return DataQuery.builder(spark).entities()\
                            .system(system)\
                            .keyValuesEq(parameters)\
                            .timeWindow(start_time, end_time)\
                            .build()
                        
    elif isinstance(parameters, list):
        return DataQuery.builder(spark).entities()\
                            .system(system)\
                            .keyValuesIn(parameters)\
                            .timeWindow(start_time, end_time)\
                            .build()
    
def DataQuery_by_entities_to_dataframe(dataquery, 
                                       variables=None, field2sort='cyclestamp',selector=None):
    
    if variables is None:
        #d = dict([f.name, f.dataType] for f in dataquery.schema.fields)
        variables = [f.name for f in dataquery.schema.fields]
    #if 'selector' not in variables:
    #    variables.append('selector')
    
    if selector is None:
        df = dataquery.select(*variables).sort(field2sort).toPandas()
    else:
        df = dataquery.select(*variables).where(f'selector=={selector}').toPandas()
    # transform timestamp to datetime.timestamp
    df.index = pd.to_datetime(df.pop(field2sort), unit='ns')
    return df.reset_index()

def get_ParameterDataQuery(start_time, end_time, deviceproperty,
                           system='CMW'):
    '''
    Parameters can be either a dict or list
    '''
    if isinstance(deviceproperty, str):
        
        if '%' in deviceproperty:
            return ParameterDataQuery.builder(spark)\
                                     .system(system)\
                                     .parameterLike(deviceproperty)\
                                     .timeWindow(start_time, end_time)\
                                     .build()
        else:
            return ParameterDataQuery.builder(spark)\
                                     .system(system)\
                                     .parameterEq(deviceproperty)\
                                     .timeWindow(start_time, end_time)\
                                     .build()
                        
    elif isinstance(deviceproperty, list):
        return ParameterDataQuery.builder(spark)\
                                 .system(system)\
                                 .parameterIn(deviceproperty)\
                                 .timeWindow(start_time, end_time)\
                                 .build()
    if select!=None:
        ParamDataQuery = ParamDataQuery.select(*select)
    if selector!=None:
        ParamDataQuery = ParamDataQuery.where(f"selector == '{selector}'")
    return ParamDataQuery
        

def ParameterDataQuery_to_dataframe(dataquery, 
                                    select=None, field2sort='cyclestamp',selector=None):
    
    if select is None:
        #d = dict([f.name, f.dataType] for f in dataquery.schema.fields)
        select = [f.name for f in dataquery.schema.fields]
    if 'selector' not in select:
        select.append('selector')
    
    if selector is None:
        df = dataquery.select(*select).sort("cyclestamp").toPandas()
    else:
        df = dataquery.select(*select).where(f'selector=="{selector}"').sort("cyclestamp").toPandas()
    # transform timestamp to datetime.timestamp
    df.index = pd.to_datetime(df.pop('cyclestamp'), unit='ns')
    return df#.reset_index()

def device_list_to_dataframe(start_time, end_time, deviceproperty_list,
                             system='CMW',
                             select_list=None, field2sort='cyclestamp', selector=None,
                             how='inner'):
    #df_master = pd.DataFrame()
    df_list = []
    for deviceproperty,select in zip(deviceproperty_list,select_list):
        dataquery = get_ParameterDataQuery(start_time, end_time, deviceproperty)
        df = ParameterDataQuery_to_dataframe(dataquery, select=select, field2sort=field2sort, selector=selector)
        #if df_master.empty:
        #    df_master = df
        #else:
        #    df_master = df_master.merge(df, on=field2sort, how=how)
        df_list.append(df)
    #else:
    #    return df
    return merge_dataframes(df_list, on=field2sort, how=how)


#######################################
# Misc 
#######################################

def merge_dataframes(df_list, on='cyclestamp', how='inner'):
    return pd.merge(*df_list, on=on, how=how)

def save_dataframe(df, where, file):
    df.to_pickle(where+file)
    print('Saved ', file, 'at ', where)
    
def load_dataframe(file, where):
    return pd.read_pickle(where+file)
